<?php

namespace app\controllers;

use app\models\Agendamento;
use app\models\AgendamentoProduto;
use app\models\Cliente;
use app\models\Filial;
use app\models\Produto;
use app\models\Profissional;
use app\models\RetornoAPI;
use app\models\Status;
use app\models\Usuario;
use app\models\Balcao;
use app\models\BalcaoProduto;

use MCrypt;
use Yii;
use yii\db\Exception;
use yii\db\IntegrityException;
use yii\helpers\Json;


\Yii::$app->response->format = 'json';


$http_origin = $_SERVER['HTTP_ORIGIN'];

$allowed_domains = array(
  'http://moradialazer.com.br',
  'http://moradialazer.com.br/agendamento',
);

if (in_array($http_origin, $allowed_domains))
{  
    header("Access-Control-Allow-Origin: $http_origin");
}



   $content_type = 'application/json';
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE");
        header("Access-Control-Allow-Headers: Authorization");
        header('Content-type: ' . $content_type);



class DivinoController extends MyController
{

    //Devolve o usuario com base no codigo e pin
    public function actionLogin($user, $pin)
    {
        if ($this->dadosValidos($user) && $this->dadosValidos($pin)) {
            $mcrypt = new MCrypt();
            //            $user = $mcrypt->decrypt($user);
            //            $PIN = $mcrypt->decrypt($PIN);

            $usuario = Usuario::find()
                ->Where(['Usuario' => $user])
                ->andWhere(['Senha' => $pin])
                ->andWhere(['Status' => $this->K_STATUS_ATIVO])
                //                ->with('filial', 'perfil', 'status')
                ////                ->orderBy('ID ASC')
                //                ->select(['ID', 'Nome', 'Email', 'Telefone', 'Usuario', 'Nascimento', 'Filial', 'Perfil', 'Status'])
                ->asArray()->all();

            //session_start();

            $_SESSION['perfil']= $usuario[0]["Perfil"];
            $_SESSION['filial']= $usuario[0]["Filial"];
            $_SESSION['nome']= $usuario[0]["Nome"];




            if ($usuario != null)
                return $this->Valido($usuario);
            else
                return $this->Invalido("Usuário e senha não foram encontrados.");

        } else {

            return $this->Invalido();
        }

    }


    public function actionLoginCliente($user, $pin)
    {
        if ($this->dadosValidos($user) && $this->dadosValidos($pin)) {
            $mcrypt = new MCrypt();
            //            $user = $mcrypt->decrypt($user);
            //            $PIN = $mcrypt->decrypt($PIN);





            $usuario = Cliente::find()
                ->Where(['Usuario' => $user])
                ->andWhere(['Senha' => $pin])

                //                ->with('filial', 'perfil', 'status')
                ////                ->orderBy('ID ASC')
                //                ->select(['ID', 'Nome', 'Email', 'Telefone', 'Usuario', 'Nascimento', 'Filial', 'Perfil', 'Status'])
                ->asArray()->all();





            if ($usuario != null)
                return $this->Valido($usuario);
            else
                return $this->Invalido("Usuário e senha não foram encontrados.");

        } else {

            return $this->Invalido();
        }

    }






    public function actionRetornaAgendamento($ultimoAgendamento)
    {


        if ($this->dadosValidos($ultimoAgendamento)) {
            $mcrypt = new MCrypt();
            //            $filial = $mcrypt->decrypt($filial);

            $lista = Agendamento::find()
                ->Where(['ID' => $ultimoAgendamento])
                ->andWhere(['Status' => $this->K_STATUS_ATIVO])
                ->with('filial', 'gerente', 'cliente', 'profissional', 'gerentecancelamento', 'produtos', 'produtos.produto', 'status')
                ->orderBy('ID ASC')
                //                ->select(['ID', 'Nome', 'Email', 'Telefone', 'Usuario', 'Filial', 'Status'])
                ->asArray()->all();

            //            echo var_dump($lista);
            //            return;

            return $this->Valido($lista, $this->K_TYPE_ARRAY);

        } else {

            return $this->Invalido();
        }

    }


    public function actionMudaFilial($filial, $id_do_cara)
    {


        if ($this->dadosValidos($filial)) {
            $mcrypt = new MCrypt();
            //            $filial = $mcrypt->decrypt($filial);

            $filiali = Usuario::findOne($id_do_cara);

            $filiali->Filial = $filial;

            try {
                if ($filiali->save(false)) {
                    //                return $this->Valido($c->getJsonResult(true), $this->K_TYPE_OBJECT);
                    return $this->Valido($filiali);
                } else {
                    return $this->Invalido("Erro ao Salvar. Tente Novamente. ");
                }
            }

            //            catch (IntegrityException $e) {
            //                return $this->Invalido("Erro ao Salvar.");
            //            }
            catch (Exception $e) {
                //                return var_dump($c);
                return $this->Invalido("Erro ao Salvar. Cliente já cadastrado.");
            }

        } else {

            return $this->Invalido();
        }

    }

    //Retorna os clientes de uma filial
    public function actionClientes($filial)
    {
        if ($this->dadosValidos($filial)) {
            $mcrypt = new MCrypt();
            //            $filial = $mcrypt->decrypt($filial);

            $lista = Cliente::find()
                ->Where(['Filial' => $filial])
                ->andWhere(['Status' => $this->K_STATUS_ATIVO])
                //                ->with('filial', 'status')
                //                ->with('filial')
                //                ->orderBy('ID ASC')
                ->select(['ID', 'Nome', 'Email', 'Telefone', 'Usuario', 'Foto', 'Social', 'Filial', 'Status'])
                ->asArray()->all();

            return $this->Valido($lista, $this->K_TYPE_ARRAY);


        } else {

            return $this->Invalido();
        }

    }



    //Retorna os clientes de uma filial
    public function actionFilialCliente($id)
    {


        $lista = Filial::find()
            ->Where(['ID' => $id])
            ->select(['ID', 'Nome', 'Status'])
            ->asArray()->all();

        return $this->Valido($lista, $this->K_TYPE_ARRAY);



    }

    public function actionFilial()
    {



        //$mcrypt = new MCrypt();
        //            $filial = $mcrypt->decrypt($filial);

        $lista = Filial::find()
            ->Where(['Status' => $this->K_STATUS_ATIVO])
            //->andWhere(['Status' => $this->K_STATUS_ATIVO])
            //                ->with('filial', 'status')
            //                ->with('status')
            ->orderBy('ID ASC')
            //                ->select(['ID', 'Filial', 'Status'])
            //                ->select(['ID', 'Nome', 'Custo', 'Venda', 'Estoque', 'Filial', 'Status'])
            ->asArray()->all();
        return $this->Valido($lista, $this->K_TYPE_ARRAY);



    }


    //Retorna os profissionais de uma Filial
    public function actionProfissionais($filial)
    {


        if ($this->dadosValidos($filial)) {
            $mcrypt = new MCrypt();
            //            $filial = $mcrypt->decrypt($filial);

            $lista = Profissional::find()
                ->Where(['Filial' => $filial])
                ->andWhere(['Status' => $this->K_STATUS_ATIVO])
                //                ->with('filial', 'perfil', 'status')
                ->with('filial', 'status')
                //                ->orderBy('ID ASC')
                ->select(['ID', 'Nome', 'Email', 'Telefone', 'Usuario', 'Filial', 'Status'])
                ->asArray()->all();

            return $this->Valido($lista, $this->K_TYPE_ARRAY);

        } else {

            return $this->Invalido();
        }

    }


    //Retorna os produtos e servicos de uma filial
    public function actionProdutos($filial)
    {



        //$mcrypt = new MCrypt();
        //            $filial = $mcrypt->decrypt($filial);

        $lista = Produto::find()
            ->Where(['Status' => $this->K_STATUS_ATIVO])
            ->andWhere(['Filial' =>  $filial])
            //                ->with('filial', 'status')
            //                ->with('status')
            ->orderBy('ID ASC')
            //                ->select(['ID', 'Filial', 'Status'])
            //                ->select(['ID', 'Nome', 'Custo', 'Venda', 'Estoque', 'Filial', 'Status'])
            ->asArray()->all();
        return $this->Valido($lista, $this->K_TYPE_ARRAY);



    }

    //Retorna os produtos e servicos de uma filial
    public function actionProdutosBalcao($filial)
    {



        //$mcrypt = new MCrypt();
        //            $filial = $mcrypt->decrypt($filial);

        $lista = Produto::find()
            ->Where('Nome Not LIKE :substr', array(':substr' => '%cortesia%'))
            ->andWhere(['Status' =>  $this->K_STATUS_ATIVO])
            ->andWhere(['Filial' =>  $filial])
            ->andWhere(['Servico' => 0])




            //                ->with('filial', 'status')
            //                ->with('status')
            ->orderBy('ID ASC')
            //                ->select(['ID', 'Filial', 'Status'])
            //                ->select(['ID', 'Nome', 'Custo', 'Venda', 'Estoque', 'Filial', 'Status'])
            ->asArray()->all();
        return $this->Valido($lista, $this->K_TYPE_ARRAY);



    }

    //Retorna os clientes de uma filial
    public function actionStatus()
    {

        $lista = Status::find()
            ->orderBy('ID ASC')
            ->asArray()->all();

        return $this->Valido($lista, $this->K_TYPE_ARRAY);



    }


    //Retorna os agendamentos de uma filial
    public function actionAgendamentos($filial)
    {


        if ($this->dadosValidos($filial)) {
            $mcrypt = new MCrypt();
            //            $filial = $mcrypt->decrypt($filial);

            $lista = Agendamento::find()
                ->Where(['Filial' => $filial])
                ->andWhere(['Status' => $this->K_STATUS_ATIVO])
                //                ->with('filial', 'gerente', 'cliente', 'profissional', 'gerentecancelamento', 'produtos', 'status')
                ->orderBy('ID ASC')
                //                ->select(['ID', 'Nome', 'Email', 'Telefone', 'Usuario', 'Filial', 'Status'])
                ->asArray()->all();

            return $this->Valido($lista, $this->K_TYPE_ARRAY);

        } else {

            return $this->Invalido();
        }

    }

    //Retorna os agendamentos de uma filial formatados para o modello FullCalendario
    public function actionAgendamentosCalendario($filial)
    {


        if ($this->dadosValidos($filial)) {
            $mcrypt = new MCrypt();
            //            $filial = $mcrypt->decrypt($filial);


            $lista = Agendamento::find()
                ->Where(['Filial' => $filial])
                ->andWhere(['Status' => $this->K_STATUS_ATIVO])
                //                ->with('filial', 'gerente', 'cliente', 'profissional', 'gerentecancelamento', 'produtos', 'status')
                ->orderBy('ID ASC')
                ->all();


            $arr = [];

            foreach ($lista as $agendamento) {
                $array = array(
                    "id" => $agendamento->ID,
                    "title" => $agendamento->cliente->Nome . ' - ' . $agendamento->profissional->Nome,
                    "start" => $agendamento->DataCalendario(),
                    "end" => $agendamento->DataCalendario()
                );

                //adiciona na lista
                $arr[] = $array;
            }


            return $arr;

        } else {

            return $this->Invalido();
        }

    }


    //Adiciona novo cliente na base de dados
    public function actionNovoCliente($nome, $telefone, $email, $usuario, $senha, $foto, $filial, $social, $aniversario, $cpf)
    {
        if ($this->dadosValidos($nome) && $this->dadosValidos($telefone) && $this->dadosValidos($email)
            && $this->dadosValidos($usuario) && $this->dadosValidos($senha) && $this->dadosValidos($foto)
            && $this->dadosValidos($filial) && $this->dadosValidos($social)) {
            $mcrypt = new MCrypt();
            //            $filial = $mcrypt->decrypt($filial);

            $c = new Cliente();

            $c->Nome = $nome;
            $c->CPF = $cpf;
            $c->Aniversario = $aniversario;
            $c->Telefone = $telefone;
            $c->Email = $email;
            $c->Usuario = $usuario;
            $c->Senha = $senha;

            $c->Filial = $filial;


            try {
                if ($c->save(false)) {
                    //                return $this->Valido($c->getJsonResult(true), $this->K_TYPE_OBJECT);
                    return $this->Valido($c, $this->K_TYPE_OBJECT);
                } else {
                    return $this->Invalido("Erro ao Salvar. Tente Novamente. ");
                }
            }

            //            catch (IntegrityException $e) {
            //                return $this->Invalido("Erro ao Salvar.");
            //            }
            catch (Exception $e) {
                //                return var_dump($c);
                return $e;
            }


        } else {

            return $this->Invalido();
        }

    }

    //Adiciona novo Usuario na base de dados
    public function actionNovoUsuario($nome, $telefone, $email, $usuario, $senha, $nascimento, $filial, $perfil, $status)
    {
        if ($this->dadosValidos($filial)) {
            $mcrypt = new MCrypt();
            //            $filial = $mcrypt->decrypt($filial);

            $u = new Usuario();

            $u->Nome = $nome;
            $u->Telefone = $telefone;
            $u->Email = $email;
            $u->Usuario = $usuario;
            $u->Senha = $senha;
            $u->Nascimento = $nascimento;
            $u->Filial = $filial;
            $u->Perfil = $perfil;

            $u->Status = $status;

            if ($u->save(false)) {
                return $this->Valido($u, $this->K_TYPE_OBJECT);
            } else {
                return $this->Invalido("Erro ao Salvar.");
            }

        } else {


            return $this->Invalido();
        }

    }





    public function actionVerificaFolga($profissional, $data)
    {
        if ($this->dadosValidos($profissional, $data)) {
            $mcrypt = new MCrypt();


            $lista = Yii::$app->db->createCommand("SELECT * FROM Profissional WHERE folga1 = DATE_FORMAT(CONVERT('$data', DATE), '%w') and ID = '$profissional'")
                ->queryOne();



            if($lista!= null){

                return false;

            }
            else
                //pode cadastrar
                return true;




        }else {

            return false;
        }

    }


    public function actionVerificaAlmoco($profissional, $hora, $horafim)
    {
        if ($this->dadosValidos($profissional, $hora, $horafim)) {
            $mcrypt = new MCrypt();


            $lista = Yii::$app->db->createCommand("SELECT * FROM Profissional WHERE CONVERT(almoco, TIME) BETWEEN CONVERT('$hora', TIME)  and CONVERT('$horafim', TIME)  and ID = '$profissional' or ADDTIME(CONVERT(almoco, TIME), '00:59:00') BETWEEN CONVERT('$hora', TIME)  and CONVERT('$horafim', TIME)  and ID = '$profissional'")
                ->queryOne();




            if($lista!= null){

                return false;

            }
            else
                //pode cadastrar
                return true;




        }else {

            return $this->Invalido();
        }

    }




    public function actionRegrasHorario($id, $profissional, $data, $hora, $horafim)
    {
        if ($this->dadosValidos($profissional, $data, $hora, $horafim)) {
            $mcrypt = new MCrypt();

            //SELECT * FROM Agendamento WHERE CONVERT(Hora, TIME) BETWEEN CONVERT('08:00', TIME)  and CONVERT('12:00', TIME) or CONVERT(HoraFim, TIME) BETWEEN CONVERT('08:00', TIME)  and CONVERT('12:00', TIME)

            //SELECT * FROM Agendamento WHERE CONVERT(Data, DATE) BETWEEN CONVERT('2017-11-03', DATE)  and CONVERT('2017-11-30', DATE)


            $lista = Yii::$app->db->createCommand("SELECT * FROM Agendamento WHERE CONVERT(Data, DATE) = CONVERT('$data', DATE) and Profissional = '$profissional' and CONVERT(Hora, TIME) BETWEEN CONVERT('$hora', TIME)  and CONVERT('$horafim', TIME) and STATUS = '1' or CONVERT(HoraFim, TIME) BETWEEN  CONVERT('$hora', TIME)  and CONVERT('$horafim', TIME) and CONVERT(Data, DATE) = CONVERT('$data', DATE) and Profissional = '$profissional' and STATUS = '1'")
                ->queryOne();


            if($lista!= null){
                //nao pode cadastrar
                if($lista["ID"] == $id){
                    return true;
                }else{
                    return false;
                }
            }
            else
                //pode cadastrar
                return true;




        }else {

            return $this->Invalido();
        }

    }

    public function actionDiasDisponiveis($profissional)
    {

        ////localhost:8888/agendamentoAPI/web/index.php?r=divino/dias-disponiveis&profissional=1


        if ($this->dadosValidos($profissional)) {
            //$mcrypt = new MCrypt();

            //dia inicial
            $dia =date('Y/m/d', strtotime('2017/12/01'));


            //dia final
            $dia_final = date("Y/m/d", strtotime( date( "Y/m/d", strtotime( date("Y/m/d") ) ) . "+1 month" ) );



            //array que recebe dias disponiveis
            $arr_dias=[];



            while($dia<=$dia_final){

                $dia_alvo = date('Y/m/d', strtotime('+1 days', strtotime($dia_final)));



                //contador do array
                $i;
                $i=0;
                for( $hora = 6; $hora< 24; $hora++ ) {



                    $horas = ($hora).':00';


                    $hora_fim = ($hora+1).':00';

                    $lista = Yii::$app->db->createCommand("SELECT Data FROM Agendamento WHERE  CONVERT(Data, DATE) = CONVERT('$dia', DATE) and Profissional = '$profissional' and CONVERT(Hora, TIME) BETWEEN CONVERT('$horas', TIME)  and CONVERT('$hora_fim', TIME) and STATUS = '1'  or CONVERT(HoraFim, TIME) BETWEEN  CONVERT('$horas', TIME)  and CONVERT('$hora_fim', TIME) and  CONVERT(Data, DATE) = CONVERT('$dia', DATE)  and Profissional = '$profissional' and STATUS = '1'")
                        ->queryOne();


                    if ($lista!=null){
                        array_push($arr_dias, $dia);

                    }

                }




                $folga = Yii::$app->db->createCommand("SELECT * from Profissional where folga1 =DATE_FORMAT(CONVERT('$dia', DATE), '%w') and ID = '$profissional' and STATUS = '1' or  folga2 =DATE_FORMAT(CONVERT('$dia', DATE), '%w') and ID = '$profissional' and STATUS = '1'")
                    ->queryOne();

                if ($folga!=null){
                    array_push($arr_dias, $dia);

                }




                //numero array ++


                //dia ++
                $dia = date('Y/m/d', strtotime('+1 days', strtotime($dia)));


            }







            return $arr_dias;


        }else {

            return $this->Invalido();
        }

    }




    public function actionAtualizaProfissionaisDisp($dia,$hora,$profissional,$ultimoagendamento)
    {
        ////localhost:8888/agendamentoAPI/web/index.php?r=divino/atualiza-profissionais-disp&dia=2018/01/02&hora=19:30&ultimoagendamento=55&profissional=3
        try{

            $dia = date("Y-m-d", strtotime($dia) );


            $var =  array(
                'ID' => $profissional,
            );

            $i = 0;

            //$mcrypt = new MCrypt();
            $lista_profissional=[];





            $lista = Yii::$app->db->createCommand("SELECT ID,NOME FROM Profissional WHERE ID NOT IN (SELECT Profissional FROM Agendamento WHERE  CONVERT(Data, DATE) = CONVERT('$dia', DATE) AND CONVERT(Hora, TIME)  BETWEEN  CONVERT('$hora', TIME)  AND  ADDTIME(CONVERT('$hora', TIME), '00:19:00')   and STATUS = '1'   or CONVERT(HoraFim, TIME)  BETWEEN  CONVERT('$hora', TIME)  AND  ADDTIME(CONVERT('$hora', TIME), '00:19:00') AND CONVERT(Data, DATE) = CONVERT('$dia', DATE) and STATUS = '1')
             and CONVERT(almoco, TIME) NOT BETWEEN CONVERT('$hora', TIME)  and ADDTIME(CONVERT('$hora', TIME), '00:19:00') and
            CONVERT(almoco, TIME) not BETWEEN SUBTIME(CONVERT('$hora', TIME),  '0:39') and ADDTIME(CONVERT('$hora', TIME), '00:39:00') and folga1 !=DATE_FORMAT( CONVERT('$dia', DATE), '%w') and  folga2 !=DATE_FORMAT( CONVERT('$dia', DATE), '%w') and
             CONVERT(HoraEntrada, TIME)  <= CONVERT('$hora', TIME) and CONVERT(HoraSaida, TIME) >= CONVERT('$hora', TIME)
             ") ->queryAll();







            $lista2 = Yii::$app->db->createCommand("SELECT Profissional FROM Agendamento WHERE  CONVERT(Data, DATE) = CONVERT('$dia', DATE) AND CONVERT(Hora, TIME)  BETWEEN  CONVERT('$hora', TIME)  AND  ADDTIME(CONVERT('$hora', TIME), '00:19:00') and STATUS = '1' and id ='$ultimoagendamento' or CONVERT(HoraFim, TIME)  BETWEEN  CONVERT('$hora', TIME)  AND  ADDTIME(CONVERT('$hora', TIME), '00:19:00') AND CONVERT(Data, DATE) = CONVERT('$dia', DATE) and STATUS = '1' and id ='$ultimoagendamento'")
                ->queryAll();






            foreach($lista as $lista_profissional){

                //dias disponiveis
                $lista_profissional=$lista; 
            }

            //retorna mesmo profissional do agendamento


            if($lista2!=null){
                $x =  $lista2[0]["PROFISSIONAL"];
                $array[0]['ID']['ID'] = $x;
                array_push($lista_profissional,  $array[0]['ID']);


            }





            return $this->Valido($lista_profissional, $this->K_TYPE_OBJECT);

        }
        catch (Exception $e) {
            //                return var_dump($c);
            return $this->Invalido();
        }


    }

    public function actionProfissionaisDisponiveis($dia,$hora)
    {

        ////localhost:8888/agendamentoAPI/web/index.php?r=divino/profissionais-disponiveis&dia=2017/12/07&hora=09:00


        if ($this->dadosValidos($dia,$hora)) {
            //$mcrypt = new MCrypt();

            $lista_profissional=[];
            $lista_profissionalx =[];

            //select Nome from Profissional where ID NOT IN (select Profissional from Agendamento where Data = '2017-12-07' AND Hora = '10:00')





            $lista = Yii::$app->db->createCommand("SELECT ID,NOME, DiaDiferenciado, HoraEntrada FROM Profissional WHERE ID NOT IN (SELECT Profissional FROM Agendamento WHERE  CONVERT(Data, DATE) = CONVERT('$dia', DATE) AND CONVERT(Hora, TIME)  BETWEEN  CONVERT('$hora', TIME)  AND  ADDTIME(CONVERT('$hora', TIME), '00:19:00')   and STATUS = '1'   or CONVERT(HoraFim, TIME)  BETWEEN  CONVERT('$hora', TIME)  AND  ADDTIME(CONVERT('$hora', TIME), '00:19:00') AND CONVERT(Data, DATE) = CONVERT('$dia', DATE) and STATUS = '1') and

            CONVERT(almoco, TIME) not BETWEEN SUBTIME(CONVERT('$hora', TIME),  '0:39') and ADDTIME(CONVERT('$hora', TIME), '00:39:00') and
               folga1 !=DATE_FORMAT( CONVERT('$dia', DATE), '%w') and  folga2 !=DATE_FORMAT( CONVERT('$dia', DATE), '%w') and
             CONVERT(HoraEntrada, TIME)  <= CONVERT('$hora', TIME) and CONVERT(HoraSaida, TIME) >= CONVERT('$hora', TIME)
             ") ->queryAll();








            //PEGA DATA E FALA SE E SEGUNDA A DOMINGO
            $dayofweek = date('w', strtotime($dia));



            foreach($lista as $lista_profissional){



                //verifica se funcionario entra mais tarde
                if($dayofweek == $lista_profissional["DiaDiferenciado"]){

                    $horaAdicional = $lista_profissional["HoraEntrada"];
                    $data = mktime($horaAdicional,40);
                    $horaAdicional= date("H:i", $data);

                    if($hora >= $horaAdicional){
                        array_push($lista_profissionalx,  $lista_profissional);

                    }

                }else{

                    array_push($lista_profissionalx,  $lista_profissional);
                }


            }



            return $this->Valido($lista_profissionalx, $this->K_TYPE_OBJECT);


        }else {

            return $this->Invalido();
        }

    }
    public function actionChicao($profissional, $dia)
    {
        //PEGA DATA E FALA SE E SEGUNDA A DOMINGO
        $dayofweek = date('w', strtotime($dia));

        if($profissional == 7){
            if($dayofweek == 6 || $dayofweek ==5){
                return true;

            }
            else{
                return false;
            }
        }
        else{
            return true;
        }
    }



    public function actionProfDispDia($dia)
    {

        ////localhost:8888/agendamentoAPI/web/index.php?r=divino/profissionais-disponiveis&dia=2017/12/07&hora=09:00

        //array que recebe dias disponiveis

        try{


            $arr_profisisonais=[];
            $lista_profissionalx=[];

            $hora=6;

            $null=0;

            $lista_profissional=0;

            for( $hora ; $hora<24; $hora++ ) {


                $data = mktime($hora,00);
                $horas= date("H:i", $data);

                $data = mktime($hora+1,00);
                $hora_fim= date("H:i", $data);


                $lista = Yii::$app->db->createCommand("SELECT ID FROM Profissional WHERE ID NOT IN (SELECT Profissional FROM Agendamento WHERE  CONVERT(Data, DATE) = CONVERT('$dia', DATE) AND CONVERT(Hora, TIME)  BETWEEN  CONVERT('$horas', TIME)  AND  CONVERT('$hora_fim', TIME) and STATUS = '1' OR CONVERT(HoraFim, TIME)  BETWEEN  CONVERT('$horas', TIME)  AND  CONVERT('$hora_fim', TIME) and STATUS = '1' AND  CONVERT(Data, DATE) = CONVERT('$dia', DATE)) and  folga1 !=DATE_FORMAT( CONVERT('$dia', DATE), '%w') and  folga2 !=DATE_FORMAT( CONVERT('$dia', DATE), '%w') and
            CONVERT(almoco, TIME) != CONVERT('$horas', TIME)  
            ")


                    ->queryAll();



                //PEGA DATA E FALA SE E SEGUNDA A DOMINGO
                $dayofweek = date('w', strtotime($dia));

                foreach($lista as $lista_profissional){

                    //verifica se funcionario entra mais tarde
                    if($dayofweek == $lista_profissional["DiaDiferenciado"]){

                        $horaAdicional = $lista_profissional["HoraEntrada"];
                        $data = mktime($horaAdicional,40);
                        $horaAdicional= date("H:i", $data);

                        if($hora >= $horaAdicional){
                            array_push($lista_profissionalx,  $lista_profissional);

                        }

                    }else{



                        array_push($lista_profissionalx,  $lista_profissional);
                    }

                    //dias disponiveis
                    if($lista!=null){
                        $null =1;
                    }


                }


            }

            if($null=1){

                //removendo resultados duplicados!!

                for ($i = 0; isset($lista_profissionalx[$i]); $i++) { // remove any duplicates from the result array
                    $c = 0;
                    for($c = 0; $i > $c; $c++) {
                        if ($lista_profissionalx[$i]["ID"] == $lista_profissionalx[$c]["ID"]) {
                            unset($lista_profissionalx[$i]);
                        }
                    }
                }  

                return $this->Valido($lista_profissionalx, $this->K_TYPE_OBJECT);
            }
            if($null=0){
                $lista_profissionalx=0;

                return $lista_profissionalx;
            }

        }
        catch (Exception $e) {
            //                return var_dump($c);
            return $this->Invalido();
        }
    }



    public function actionHorasDisponiveis($profissional, $dia)

    {
        ////localhost:8888/agendamentoAPI/web/index.php?r=divino/horas-disponiveis&profissional=3&dia=2018/01/02&hora=9:30

        if ($this->dadosValidos($profissional)) {



            $result=[];
            $listinha1=[];
            $listinha2=[];



            $lista = Yii::$app->db->createCommand("SELECT Hora, HoraFim FROM Agendamento WHERE  CONVERT(Data, DATE) = CONVERT('$dia', DATE) and Profissional = '$profissional' and Status =1")
                ->queryAll();





            foreach($lista as $lista_horas){
                array_push($listinha1, $lista_horas["Hora"]);
                array_push($listinha1, $lista_horas["HoraFim"]);
                array_push($result, $listinha1);
                $listinha1=[];

            }



            //PEGA DATA E FALA SE E SEGUNDA A DOMINGO
            $dayofweek = date('w', strtotime($dia));



            //select do dia que entra mais tarde

            $horatardia = Yii::$app->db->createCommand("SELECT DiaDiferenciado from Profissional where ID = '$profissional' and STATUS = '1'")->queryOne();



            $horaEntrada = Yii::$app->db->createCommand("SELECT HoraEntrada from Profissional where ID = '$profissional' and STATUS = '1'")->queryOne();
            $horaSaida = Yii::$app->db->createCommand("SELECT HoraSaida from Profissional where ID = '$profissional' and STATUS = '1'")->queryOne();








            //verifica se funcionario entra mais tarde
            if(intval($dayofweek) == intval($horatardia["DiaDiferenciado"])){

                $horaEntrada = $horaEntrada["HoraEntrada"];

                $data = mktime($horaEntrada,40);


                $horaEntrada= date("H:i", $data);


                $horaSaida = $horaSaida["HoraSaida"];

                $data = mktime($horaSaida,40);
                $horaSaida= date("H:i", $data);





                $horario = Yii::$app->db->createCommand("SELECT HoraInicio, HoraFim from Horas where HoraInicio NOT BETWEEN '$horaEntrada' and '$horaSaida' and HoraFim NOT BETWEEN '$horaEntrada' and '$horaSaida'")->queryAll();




            }
            else{
                $horaEntrada = $horaEntrada["HoraEntrada"];

                $horaSaida = $horaSaida["HoraSaida"];

                $horario = Yii::$app->db->createCommand("SELECT HoraInicio, HoraFim from Horas where HoraInicio NOT BETWEEN '$horaEntrada' and '$horaSaida' and HoraFim NOT BETWEEN '$horaEntrada' and '$horaSaida'")->queryAll();
            }





            foreach($horario as $lista_horario){
                array_push($listinha2, $lista_horario["HoraInicio"]);
                array_push($listinha2, $lista_horario["HoraFim"]);
                array_push($result, $listinha2);
                $listinha2=[];

            }








            $almoco = Yii::$app->db->createCommand("SELECT almoco from Profissional where ID = '$profissional' and STATUS = '1'")->queryOne();






            if ($almoco!=null){
                $almoco = implode("", $almoco);
                $data = mktime(intval($almoco)+1,19);
                $almocofim= date("H:i", $data);
                array_push($listinha1, $almoco);
                array_push($listinha1, $almocofim);
                array_push($result, $listinha1);
                $listinha1=[];

            }





            //[['6:00am', '11:30am']
            return $result;


        }else {

            return $this->Invalido();
        }

    }

    public function actionHorasPossiveis($profissional, $dia)

    {
        ////localhost:8888/agendamentoAPI/web/index.php?r=divino/horas-disponiveis&profissional=3&dia=2018/01/02&hora=9:30

        if ($this->dadosValidos($profissional)) {


            $result=[];
            $listinha1=[];
            $listinha2=[];



            $lista = Yii::$app->db->createCommand("SELECT Hora, HoraFim FROM Agendamento WHERE  CONVERT(Data, DATE) = CONVERT('$dia', DATE) and Profissional = '$profissional' and Status =1")
                ->queryAll();


            $lista = Yii::$app->db->createCommand("SELECT Hora, HoraFim FROM Agendamento WHERE  CONVERT(Data, DATE) = CONVERT('$dia', DATE) and Profissional = '$profissional' and Status =1")
                ->queryAll();





            foreach($lista as $lista_horas){
                array_push($listinha1, $lista_horas["Hora"]);
                array_push($listinha1, $lista_horas["HoraFim"]);
                array_push($result, $listinha1);
                $listinha1=[];

            }



            //PEGA DATA E FALA SE E SEGUNDA A DOMINGO
            $dayofweek = date('w', strtotime($dia));



            //select do dia que entra mais tarde

            $horatardia = Yii::$app->db->createCommand("SELECT DiaDiferenciado from Profissional where ID = '$profissional' and STATUS = '1'")->queryOne();



            $horaEntrada = Yii::$app->db->createCommand("SELECT HoraEntrada from Profissional where ID = '$profissional' and STATUS = '1'")->queryOne();
            $horaSaida = Yii::$app->db->createCommand("SELECT HoraSaida from Profissional where ID = '$profissional' and STATUS = '1'")->queryOne();








            //verifica se funcionario entra mais tarde
            if(intval($dayofweek) == intval($horatardia["DiaDiferenciado"])){

                $horaEntrada = $horaEntrada["HoraEntrada"];

                $data = mktime($horaEntrada,40);


                $horaEntrada= date("H:i", $data);


                $horaSaida = $horaSaida["HoraSaida"];

                $data = mktime($horaSaida,40);
                $horaSaida= date("H:i", $data);





                $horario = Yii::$app->db->createCommand("SELECT HoraInicio, HoraFim from Horas where HoraInicio NOT BETWEEN '$horaEntrada' and '$horaSaida' and HoraFim NOT BETWEEN '$horaEntrada' and '$horaSaida'")->queryAll();




            }
            else{
                $horaEntrada = $horaEntrada["HoraEntrada"];

                $horaSaida = $horaSaida["HoraSaida"];

                $horario = Yii::$app->db->createCommand("SELECT HoraInicio, HoraFim from Horas where HoraInicio NOT BETWEEN '$horaEntrada' and '$horaSaida' and HoraFim NOT BETWEEN '$horaEntrada' and '$horaSaida'")->queryAll();
            }





            foreach($horario as $lista_horario){
                array_push($listinha2, $lista_horario["HoraInicio"]);
                array_push($listinha2, $lista_horario["HoraFim"]);
                array_push($result, $listinha2);
                $listinha2=[];

            }


            $almoco = Yii::$app->db->createCommand("SELECT almoco from Profissional where ID = '$profissional' and STATUS = '1'")->queryOne();


            if ($almoco!=null){
                $almoco = implode("", $almoco);
                $data = mktime(intval($almoco)+1,19);
                $almocofim= date("H:i", $data);
                array_push($listinha1, $almoco);
                array_push($listinha1, $almocofim);
                array_push($result, $listinha1);
                $listinha1=[];

            }


            //$result horas q nao pode

            $final= [];

            $input = Yii::$app->db->createCommand("SELECT HoraInicio from Horas")->queryAll();


            $listinha1 =[];
            foreach($result as $listinha){



                array_push($listinha1, $listinha[0]);



            }





            $listinha2 =[];
            foreach($input as $as){

                array_push($listinha2, $as['HoraInicio']);





            }

            $resultado = array_diff($listinha2, $listinha1);
            //
            
            
            foreach($resultado as $query){
                
                 array_push($final, $query);
                
            }
       









            //[['6:00am', '11:30am']
            return $this->Valido($final, $this->K_TYPE_OBJECT);



        }else {

            return $this->Invalido();
        }

    }


    public function actionHorasApi()

    {
        ////localhost:8888/agendamentoAPI/web/index.php?r=divino/horas-disponiveis&profissional=3&dia=2018/01/02&hora=9:30



        $result=[];
        $listinha1=[];
        $listinha2=[];

        $lista = Yii::$app->db->createCommand("SELECT HoraInicio FROM Horas ORDER BY TIME(HoraInicio) ASC")
            ->queryAll();





        foreach($lista as $lista_horas){
            array_push($listinha1, $lista_horas["HoraInicio"]);



        }



        //[['6:00am', '11:30am']
        return $this->Valido($listinha1, $this->K_TYPE_OBJECT);



    }


    public function actionHorasDisponiveisAtualizar($profissional, $dia, $hora)

    {
        ////localhost:8888/agendamentoAPI/web/index.php?r=divino/horas-disponiveis&profissional=3&dia=2018/01/02&hora=9:30

        if ($this->dadosValidos($profissional)) {

            $result=[];
            $listinha1=[];
            $listinha2=[];

            $lista = Yii::$app->db->createCommand("SELECT Hora, HoraFim FROM Agendamento WHERE  CONVERT(Data, DATE) = CONVERT('$dia', DATE) and Profissional = '$profissional' and Status =1 and CONVERT(Hora, TIME) != CONVERT(' $hora', TIME)")
                ->queryAll();



            foreach($lista as $lista_horas){
                array_push($listinha1, $lista_horas["Hora"]);
                array_push($listinha1, $lista_horas["HoraFim"]);
                array_push($result, $listinha1);
                $listinha1=[];

            }

            $almoco = Yii::$app->db->createCommand("SELECT almoco from Profissional where ID = '$profissional' and STATUS = '1'")
                ->queryOne();




            $horaEntrada = Yii::$app->db->createCommand("SELECT HoraEntrada from Profissional where ID = '$profissional' and STATUS = '1'")->queryOne();





            $horaSaida = Yii::$app->db->createCommand("SELECT HoraSaida from Profissional where ID = '$profissional' and STATUS = '1'")->queryOne();



            //PEGA DATA E FALA SE E SEGUNDA A DOMINGO
            $dayofweek = date('w', strtotime($dia));



            //select do dia que entra mais tarde

            $horatardia = Yii::$app->db->createCommand("SELECT DiaDiferenciado from Profissional where ID = '$profissional' and STATUS = '1'")->queryOne();

            //verifica se funcionario entra mais tarde
            if(intval($dayofweek) == intval($horatardia["DiaDiferenciado"])){

                $horaEntrada = $horaEntrada["HoraEntrada"];

                $data = mktime($horaEntrada,40);


                $horaEntrada= date("H:i", $data);


                $horaSaida = $horaSaida["HoraSaida"];

                $data = mktime($horaSaida,40);
                $horaSaida= date("H:i", $data);





                $horario = Yii::$app->db->createCommand("SELECT HoraInicio, HoraFim from Horas where HoraInicio NOT BETWEEN '$horaEntrada' and '$horaSaida' and HoraFim NOT BETWEEN '$horaEntrada' and '$horaSaida'")->queryAll();




            }
            else{
                $horaEntrada = $horaEntrada["HoraEntrada"];

                $horaSaida = $horaSaida["HoraSaida"];

                $horario = Yii::$app->db->createCommand("SELECT HoraInicio, HoraFim from Horas where HoraInicio NOT BETWEEN '$horaEntrada' and '$horaSaida' and HoraFim NOT BETWEEN '$horaEntrada' and '$horaSaida'")->queryAll();
            }

            foreach($horario as $lista_horario){
                array_push($listinha2, $lista_horario["HoraInicio"]);
                array_push($listinha2, $lista_horario["HoraFim"]);
                array_push($result, $listinha2);
                $listinha2=[];

            }



            if ($almoco!=null){
                $almoco = implode("", $almoco);
                $data = mktime(intval($almoco)+1,19);
                $almocofim= date("H:i", $data);
                array_push($listinha1, $almoco);
                array_push($listinha1, $almocofim);
                array_push($result, $listinha1);
                $listinha1=[];

            }








            //[['6:00am', '11:30am']


            //array_multisort($result[0], SORT_ASC, SORT_STRING, $result[1], SORT_NUMERIC);
            return $result;


        }else {

            return $this->Invalido();
        }

    }



    public function actionAgendamentoGratuito($id)
    {



        if ($this->dadosValidos($id)) {
            $mcrypt = new MCrypt();

            $agendamento = Agendamento::findOne($id);
            $cliente = $agendamento->Cliente;
            $clientetodo = Cliente::findOne($cliente);

            if($clientetodo->Bonus >= 11){

                //agendamento vira gratuito
                $agendamento->Gratuito = 1;

                if ($agendamento->save(false)) {
                }

                //zera bonus
                $clientetodo->Bonus = 0;
                if ($clientetodo->save(false)) {
                }


                //zera valores dos produtos deste agendamento
                $ap = AgendamentoProduto::find()
                    ->Where(['Agendamento' => $id])
                    ->asArray()->all();

                $bonus =0;

                foreach ($ap as $arr) {
                    $ap = AgendamentoProduto::findOne($arr);

                    if($bonus ==0){

                        if($ap->produto->Servico ==1) {
                            //zerando valor do produto
                            $ap->Gratuito = 1;


                            $bonus = 1;
                        }
                    }

                    if ($ap->save(false)) {

                    }

                }



                $agendamento = 1;
                return $this->Valido($agendamento, $this->K_TYPE_OBJECT);

            }else{

                $agendamento = $agendamento->Total;
                return $this->Valido($agendamento, $this->K_TYPE_OBJECT);
            }




        }else {

            return $this->Invalido();
        }

    }




    public function actionProdutosId($id,$filial)
    {




        if ($this->dadosValidos($id)) {
            //            $mcrypt = new MCrypt();
            //            $filial = $mcrypt->decrypt($filial);

            $prod = Produto::find()
                ->Where(['Filial' => $filial])
                ->andWhere(['ID' => $id])

                ->asArray()->one();




            return $this->Valido($prod, $this->K_TYPE_OBJECT);

        }else {

            return $this->Invalido();
        }

    }





    //Adiciona novo Agendamento na base de dados
    //Produtos vem em array
    public function actionNovoAgendamento($data, $hora, $horafim, $cliente, $profissional, $gerente, $filial, $produtos)
    {


        try{


            if ($this->dadosValidos($data)) {
                $mcrypt = new MCrypt();
                //            $filial = $mcrypt->decrypt($filial);


                //TODO: Criar estrutura de Retorno para padronizar erros e resultados.
                $arr = explode(",", $produtos);
                if ($arr != null && sizeof($arr) > 0 && $arr[0] != "") {

                } else {
                    return $this->Invalido("Produtos.");
                }

                $a = new Agendamento();
                $a->Data = $data;
                $a->HoraFim = $horafim;
                $a->Hora = $hora;

                $total = 0;
                $a->Cliente = $cliente;
                $a->Profissional = $profissional;
                $a->Gerente = $gerente;
                $a->Filial = $filial;






                if ($a->save(false)) {


                    //grava os produtos
                    foreach ($arr as $id) {
                        $prod = Produto::findOne($id);
                        if ($prod != null) {
                            $total = (float)$total + (float)$prod->Venda;

                            if($prod->Servico==0){



                                $prod->Estoque =  (float)$prod->Estoque - 1;

                                if($prod->save(false)){


                                }

                            }



                            $ap = new AgendamentoProduto();
                            $ap->Agendamento = $a->ID;
                            $ap->Produto = $id;
                            $ap->Valor = $prod->Venda;
                            $ap->Custo = $prod->Custo;
                            $ap->Cortesia = $prod->Cortesia;
                            $ap->Status = $this->K_STATUS_ATIVO;



                            if ($ap->save(false)) {

                            }

                        }


                    }


                    //atualiza total
                    $a->Total = $total;
                    $a->Status = $this->K_STATUS_ATIVO;
                    if ($a->save(false)) {

                        $agendamento = Agendamento::find()
                            ->Where(['ID' => $a->ID])
                            ->andWhere(['Status' => $this->K_STATUS_ATIVO])
                            ->with('cliente', 'profissional', 'gerente', 'filial', 'produtos', 'status')
                            //                                ->select(['ID', 'Nome', 'Email', 'Telefone', 'Usuario', 'Nascimento', 'Filial', 'Perfil', 'Status'])
                            ->asArray()->one();


                        //adiciona Bonus ao Cliente
                        $cliente = Cliente::findOne($cliente);
                        $cliente->Bonus = (float)$cliente->Bonus +1;
                        $cliente->save(false);



                        return $this->Valido($agendamento, $this->K_TYPE_OBJECT);
                    }


                } //falha ao salvar
                else {

                    return $this->Invalido("Falha ao Salvar Agendamento.");


                }


            } else {

                return $this->Invalido();

            }
        }
        catch (Exception $e) {
            //               
            return var_dump($e);
        }

    }


    public function actionNovoBalcao($data, $produtos, $gerente, $filial, $pagamento)


    {

        try {

            if ($this->dadosValidos($produtos)) {
                $mcrypt = new MCrypt();
                //            $filial = $mcrypt->decrypt($filial);


                //TODO: Criar estrutura de Retorno para padronizar erros e resultados.
                $arr = explode(",", $produtos);
                if ($arr != null && sizeof($arr) > 0 && $arr[0] != "") {

                } else {
                    return $this->Invalido("Produtos.");
                }




                $a = new Balcao();
                $a->Data = getdate();
                $a->Data  = date("Y-m-d"); 
                $a->FormaPagamento = $pagamento;






                //add parametros do balcao

                $total = 0;

                $a->Gerente = $gerente;
                $a->Filial = $filial;



                if ($a->save(false)) {


                    //grava os produtos
                    foreach ($arr as $id) {
                        $prod = Produto::findOne($id);
                        if ($prod != null) {
                            $total = (float)$total + (float)$prod->Venda;

                            if($prod->Servico==0){
                                $prod->Estoque =  (float)$prod->Estoque - 1;
                                if($prod->save(false)){

                                }
                            }


                            $ap = new BalcaoProduto();
                            $ap->Balcao = $a->ID;
                            $ap->Produto = $id;
                            $ap->Valor = $prod->Venda;
                            $ap->Custo = $prod->Custo;
                            $ap->Status = $this->K_STATUS_ATIVO;

                            if ($ap->save(false)) {

                            }

                        }


                    }


                    //atualiza total
                    $a->Total = $total;
                    $a->Status = $this->K_STATUS_ATIVO;
                    if ($a->save(false)) {

                        $balcao = Balcao::find()
                            ->Where(['ID' => $a->ID])
                            ->andWhere(['Status' => $this->K_STATUS_ATIVO])
                            ->with( 'gerente', 'filial', 'status')
                            //                                ->select(['ID', 'Nome', 'Email', 'Telefone', 'Usuario', 'Nascimento', 'Filial', 'Perfil', 'Status'])
                            ->asArray()->one();


                        return $this->Valido($balcao, $this->K_TYPE_OBJECT);
                    }


                } //falha ao salvar
                else {

                    return $this->Invalido("Falha ao Salvar Agendamento.");


                }


            } else {

                return $this->Invalido();

            }
        }

        catch (Exception $e) {
            //               
            return var_dump($e);
        }


    }

    public function actionDeletaProduto($id)
    {

        //$produto = AgendamentoProduto::findAll($id);
        $ap= AgendamentoProduto::deleteAll('Agendamento ='.$id);

        return $ap;
        //echo var_dump($ap); 

        //echo var_dump($ap);
    }



    public function actionAtualizaAgendamento($id, $data, $hora, $horafim, $cliente, $profissional, $gerente, $filial, $produtos)


    {

        if ($this->dadosValidos($data)) {
            //            $mcrypt = new MCrypt();
            //            $filial = $mcrypt->decrypt($filial);


            //TODO: Criar estrutura de Retorno para padronizar erros e resultados.
            $arr = explode(",", $produtos);
            if ($arr != null && sizeof($arr) > 0 && $arr[0] != "") {

            } else {
                return $this->Invalido("Produtos.");
            }

            $a = Agendamento::findOne($id);
            //            $a = new Agendamento();
            //            $a->ID - $id;
            $a->Data = $data;
            $a->Hora = $hora;
            $a->HoraFim = $horafim;

            $total = 0;
            $a->Cliente = $cliente;
            $a->Profissional = $profissional;
            $a->Gerente = $gerente;
            $a->Filial = $filial;





            if ($a->save(false)) {

                //vai para funcao deleta produto
                $apagados = $this->actionDeletaProduto($id);


                //grava os produtos
                foreach ($arr as $ide) {
                    $prod = Produto::findOne($ide);
                    if ($prod != null) {
                        $total = (float)$total + (float)$prod->Venda;

                        if($prod->Servico==0){



                            $prod->Estoque =  (float)$prod->Estoque - 1;

                            if($prod->save(false)){


                            }

                        }



                        $ap = new AgendamentoProduto();
                        $ap->Agendamento = $a->ID;
                        $ap->Produto = $ide;
                        $ap->Valor = $prod->Venda;
                        $ap->Custo = $prod->Custo;
                        $ap->Cortesia = $prod->Cortesia;
                        $ap->Status = $this->K_STATUS_ATIVO;

                        if ($ap->save(false)) {

                        }

                    }


                }


                //atualiza total
                $a->Total = $total;
                $a->Status = $this->K_STATUS_ATIVO;
                if ($a->save(false)) {

                    $agendamento = Agendamento::find()
                        ->Where(['ID' => $a->ID])
                        ->andWhere(['Status' => $this->K_STATUS_ATIVO])
                        ->with('cliente', 'profissional', 'gerente', 'filial', 'produtos', 'status')
                        //                                ->select(['ID', 'Nome', 'Email', 'Telefone', 'Usuario', 'Nascimento', 'Filial', 'Perfil', 'Status'])
                        ->asArray()->one();


                    return $this->Valido($agendamento, $this->K_TYPE_OBJECT);
                }


            } //falha ao salvar
            else {

                return $this->Invalido("Falha ao Salvar Agendamento.");


            }


        } else {

            return $this->Invalido();

        }

    }
    //Cancela um Agendamento
    public
        function actionFinalizaAgendamento($agendamento, $gerente)
    {
        if ($this->dadosValidos($agendamento) && $this->dadosValidos($gerente)) {
            $mcrypt = new MCrypt();
            //            $filial = $mcrypt->decrypt($filial);

            $a = Agendamento::find()
                ->Where(['ID' => $agendamento])
                ->andWhere(['Status' => $this->K_STATUS_ATIVO])
                ->one();

            if ($a != null) {

                $a->Status = $this->K_STATUS_FINALIZADO;
                $a->GerenteCancelamento = $gerente;
                if ($a->save(false)) {
                    return $this->Valido($a);
                }

            } else {
                return $this->Invalido("Falha ao Finalizar Agendamento.");
            }


        } else {

            return $this->Invalido();
        }

    }


    //Forma de Pagamento
    public
        function actionFinalizaPagamento($pagamento, $ultimoAgendamento)
    {


        if ($this->dadosValidos($ultimoAgendamento) && $this->dadosValidos($pagamento)) {
            $mcrypt = new MCrypt();
            //            $filial = $mcrypt->decrypt($filial);

            $a = Agendamento::find()
                ->Where(['ID' => $ultimoAgendamento])

                ->one();

            if ($a != null) {

                $a->FormaPagamento = $pagamento;

                if ($a->save(false)) {
                    return $this->Valido($a);
                }

            } else {
                return $this->Invalido("Falha ao Finalizar Pagamento.");
            }


        } else {

            return $this->Invalido();
        }

    }

    //Cancela um Agendamento
    public
        function actionCancelarAgendamento($agendamento, $justificativa ,$gerente)
    {
        if ($this->dadosValidos($agendamento) && $this->dadosValidos($gerente)) {
            $mcrypt = new MCrypt();
            //            $filial = $mcrypt->decrypt($filial);

            $a = Agendamento::find()
                ->Where(['ID' => $agendamento])
                ->andWhere(['Status' => $this->K_STATUS_ATIVO])
                ->one();

            if ($a != null) {

                $a->Status = $this->K_STATUS_CANCELADO;
                $a->Justificativa = $justificativa;
                $a->GerenteCancelamento = $gerente;
                if ($a->save(false)) {
                    return $this->Valido($a);
                }

            } else {
                return $this->Invalido("Falha ao Cancelar Agendamento.");
            }


        } else {

            return $this->Invalido();
        }

    }


    //Adiciona um produto no agendamento
    public
        function actionAdicionaProdutoAgendamento($agendamento, $produto)
    {
        if ($this->dadosValidos($agendamento) && $this->dadosValidos($produto)) {
            $mcrypt = new MCrypt();
            //            $filial = $mcrypt->decrypt($filial);

            $ap = new AgendamentoProduto();

            $ap->Agendamento = $agendamento;
            $ap->Produto = $produto;
            $ap->Status = $this->K_STATUS_ATIVO;

            if ($ap->save(false)) {
                return $this->Valido($ap);
            } else {
                return $this->Invalido("Falha ao Salvar produto no Agendamento.");
            }

        } else {
            return $this->Invalido();
        }

    }





    /*SELECT *
    FROM   divino.Agendamento
    WHERE  '2017-11-13' >= date(Data)
    AND  '2017-11-09' <= date(Data);*/
    public function actionRelatorioAgendamento($filial, $status = 0, $inicio = "", $fim = "")
    {
        //se status 0 volta todos

        try{

            if ($status == 0 || $status == "0") {
                $lista = Agendamento::find()
                    ->where("Filial = " . $filial
                            . " AND date(Data) <= '" . $fim . "'"
                            . " AND date(Data) >= '" . $inicio . "'"
                            . " AND Gratuito != 1"
                           )
                    ->orderBy('ID ASC')
                    ->all();

            } else {
                $lista = Agendamento::find()
                    ->where("Filial = " . $filial
                            . " AND date(Data) <= '" . $fim . "'"
                            . " AND date(Data) >= '" . $inicio . "'"
                            . " AND Gratuito != 1"
                           )
                    ->andWhere(['Status' => $status])
                    ->orderBy('ID ASC')
                    ->all();
            }






            $table='';
            $table .= $this->cabecalhoAgendamento($filial, $inicio, $fim);

            foreach ($lista as $a) {


                $table .= '<div class="row"  style="background-color:#d1d1d1;text-align:center"; id="border_relatorio"><center><div class="col-md-2"><p style="text-align:center";color:#bc9355;>Cliente</p>';
                $table .= '<span>' .$a->cliente->Nome .'</span></div>';

                $table .= '<div class="col-md-3"> <p style="text-align:center";color:#bc9355;>Telefone</p>';
                $table .= '<span>' .$a->cliente->Telefone. '</span></div>';

                $table .= '<div class="col-md-4"> <p style="text-align:center";color:#bc9355;>Email</p>';
                $table .= '<span>' .$a->cliente->Email. '</span></div>';

                $table .= '<div class="col-md-3"><p style="text-align:center"color:#bc9355;;>Profissional</p>';
                $table .= '<span>' .$a->profissional->Nome. '</span></div>';



                $table .= '</div>';

                $table .= '<div class="row"  style="text-align:center"; id="border_relatorio"><center>';

                $table .= ' <div class="col-md-2"><p style="text-align:center";color:#bc9355;>Gerente</p>';
                $table .= '<span>' .$a->gerente->Nome .'</span></div>';

                $table .= ' <div class="col-md-3"><p style="text-align:center";color:#bc9355;>Data</p>';
                $table .= '<span>' .$a->Data .'</span></div>';

                $table .= ' <div class="col-md-1"><p style="text-align:center";color:#bc9355;>Hora</p>';
                $table .= '<span>' .$a->Hora .'</span></div>';




                $table .= '<div class="col-md-3"><p style="text-align:center";color:#bc9355;>Total</p>';
                $table .= '<span>' .$a->getTotal().' R$</span></div>';

                $table .= ' <div class="col-md-3"><p style="text-align:center";color:#bc9355;>Tempo</p>';
                $table .= '<span>' .$a->getTotalTempo().' Minutos </span></div>';

                $table .= '</div>';

                $table .= '<div class="row" id="border_relatorio" >';






                $table .= $this->converteProdutos($a->produtos);
                $table .= '<br>';

            }

            echo $table;

        }
        catch (Exception $e) {
            echo $e;
            //                return var_dump($c);
            return $this->Invalido("Erro.");
        }

    }



    public function actionEstoque($filial, $status = 0, $inicio = "", $fim = "")
    {
        //se status 0 volta todos

        if ($status == 0 || $status == "0") {
            $lista = Produto::find()
                ->where("Filial = " . $filial)
                ->orderBy('ID ASC')
                ->all();

        } else {
            $lista = Produto::find()
                ->where("Filial = " . $filial)
                ->andWhere(['Status' => $status])
                ->orderBy('ID ASC')
                ->all();
        }







        $table='';


        foreach ($lista as $a) {


            $table .= '<div class="row" id="border_relatorio"  style="text-align:center;"><div class="col-md-3"><center><p style="color:#bc9355; text-align:center;">Produto</p>';
            $table .= '<span style="text-align:center;">' .$a->Nome .'</span></div>';

            $table .= '<div class="col-md-3"  style="text-align:center;"> <p style="color:#bc9355; text-align:center;">Valor</p>';
            $table .= '<span>' .$a->Venda. '</span></div>';

            $table .= '<div class="col-md-3"> <p style="color:#bc9355; text-align:center;">Estoque</p>';
            $table .= '<span style="text-align:center;">' .$a->Estoque. '</span></div>';

            $table .= '<div class="col-md-3"><p style="color:#bc9355; text-align:center;">Filial</p>';
            $table .= '<span style="text-align:center;">' .$a->filial->Nome. '</span></div>';



            $table .= '</div>';


            $table .= '<br>';

        }

        echo $table;

    }


    public function actionRelatorioGratuitoAgendamento($filial, $status = 0, $inicio = "", $fim = "")
    {


        try{



            //se status 0 volta todos

            if ($status == 0 || $status == "0") {
                $lista = Agendamento::find()
                    ->where("Filial = " . $filial
                            . " AND date(Data) <= '" . $fim . "'"
                            . " AND date(Data) >= '" . $inicio . "'"
                           )
                    ->andWhere(['Gratuito' => 1])
                    ->orderBy('ID ASC')
                    ->all();


            } else {
                $lista = Agendamento::find()
                    ->where("Filial = " . $filial
                            . " AND date(Data) <= '" . $fim . "'"
                            . " AND date(Data) >= '" . $inicio . "'"
                           )
                    ->andWhere(['Status' => $status])
                    ->andWhere(['Gratuito' => 1])
                    ->orderBy('ID ASC')
                    ->all();
            }






            $table='';
            $table .= $this->cabecalhoAgendamento($filial, $inicio, $fim);



            foreach ($lista as $a) {




                $table .= '<center>';

                $table .= '<div class="row" style= "text-align:center;" id="border_relatorio"><div class="col-md-3"><p style="color:#bc9355; text-align:center;">Profissional</p>';
                $table .= '<span>' .$a->cliente->Nome .'</span></div>';

                $table .= '<div class="col-md-3"> <p style="color:#bc9355; text-align:center;">Telefone</p>';
                $table .= '<span>' .$a->cliente->Telefone. '</span></div>';

                $table .= '<div class="col-md-3"> <p style="color:#bc9355; text-align:center;">Email</p>';
                $table .= '<span>' .$a->cliente->Email. '</span></div>';

                $table .= '<div class="col-md-3"><p style="color:#bc9355; text-align:center;">Profissional</p>';
                $table .= '<span>' .$a->profissional->Nome. '</span></div>';



                $table .= '</div>';

                $table .= '<div class="row" id="border_relatorio" >';

                $table .= ' <div class="col-md-3"><p style="color:#bc9355; text-align:center;">Gerente</p>';
                $table .= '<span style=" text-align:center;">' .$a->gerente->Nome .'</span></div>';


                $table .= ' <div class="col-md-2"><p style="text-align:center";color:#bc9355;>Data</p>';
                $table .= '<span>' .$a->Data .'</span></div>';

                $table .= ' <div class="col-md-2"><p style="text-align:center";color:#bc9355;>Hora</p>';
                $table .= '<span>' .$a->Hora .'</span></div>';




                $table .= '<div class="col-md-3"><p style="color:#bc9355; text-align:center;">Total</p>';
                $table .= '<span style=" text-align:center;">' .$a->Total .' R$</span></div>';

                $table .= ' <div class="col-md-3"><p style="color:#bc9355; text-align:center;">Tempo</p>';
                $table .= '<span style=" text-align:center;">' .$a->getTotalTempo() .' Minutos </span></div>';

                $table .= '</div>';

                $table .= '<div class="row" id="border_relatorio" >';






                $table .= $this->converteProdutos($a->produtos);
                $table .= '<br>';

            }

            echo $table;

        }



        catch (Exception $e) {
            echo $e;
            //                return var_dump($c);
            return $this->Invalido("Erro ao Salvar. Cliente já cadastrado.");
        }
    }


    public function actionRelatorioBalcao($filial, $status = 0, $inicio = "", $fim = "")
    {
        //se status 0 volta todos

        if ($status == 0 || $status == "0") {

            $lista = Balcao::find()
                ->where("Filial = " . $filial
                        . " AND date(Data) <= '" . $fim . "'"
                        . " AND date(Data) >= '" . $inicio . "'"
                       )
                ->orderBy('ID ASC')
                ->all();



        } else {


            $lista = Balcao::find()
                ->where("Filial = " . $filial
                        . " AND date(Data) <= '" . $fim . "'"
                        . " AND date(Data) >= '" . $inicio . "'"
                       )
                ->andWhere(['Status' => $status])
                ->orderBy('ID ASC')
                ->all();
        }






        $table='';
        $table .= $this->cabecalhoAgendamento($filial, $inicio, $fim);

        foreach ($lista as $a) {
            $table .= '<div class="row" id="border_relatorio" >';

            $table .= '<div class="row" style= "text-align:center;" >';
            $table .= '<div class="col-md-3"> <p style="color:#bc9355; text-align:center;">Data</p>';
            $table .= '<span>' .$a->Data. '</span></div>';

            $table .= '<div class="col-md-3"> <p style="color:#bc9355; text-align:center;">Forma Pagamento</p>';
            $table .= '<span>' .$a->FormaPagamento. '</span></div>';



            $table .= '<div class="col-md-3"><p style="color:#bc9355; text-align:center;">Total</p>';
            $table .= '<span>' .$a->Total. '</span></div>';


            $table .= ' <div class="col-md-3"><p style="color:#bc9355; text-align:center;">Gerente</p>';
            $table .= '<span>' .$a->gerente->Nome .'</span></div>';


            $table .= '</div>';








            $table .= $this->converteProdutos($a->produtos);
            $table .= '<br>';

        }

        echo $table;

    }




    public function actionRelatorioGeral($filial, $status = 0, $inicio = "", $fim = "")


    {

        //se status 0 volta todos
        //agendamento
        if ($status == 0 || $status == "0") {
            $lista = Agendamento::find()
                ->where("Filial = " . $filial
                        . " AND date(Data) <= '" . $fim . "'"
                        . " AND date(Data) >= '" . $inicio . "'"
                       )
                ->orderBy('ID ASC')
                ->all();

        } else {
            $lista = Agendamento::find()
                ->where("Filial = " . $filial
                        . " AND date(Data) <= '" . $fim . "'"
                        . " AND date(Data) >= '" . $inicio . "'"
                       )
                ->andWhere(['Status' => $status])
                ->orderBy('ID ASC')
                ->all();
        }


        //balcao

        if ($status == 0 || $status == "0") {
            $lista2 = Balcao::find()
                ->where("Filial = " . $filial
                        . " AND date(Data) <= '" . $fim . "'"
                        . " AND date(Data) >= '" . $inicio . "'"
                       )
                ->orderBy('ID ASC')
                ->all();

        } else {
            $lista2 = Balcao::find()
                ->where("Filial = " . $filial
                        . " AND date(Data) <= '" . $fim . "'"
                        . " AND date(Data) >= '" . $inicio . "'"
                       )
                ->andWhere(['Status' => $status])
                ->orderBy('ID ASC')
                ->all();
        }



        $_SESSION['Total'] = 0;
        $_SESSION['Gastos'] = 0;
        $_SESSION['Lucros'] = 0;
        $_SESSION['lucro'] = 0;
        $_SESSION['GastosFuncionario'] = 0;
        $_SESSION['valorprod'] = 0;



        $table = $this->cabecalhoAgendamento($filial, $inicio, $fim);

        //agendamento
        foreach ($lista as $a) {




            foreach ($a->produtos as $prod) {

                if  ($prod->produto->Servico==1){


                    $_SESSION['GastosFuncionario'] += $prod->Valor * 0.429;

                    if  ($prod->Gratuito==0){
                        $_SESSION['lucro'] += $prod->Valor * 0.571;

                    }
                    $_SESSION['lucro'] -=  $prod->Custo;
                    $_SESSION['Gastos'] +=  $prod->Custo;
                }
                else{  

                    $_SESSION['GastosFuncionario'] += 2;
                    $_SESSION['lucro'] += $prod->Valor -  $prod->Custo;

                }


                //                    $_SESSION['Total'] = $a->getTotal();

            }

        }


        //balcao
        foreach ($lista2 as $a) {


            foreach ($a->produtos as $prod) {
                $_SESSION['Total'] += $prod->Valor;
                $_SESSION['lucro'] += $prod->Valor -  $prod->Custo;
            }

        }


        $_SESSION['Lucros'] = $_SESSION['lucro'];
        $_SESSION['Gastos'] += $_SESSION['GastosFuncionario'];
        $_SESSION['Total'] =   $_SESSION['Lucros'] +$_SESSION['Gastos'];





        $table = '<div class="row" id="border_relatorio" style="text-align:center;"><div class="col-md-4"><p style="color:#bc9355; text-align:center;">Total</p>';
        $table .= '<span>'  . $_SESSION['Total'] .'R$ </span></div>';

        $table .= '<div class="col-md-4"><p style="color:#bc9355; text-align:center;"">Lucros</p>';
        $table .= '<span>'  . $_SESSION['Lucros'] .'R$ </span></div>';

        $table .= '<div class="col-md-4"><p style="color:#bc9355; text-align:center;">Gastos</p>';
        $table .= '<span>'  . $_SESSION['Gastos'] .'R$ </span></div></div>';



        echo $table;


    }







    public function actionRelatorioProfissional($filial, $profissional = 0, $status = 0, $inicio, $fim)
    {
        //se profissional 0 volta todos

        try{


            if ( $profissional != "0" ) {
                $lista = Profissional::find()
                    ->where("Filial = " . $filial)
                    ->andWhere(['ID' => $profissional   ])
                    ->andWhere(['Status' => $status])
                    ->orderBy('ID ASC')
                    ->all();
            }


            else {

                $lista = Profissional::find()
                    ->where("Filial = " . $filial)
                    ->andWhere(['Status' => $this->K_STATUS_ATIVO])
                    ->orderBy('ID ASC')
                    ->all();

            }







            $table = $this->cabecalhoProfissional($filial);
            $table = '';
            $table2 = '';



            $_SESSION['totalgeral'] = 0;
            $_SESSION['lucrogeral'] = 0;
            $_SESSION['gastos'] = 0;
            $_SESSION['lucrofinal'] = 0;
            $_SESSION['lucrototal'] = 0;






            foreach ($lista as $a) {

                $table .= '<br>';

                $agendamento = null;

                //preciso pegar os valores depois imprimir em baixo da tabela
                $agendamento .=   $this->converteAgendamentosdata($a->agendamentos, $inicio, $fim, true, false) ;


                $table .= '<div style="text-align:center;">    <center>';
                $table .= '<div style="background-color:#d1d1d1;" class="row" id="border_relatorio"/div>';
                $table .= '<div class="col-md-2"><p style="color:#bc9355; text-align:center;">Profissional</p>';
                $table .= '<span>'. $a->Nome .'</span>';
                $table .= '</div>';
                $table .= '<div class="col-md-3"><p style="color:#bc9355; text-align:center;">Telefone</p>';
                $table .= '<span>'. $a->Telefone .'</span>';
                $table .= '</div>';

                $table .= '<div class="col-md-2"><p style="color:#bc9355; text-align:center;">Email</p>';
                $table .= '<span>'. $a->Email .'</span>';
                $table .= '</div>';
                $table .= '<div class="col-md-1"><p style="color:#bc9355; text-align:center;">Total</p>';
                $table .= '<span>'. $_SESSION['TOTAL']  . 'R$</span>';  
                $table .= '</div>';
                $table .= '<div class="col-md-2"><p style="color:#bc9355; text-align:center;">Valor Profisisonal</p>';
                $table .= '<span>'. $_SESSION['TOTALf']  . 'R$</span>';  
                $table .= '</div>';


                $_SESSION['totalgeral'] += $_SESSION['TOTALf'] + $_SESSION['gastos'];
                $_SESSION['lucrogeral'] += $_SESSION['lucrototal']  - $_SESSION['gastos'];
                $_SESSION['lucrofinal'] = $_SESSION['lucrototal']  - $_SESSION['gastos'];
                $table .= '<div class="col-md-2"><p style="text-align:center;">Lucro Profisisonal</p>';
                $table .= '<span>'. $_SESSION['lucrofinal']  . 'R$</span>';  
                $table .= '</div>';

                $table .= '</div>';
                $table .= $agendamento;









            }








            $table2 = '<div class="row" id="border_relatorio" style="color:#bc9355; text-align:center;"><div class="col-md-6"><p style="color:#bc9355; text-align:center;">Gasto</p>';
            $table2 .= '<span>'  . $_SESSION['totalgeral']  .'R$ </span></div>';

            $table2 .= '<div class="col-md-6"><p style="color:#bc9355; text-align:center;">Lucro</p>';
            $table2 .= '<span>'  . $_SESSION['lucrogeral'] .'R$ </span></div></div>';

            echo $table2;

            echo "<br>";
            echo $table;





            echo "<br>";

            echo '<hr>';




        }
        catch (Exception $e) {
            echo $e;
            //                return var_dump($c);
            return $this->Invalido("Erro ao Salvar. Cliente já cadastrado.");
        }


    }



    public function actionProdutoCortesia($filial, $status = 0, $inicio = "", $fim = "")
    {
        //se profissional 0 volta todos

        $table = '';
        $table2 = '';
        $agendamento = '';
        $permite = 0;



        try{

            $lista = Agendamento::find()
                ->where("Filial = " . $filial
                        . " AND date(Data) <= '" . $fim . "'"
                        . " AND date(Data) >= '" . $inicio . "'"
                       )

                ->orderBy('ID ASC')
                ->all();



            //nome


            //se 

            $data = 0;
            $hora = 0;
            $nome = 0;
            $x= 0;
            $cont= 0;


            $table .= '<div class="row" id="border_relatorio" >';
            $table .= '<center >';

            foreach ($lista as $a) {




                foreach ($a->produtos as $p) {

                    if($p->Cortesia==1){

                        $cont ++;



                        $table .= '<div class="col-md-3"><p style="color:#bc9355; text-align:center;">Valor</p>';
                        $table .= '<span >' . $p->Valor .' </span></div>';





                        $table .= '<div class="col-md-3"><p style="color:#bc9355; text-align:center;">Produto</p>';
                        $table .= '<span >' . $p->produto->Nome .' </span></div>';


                        $table .= '<div class="col-md-3"><p style="color:#bc9355; text-align:center;">Data</p>';
                        $table .= '<span >' . $a->Data .' </span></div>';
                        $table .= '<div class="col-md-3"><p style="color:#bc9355; text-align:center;">Hora</p>';
                        $table .= '<span >' . $a->Hora .' </span></div>';
                        $table .= '<div class="col-md-3"><p style="color:#bc9355; text-align:center;">Cliente</p>';
                        $table .= '<span >' . $a->cliente->Nome .' </span></div>';

                    }




                }


            }

            $table .= '</div>';
            $table2 .= '<div class="row" id="border_relatorio"  style="text-align:center"; >';
            $table2 .= '<div class="col-md-3"><p style="color:#bc9355; text-align:center;">Total de Cortesias</p>';
            $table2 .= '<span style="text-align:center";>' . $cont .' </span></div>';
            $table2 .= '</div>';

            echo $table2;

            echo $table;


        }

        catch (Exception $e) {
            echo $e;
            //                return var_dump($c);
            return $this->Invalido("Erro ao Salvar. Cliente já cadastrado.");
        }


    }







    public function actionRelatorioCliente($filial, $inicio, $fim, $status)
    {
        //se status 0 volta todos

        $lista = Cliente::find()
            ->where("Filial = " . $filial
                   )
            ->andWhere(['Status' => $this->K_STATUS_ATIVO])
            ->orderBy('ID ASC')
            ->all();


        $table ='';
        $agendamento ='';


        foreach ($lista as $a) {

            $table .= '<br>';

            $agendamento .=   $this->converteAgendamentos2($a->agendamentos, $inicio, $fim, true, false) ;

            $table .= '<div class="row" id="border_relatorio" >';
            $table .= '<center >';

            $table .= '<div class="col-md-3"><p style="color:#bc9355; text-align:center;">Nome</p>';
            $table .= '<span >' . $a->Nome .' </span></div>';

            $table .= '<div class="col-md-3"><p style="color:#bc9355; text-align:center;">Telefone</p>';
            $table .= '<span>'  . $a->Telefone .'</span></div>';

            $table .= '<div class="col-md-3"><p style="color:#bc9355; text-align:center;">Email</p>';
            $table .= '<span>' . $a->Email .' </span></div>';



            $table .= '<div class="col-md-3"><p style="color:#bc9355; text-align:center;">Filial</p>';
            $table .= '<span>'  .$a->filial->Nome.' </span></div>';


            $table .= '</div>';
            $table .= '</div>';

            $table .= '</center >';
        }


        echo $table;
        echo $agendamento;

    }



    private function cabecalhoAgendamento($filial, $inicio, $fim)
    {
        $f = Filial::find()
            ->where("ID = " . $filial)
            ->one();

        //        $data = (new \DateTime())->format('Y-m-d H:i:s');
        $data = (new \DateTime())->format('Y-m-d');
        $r = '<p>Relatório de Agendamento. Filial: ' . $f->Nome . '. Data: ' . $data . '. Período: ' . $inicio . ' a ' . $fim . '</p>';

        return $r;
    }

    private function cabecalhoProfissional($filial)
    {
        $f = Filial::find()
            ->where("ID = " . $filial)
            ->one();

        $data = (new \DateTime())->format('Y-m-d H:i:s');
        $r = '<p>Relatório de Profissionais. Filial: ' . $f->Nome . '. Data: ' . $data . '</p>';

        return $r;
    }

    private function converteProdutos($produtos)
    {
        $r = '';


        foreach ($produtos as $p) {

            //


            $r .= '<div class="col-md-3" style="text-align:center";>';
            $r .= '<span style="color:#bc9355; text-align:center;">Produto </span>';
            $r .= '<br>';
            $r .= $p->produto->Nome . ' R$' . $p->produto->Venda . ' ';

            $r .= '</div>';
        }

        $r .= '</div>';
        //Json::encode($a->produtos)
        return $r;
    }

    private function converteAgendamentosdata($agendamentos,$inicio, $fim, $cliente = true,  $profissional = true, $status = true)
    {
        $r = '';
        $_SESSION['TOTAL'] = 0;
        $_SESSION['TOTALf'] = 0;
        $_SESSION['lucrop'] = 0;
        $_SESSION['lucrof'] = 0;
        $_SESSION['lucrototal'] = 0;
        $_SESSION['gastos'] =  0;







        foreach ($agendamentos as $a) {

            if($a->Data >=$inicio && $a->Data <=$fim){

                //produtos do agendamento
                //$a->produtos;

                foreach ($a->produtos as $prod) {

                    if  ($prod->produto->Servico==1){
                        $_SESSION['TOTALf'] += $prod->Valor * 0.429;
                        if  ($prod->Gratuito==0){
                            $_SESSION['lucrop'] += $prod->Valor * 0.571;

                        }



                        $_SESSION['gastos'] += $prod->Custo;
                    }
                    if  ($prod->produto->Servico==0){

                        $_SESSION['TOTALf'] += 2;
                        $_SESSION['lucrof'] += $prod->Valor - $prod->Custo;
                    }
                }


                $r .= '<div class="row" id="border_relatorio" style="text-align:center;">';

                //fim valor da comissao do profissional


                if ($cliente){
                    $r .= '<div class="col-md-2"><p style="color:#bc9355; text-align:center;">Cliente</p>';
                    $r .= '<span>';
                    $r .=  $a->cliente->Nome;
                    $r .= '</span>';
                    $r .= '</div>';
                }


                $r .= '<div class="col-md-3"><p style="color:#bc9355; text-align:center;">Produto</p>';
                foreach ($a->produtos as $prod) {

                    if ($cliente){


                        $r .=  $prod->produto->Nome;
                        $r .= ' / ';
                        $r .= 'Valor: ' . $prod->Valor;


                    }


                }
                $r .= '</div>';


                $r .= '<div class="col-md-3"><p style="color:#bc9355; text-align:center;">Forma Pagamento</p>';
                $r .= '<span>'. $a->FormaPagamento .'</span>';
                $r .= '</div>';






                //            $r .= '<p>';
                //            $r .= 'Produtos: ' . $this->converteProdutos($a->produtos);
                //            $r .= '</p>';

                $r .= '<div class="col-md-1"><p style="color:#bc9355; text-align:center;">Total</p>';
                $r .= '<span>';

                $r .=  $a->getTotal();
                $r .= '</span></div>';

                if ($profissional) {
                    $r .= '<div class="col-md-3"><p style="color:#bc9355; text-align:center;">Profissional</p>';
                    $r .= '<span>';
                    $r .= $a->profissional->Nome;
                    $r .= '</span>';
                    $r .= '<div>';
                }




                $_SESSION['TOTAL'] += $a->getTotal();
                //lucro do funcionario para emprea
                $_SESSION['lucrototal'] =  $_SESSION['lucrop'] + $_SESSION['lucrof'];

                $r .= '</div>';



            }
        }


        $r .= '</div>';

        //Json::encode($a->produtos)
        return $r;
    }

    private function converteAgendamentos($agendamentos, $cliente = true,  $profissional = true, $status = true)
    {
        $r = '';
        $_SESSION['TOTAL'] = 0;
        $_SESSION['TOTALf'] = 0;
        $_SESSION['lucrop'] = 0;
        $_SESSION['lucrof'] = 0;
        $_SESSION['lucrototal'] = 0;
        $_SESSION['gastos'] =  0;







        foreach ($agendamentos as $a) {


            //produtos do agendamento
            //$a->produtos;

            foreach ($a->produtos as $prod) {

                if  ($prod->produto->Servico==1){
                    $_SESSION['TOTALf'] += $prod->Valor * 0.429;
                    if  ($prod->Gratuito==0){
                        $_SESSION['lucrop'] += $prod->Valor * 0.571;

                    }


                    $_SESSION['gastos'] += $prod->Custo;
                }
                if  ($prod->produto->Servico==0){

                    $_SESSION['TOTALf'] += 2;
                    $_SESSION['lucrof'] += $prod->Valor - $prod->Custo;
                }
            }


            $r .= '<div class="row" id="border_relatorio" style="color:#bc9355; text-align:center;">';

            //fim valor da comissao do profissional


            if ($cliente){
                $r .= '<div class="col-md-2"><p style="color:#bc9355; text-align:center;">Cliente</p>';
                $r .= '<span>';
                $r .=  $a->cliente->Nome;
                $r .= '</span>';
                $r .= '</div>';
            }


            $r .= '<div class="col-md-3"><p style="color:#bc9355; text-align:center;">Produto</p>';
            foreach ($a->produtos as $prod) {

                if ($cliente){


                    $r .=  $prod->produto->Nome;
                    $r .= ' / ';
                    $r .= 'Valor: ' . $prod->Valor;


                }


            }
            $r .= '</div>';




            //            $r .= '<p>';
            //            $r .= 'Produtos: ' . $this->converteProdutos($a->produtos);
            //            $r .= '</p>';

            $r .= '<div class="col-md-1"><p style="color:#bc9355; text-align:center;">Total</p>';
            $r .= '<span>';
            $r .=  $a->getTotal();
            $r .= '</span></div>';

            if ($profissional) {
                $r .= '<div class="col-md-3"><p style="color:#bc9355; text-align:center;">Profissional</p>';
                $r .= '<span>';
                $r .= $a->profissional->Nome;
                $r .= '</span>';
                $r .= '<div>';
            }




            $_SESSION['TOTAL'] += $a->getTotal();
            //lucro do funcionario para emprea
            $_SESSION['lucrototal'] =  $_SESSION['lucrop'] + $_SESSION['lucrof'];

            $r .= '</div>';




        }

        $r .= '</div>';

        //Json::encode($a->produtos)
        return $r;
    }




    private function converteAgendamentos2($agendamentos, $inicio, $fim, $cliente = true,  $profissional = true, $status = true)
    {
        $r = '';
        $_SESSION['TOTAL'] = 0;
        $_SESSION['TOTALf'] = 0;
        $_SESSION['lucrop'] = 0;
        $_SESSION['lucrof'] = 0;
        $_SESSION['lucrototal'] = 0;
        $_SESSION['gastos'] =  0;






        foreach ($agendamentos as $a) {

            if($a->Data >=$inicio && $a->Data <=$fim){

                //produtos do agendamento
                //$a->produtos;

                foreach ($a->produtos as $prod) {

                    if  ($prod->produto->Servico==1){
                        $_SESSION['TOTALf'] += $prod->Valor * 0.429;
                        if  ($prod->Gratuito==0){
                            $_SESSION['lucrop'] += $prod->Valor * 0.571;

                        }


                        $_SESSION['gastos'] += $prod->Custo;
                    }
                    if  ($prod->produto->Servico==0){

                        $_SESSION['TOTALf'] += 2;
                        $_SESSION['lucrof'] += $prod->Valor - $prod->Custo;
                    }
                }


                $r .= '<div class="row" id="border_relatorio" style="text-align:center;">';

                //fim valor da comissao do profissional




                $r .= '<div class="col-md-3"><p style="color:#bc9355; text-align:center;">Produto</p>';
                foreach ($a->produtos as $prod) {

                    if ($cliente){


                        $r .=  $prod->produto->Nome;
                        $r .= ' / ';
                        $r .= 'Valor: ' . $prod->Valor;


                    }


                }
                $r .= '</div>';




                //            $r .= '<p>';
                //            $r .= 'Produtos: ' . $this->converteProdutos($a->produtos);
                //            $r .= '</p>';

                $r .= '<div class="col-md-3"><p style="color:#bc9355; text-align:center;">Total</p>';
                $r .= '<span>';
                $r .=  $a->getTotal();
                $r .= '</span></div>';



                $r .= '<div class="col-md-3"><p style="color:#bc9355; text-align:center;">Profissional</p>';
                $r .= '<span>';
                $r .=  $a->profissional->Nome;
                $r .= '</span></div>';




                $_SESSION['TOTAL'] += $a->getTotal();
                //lucro do funcionario para emprea
                $_SESSION['lucrototal'] =  $_SESSION['lucrop'] + $_SESSION['lucrof'];

                $r .= '</div>';
            }


        }

        $r .= '</div>';

        $r .= '<hr>';

        //Json::encode($a->produtos)
        return $r;
    }



    private function converteCortesia($agendamentos, $cliente = true,  $profissional = true, $status = true)
    {
        $r = '';



        foreach ($agendamentos as $a) {

            //produtos do agendamento
            //$a->produtos;

            foreach ($a->produtos as $prod) {



                $r .= '<div class="row" id="border_relatorio" style="text-align:center;">';

                //fim valor da comissao do profissional


                if ($cliente){
                    $r .= '<div class="col-md-2"><p style="color:#bc9355; text-align:center;">Cliente</p>';
                    $r .= '<span>';
                    $r .=  $a->cliente->Nome;
                    $r .= '</span>';
                    $r .= '</div>';
                }


                $r .= '<div class="col-md-3"><p style="color:#bc9355; text-align:center;">Produto</p>';
                foreach ($a->produtos as $prod) {

                    if ($cliente){


                        $r .=  $prod->produto->Nome;
                        $r .= ' / ';
                        $r .= 'Valor: ' . $prod->Valor;


                    }


                }
                $r .= '</div>';






                //            $r .= '<p>';
                //            $r .= 'Produtos: ' . $this->converteProdutos($a->produtos);
                //            $r .= '</p>';





                $r .= '</div>';



            }

            $r .= '</div>';

            //Json::encode($a->produtos)
            return $r;
        }
    }



    public function actionComissao($filial)
    {
        $lista = Agendamento::find()
            ->where(['Filial' => $filial])
            ->orderBy('ID ASC')
            ->all();


        $x= "";

        foreach ($lista as $a) {
            //retorna ID do item numero 0


            foreach ($a->produtos as $p){


                echo var_dump ($p->ID);

            }



            //produtos[0]["ID"];
        }
        return $x;

    }


}
