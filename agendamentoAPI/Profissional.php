<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Profissional".
 *
 * @property integer $ID
 * @property string $Nome
 * @property string $Telefone
 * @property string $Email
 * @property string $Nascimento
 * @property string $Usuario
 * @property string $Senha
 * @property integer $Profissao
 * @property integer $Filial
 * @property integer $Status
 * @property string $HoraEntrada
 * @property string $HoraSaida
 */
class Profissional extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Profissional';
    }

    /**
     * @inheritdoc
     */
    public function rules()
        
        
    {
        return [
            [['Nome', 'Telefone', 'Email', 'Nascimento','HoraEntrada','HoraSaida', 'Usuario', 'Senha', 'Profissao','almoco','folga1','folga2', 'Filial', 'DiaDiferenciado'], 'required'],
            [['Profissao', 'Filial', 'Status'], 'integer'],
            [['Nome','HoraEntrada', 'HoraSaida', 'Telefone', 'Email', 'Nascimento', 'Usuario', 'Senha'], 'string', 'max' => 255],
            

            ['Email', 'email'],
      
          
        ];
    


    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Nome' => 'Nome',
            'Telefone' => 'Telefone',
            'Email' => 'Email',
            'Nascimento' => 'Nascimento',
            'Usuario' => 'Usuario',
            'Senha' => 'Senha',
            'Profissao' => 'Profissao',
            'Filial' => 'Filial',
            'Status' => 'Status',
            'Almoco' => 'almoco',
            'Folga1' => 'folga1',
            'HoraEntrada' => 'HoraEntrada',
            'HoraSaida' => 'HoraSaida',
            'Folga2' => 'folga2',
            'DiaDiferenciado' => 'DiaDiferenciado',
        ];
    }

    public function getFilial()
    {
        // a comment has one customer
        return $this->hasOne(Filial::className(), ['ID' => 'Filial']);
    }


    public function getStatus()
    {
        // a comment has one customer
        return $this->hasOne(Status::className(), ['ID' => 'Status']);
    }


    public function getAgendamentos()
    {
        return $this->hasMany(Agendamento::className(), ['Profissional' => 'ID'])->where(['Status' => 7]);
    }


}
