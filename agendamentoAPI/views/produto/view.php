<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Produto */


$this->params['breadcrumbs'][] = ['label' => 'Produtos', 'url' => ['index']];

?>
<div class="produto-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Atualizar', ['update', 'id' => $model->ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Apagar', ['delete', 'id' => $model->ID], [
    'class' => 'btn btn-danger',
    'data' => [
         'confirm' => 'Tem certeza que deseja deletar este Item?',
        'method' => 'post',
    ],
]) ?>
    </p>

    <?= DetailView::widget([
    'model' => $model,
    'attributes' => [

        'Nome',
        'Custo',
        'Venda',
           'Estoque',
      
           'EstoqueMinimo',

        'Servico',
        'Filial',
        'Foto',
        'Status',
    ],
]) ?>

</div>
