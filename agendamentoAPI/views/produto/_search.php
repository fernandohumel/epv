<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProdutoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="produto-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>



    <?= $form->field($model, 'Nome') ?>

    <?= $form->field($model, 'Custo') ?>

    <?= $form->field($model, 'Venda') ?>

    <?= $form->field($model, 'Estoque') ?>

    <?php // echo $form->field($model, 'EstoqueMinimo') ?>

    <?php // echo $form->field($model, 'EstoqueMaximo') ?>

    <?php // echo $form->field($model, 'Categoria') ?>

    <?php // echo $form->field($model, 'Servico') ?>

    <?php // echo $form->field($model, 'Filial') ?>

    <?php // echo $form->field($model, 'Foto') ?>

    <?php // echo $form->field($model, 'Status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
