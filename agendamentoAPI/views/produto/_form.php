<?php

use app\models\Categoria;
use app\models\Produto;
use app\models\Filial;
use app\models\Status;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Produto */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="produto-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Nome')->textInput(['maxlength' => true]) ?>



    <?= $form->field($model, 'Tempo')->textInput() ?>



    <?= $form->field($model, 'Custo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Venda')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Estoque')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'EstoqueMinimo')->textInput(['maxlength' => true]) ?>




    <?=
    $form->field($model, 'Servico')
    ->dropDownList(
    ArrayHelper::map(Categoria::find()->asArray()->all(), 'ID', 'Nome'), ['prompt' => 'Escolha...']
)
    ?>

    
    <? if ($_SESSION['perfil']==1 ||  $_SESSION['perfil']==3){?>

    <?=
    $form->field($model, 'Filial')
    ->dropDownList(
    ArrayHelper::map(Filial::find()->asArray()->all(), 'ID', 'Nome'), ['prompt' => 'Escolha...']
)
    ?>
    <? 
                                  }
    else{?>

    <?=$form->field($model, 'Filial')
        ->dropDownList( 
        array($_SESSION['filial']=>$_SESSION['filial']                 //Flat                  //options
             ));?>
    <?
        }

    ?>



    <?=$form->field($model, 'Status')
    ->dropDownList( 
    array('1'=>'Ativo', '8'=>'Desativado'),                  //Flat 
    ['prompt'=>'Selecione um Status']                  //options
);?>

    <?=$form->field($model, 'Cortesia')
    ->dropDownList( 
    array('1'=>'Sim', '0'=>'Nao'),                  //Flat 
    ['prompt'=>'Selecione um Status']                  //options
);?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Criar' : 'Atualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<style>

</style>
