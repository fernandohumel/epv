<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ProfissionalHorario */

$this->title = 'Create Profissional Horario';
$this->params['breadcrumbs'][] = ['label' => 'Profissional Horarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profissional-horario-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
