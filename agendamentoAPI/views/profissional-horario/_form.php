<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProfissionalHorario */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="profissional-horario-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Data')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Hora')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Profissional')->textInput() ?>

    <?= $form->field($model, 'Status')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
