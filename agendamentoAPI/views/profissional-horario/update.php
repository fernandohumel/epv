<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProfissionalHorario */

$this->title = 'Update Profissional Horario: ' . $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Profissional Horarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="profissional-horario-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
