<?php
/* @var $this \yii\web\View */
/* @var $content string */
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Dvino Admin Panel"/>
    <meta name="keywords"
          content="Admin, Dashboard, Divino, Sass, CSS3, HTML5, Responsive Dashboard, Responsive Admin Template, Admin Template, Best Admin Template, Bootstrap Template, Themeforest"/>
    <meta name="author" content="Fernando Humel"/>
    <link rel="shortcut icon" href="//localhost:8888/agendamento/img/white-logo.png"/>
    <title>Divino Dashboard</title>

    <!-- Common CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css"/>
    <link rel="stylesheet" href="fonts/icomoon/icomoon.css"/>


    <link rel="stylesheet" href="css/bootstrap-theme.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/main.css">
    <link href="jquery-ui/jquery-ui.css" rel="stylesheet">
    <link href="js/jquery.timepicker.css" rel="stylesheet">
    <link href="js/wickedpicker/wickedpicker.min.css" rel="stylesheet">
    <link href="js/fullcalendar/fullcalendar.css" rel="stylesheet">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/main.css"/>

    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>



<!-- BEGIN .app-wrap -->
<!-- BEGIN .app-wrap -->
<div class="app-wrap">
    <!-- BEGIN .app-heading -->
    <header class="app-header">
        <div class="container-fluid">
            <div class="row gutters">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-3 col-4">
                    <a class="mini-nav-btn" href="#" id="app-side-mini-toggler">
                        <i class="icon-chevron-thin-left"></i>
                    </a>
                    <a href="#app-side" data-toggle="onoffcanvas" class="onoffcanvas-toggler" aria-expanded="true">
                        <i class="icon-chevron-thin-left"></i>
                    </a>
                </div>
                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-4">
                    <a href="//localhost:8888/agendamento/pages/index.php" class="logo">

                    </a>
                </div>
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-3 col-4">
                    <ul class="header-actions">

                        <li>

                            <div class="dropdown-menu dropdown-menu-right lg" aria-labelledby="todos">
                                <ul class="stats-widget">
                                    <li>
                                        <h4>$37895</h4>
                                        <p>Revenue <span>+2%</span></p>
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="87"
                                                 aria-valuemin="0" aria-valuemax="100" style="width: 87%">
                                                <span class="sr-only">87% Complete (success)</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <h4>4,897</h4>
                                        <p>Downloads <span>+39%</span></p>
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="65"
                                                 aria-valuemin="0" aria-valuemax="100" style="width: 65%">
                                                <span class="sr-only">65% Complete (success)</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <h4>2,219</h4>
                                        <p>Uploads <span class="text-secondary">-7%</span></p>
                                        <div class="progress">
                                            <div class="progress-bar bg-danger" role="progressbar" aria-valuenow="42"
                                                 aria-valuemin="0" aria-valuemax="100" style="width: 42%">
                                                <span class="sr-only">42% Complete (success)</span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="dropdown">
                            <a href="#" id="userSettings" class="user-settings" data-toggle="dropdown"
                               aria-haspopup="true">
                                <!--                                        <img class="avatar" src="img/user.png" alt="User Thumb" />-->
                                <span class="user-name" id="nome"></span>
                                <i class="icon-chevron-small-down"></i>
                            </a>
                            <div class="dropdown-menu lg dropdown-menu-right" aria-labelledby="userSettings">
                                <!--
                                                                        <ul class="user-settings-list">
                                                                            <li>
                                                                                <a href="profile.html">
                                                                                    <div class="icon">
                                                                                        <i class="icon-account_circle"></i>
                                                                                    </div>
                                                                                    <p>Profile</p>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="profile.html">
                                                                                    <div class="icon red">
                                                                                        <i class="icon-cog3"></i>
                                                                                    </div>
                                                                                    <p>Settings</p>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="filters.html">
                                                                                    <div class="icon yellow">
                                                                                        <i class="icon-schedule"></i>
                                                                                    </div>
                                                                                    <p>Activity</p>
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                -->
                                <div class="logout-btn" id='logout'>
                                    <a  class="btn btn-primary">Logout</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </header>
    <!-- END: .app-heading -->
    <!-- BEGIN .app-container -->
    <div class="app-container">
        <!-- BEGIN .app-side -->
        <aside class="app-side" id="app-side">
            <!-- BEGIN .side-content -->
            <div class="side-content ">
                <!-- BEGIN .user-profile -->
                <div class="user-profile">
                    <a href="//localhost:8888/agendamento/pages/index.php" class="logo">
                        <img src="img/white-logo.png" style="max-height:100px;" width="64px" class="img-responsive"
                             alt="Divino Admin Dashboard"/>
                    </a>
                    <ul class="profile-actions">
                        <li>
                            <a href="#">
                                <i class="fa fa-instagram"></i>

                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="icon-social-facebook"></i>
                            </a>
                        </li>
                        <li>
                            <a id='logout'>
                                <i class="icon-export"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- END .user-profile -->
                <!-- BEGIN .side-nav -->
                <nav class="side-nav">
                    <!-- BEGIN: side-nav-content -->
                    <ul class="unifyMenu" id="unifyMenu">
                        <li>
                            <a href="#" class="has-arrow" aria-expanded="false">
                                        <span class="has-icon">
                                            <i class="icon-laptop_windows"></i>
                                        </span>
                                <span class="nav-title">Dashboards</span>
                            </a>
                            <ul aria-expanded="false">
                                <li>
                                    <a href='//localhost:8888/agendamento/pages/index.php'>Dashboard</a>
                                </li>

                            </ul>

                            <ul aria-expanded="false">

                                <li>
                                    <a href='//localhost:8888/agendamento/pages/calendar.php'>Calendar</a>
                                </li>

                            </ul>

                            <ul aria-expanded="false">

                                <li>
                                    <a href='//localhost:8888/agendamento/pages/balcao.php'
                                       class="current-page">Balcao</a>
                                </li>

                            </ul>
                        </li>


                        <li>


                            <i class="icon-tabs-outline"></i>
                            <i class="icon-center_focus_strong"></i>

                       <br>
                            <a href="#" class="has-arrow" aria-expanded="false">
                                        <span class="has-icon">
                                         <i class="icon-tabs-outline"></i>
                                        </span>
                                <span class="nav-title">Relatorios</span>
                            </a>
                            <ul aria-expanded="false">

                                <li>
                                    <a href='//localhost:8888/agendamento/pages/relatorio_aniversarios.php'>Agendamentos</a>

                                </li>
                                <li>
                                    <a href='//localhost:8888/agendamento/pages/relatorio_aniversarios.php'>Profisisonais</a>

                                </li>
                                <li>
                                    <a href='//localhost:8888/agendamento/pages/relatorio_aniversarios.php'>Total</a>

                                </li>
                                <li>
                                    <a href='//localhost:8888/agendamento/pages/relatorio_aniversarios.php'>Clientes</a>

                                </li>
                                <li>
                                    <a href='//localhost:8888/agendamento/pages/relatorio_aniversarios.php'>Balcao</a>

                                </li>
                                <li>
                                    <a href='//localhost:8888/agendamento/pages/relatorio_aniversarios.php'>Agendamentos Gratuitos</a>

                                </li>

                                <li>
                                    <a href='//localhost:8888/agendamento/pages/relatorio_aniversarios.php'>Produtos Cortesia</a>

                                </li>

                                <li>
                                    <a href='//localhost:8888/agendamento/pages/relatorio_aniversarios.php'>Estoque</a>

                                </li>

                                <li>
                                    <a href='//localhost:8888/agendamento/pages/relatorio_aniversarios.php' >Aniversáriantes</a>

                                </li>


                            </ul>
                        </li>


                        <li class="active selected">
                            <br>
                            <a href="#" class="has-arrow" aria-expanded="false">
                                        <span class="has-icon">
                                             <i class="icon-center_focus_strong"></i>
                                        </span>
                                <span class="nav-title">Cadastros</span>
                            </a>
                            <ul aria-expanded="false" class="collapse in">
                                <li>
                                    <a id='cliente_page'
                                       href='//localhost:8888/agendamentoAPI/web/index.php?r=cliente%2Findex'>Clientes</a>
                                    <a id='filial_page'
                                       href='//localhost:8888/agendamentoAPI/web/index.php?r=filial%2Findex'>Filiais</a>
                                    <a id='produto_page'
                                       href='//localhost:8888/agendamentoAPI/web/index.php?r=produto%2Findex'>Produtos</a>
                                    <a id='profissao_page'
                                       href='//localhost:8888/agendamentoAPI/web/index.php?r=profissao%2Findex'>Profissões</a>

                                    <a id='profissional_page'
                                       href='//localhost:8888/agendamentoAPI/web/index.php?r=profissional%2Findex'>Profissionais</a>
                                    <a id='usuario_page'
                                       href='//localhost:8888/agendamentoAPI/web/index.php?r=usuario%2Findex'>Usuários</a>


                                </li>


                            </ul>
                        </li>
                    </ul>
                    <!-- END: side-nav-content -->
                </nav>
                <!-- END: .side-nav -->
            </div>
            <!-- END: .side-content -->
        </aside>
        <!-- END: .app-side -->
        <!-- END: .app-side -->

        <!-- BEGIN .app-main -->
        <div class="app-main">
            <!-- BEGIN .main-heading -->
            <header class="main-heading">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
                            <div class="page-icon">
                                <i class="icon-laptop_windows"></i>
                            </div>
                            <div class="page-title">
                                <h5>Dashboard</h5>
                                <h6 class="sub-heading">Bem vindo ao Dashboard da Divino</h6>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                            <div class="right-actions">

                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- END: .main-heading -->
            <!-- BEGIN .main-content -->
            <div class="main-content">

                <script type="text/javascript"
                        src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
                <script src="js/vendor/jquery-1.11.2.js"></script>
                <script>window.jQuery || document.write('<script href="js/vendor/jquery-1.11.2.js"><\/script>')</script>
                <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
                <script
                        src="https://code.jquery.com/jquery-3.3.1.js"
                        integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
                        crossorigin="anonymous"></script>
                <!-- Row start -->
                <?php
                $rVariable = trim($_GET["r"]);
                $rVariable = str_replace('%2F', '', $rVariable);
                if (isset($rVariable) && !empty($rVariable)) {
                    if (strpos($rVariable, 'cliente') !== false) {
                        echo "<script>  $('#cliente_page').addClass('current-page');</script>";
                    }
                    if (strpos($rVariable, 'filial') !== false) {
                        echo "<script>  $('#filial_page').addClass('current-page');</script>";
                    }
                    if (strpos($rVariable, 'produto') !== false) {
                        echo "<script>  $('#produto_page').addClass('current-page');</script>";
                    }
                    if (strpos($rVariable, 'profissao') !== false) {
                        echo "<script>  $('#profissao_page').addClass('current-page');</script>";
                    }
                    if (strpos($rVariable, 'profissional') !== false) {
                        echo "<script>  $('#profissional_page').addClass('current-page');</script>";
                    }
                    if (strpos($rVariable, 'usuario') !== false) {
                        echo "<script>  $('#usuario_page').addClass('current-page');</script>";
                    }
                }
                ?>
                <div class="card">
                    <div class="card-body">


                        <?= $content ?>
                    </div>
                </div


            </div>


        </div>
    </div>

    <!-- END: .main-content -->
</div>
<?php $this->endBody() ?>

<script src="js/plugins.js"></script>




<script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
<script src="js/vendor/bootstrap.min.js"></script>


<!-- jQuery first, then Tether, then other JS. -->

<script src="js/tether.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="vendor/unifyMenu/unifyMenu.js"></script>
<script src="vendor/onoffcanvas/onoffcanvas.js"></script>
<script src="js/moment.js"></script>

<!-- Slimscroll JS -->
<script src="vendor/slimscroll/slimscroll.min.js"></script>
<script src="vendor/slimscroll/custom-scrollbar.js"></script>


<script src="jquery-ui/jquery-ui.js"></script>

<script type="text/javascript" src="js/wickedpicker/wickedpicker.min.js"></script>
<script type="text/javascript" src="js/fullcalendar/moment.min.js"></script>

<script type="text/javascript" src="js/fullcalendar/fullcalendar.js"></script>
<script type="text/javascript" src="js/fullcalendar/pt-br.js"></script>
<script type="text/javascript" src="js/fullcalendar/gcal.js"></script>

<script type="text/javascript" src="js/jquery.timepicker.js"></script>


<!-- Common JS -->
<script src="js/common.js"></script>
<script href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"></script>
<script href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script href=" https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>


<style>

    .btn-sucess:hover {
        color: #fff;
        background-color: #5cb85c;
        border-color: #4cae4c;

    }

    .btn-success {
        color: #fff;
        background-color: #449d44;
        border-color: #398439;
    }

    .glyphicon-eye-open {
        color: #218838;
    }

    .glyphicon-pencil {
        color: #e0a800;
    }

    .glyphicon-trash {
        color: #c82333;

    }

    tbody tr {
        background-color: rgba(246, 242, 247, 0.73);
    }

    thead tr {
        background-color: #252835;;
        border: none;
    }


    .table, table-striped, table-bordered{
        border:0px;
        /*border-bottom:;: 1px solid #ddd;*/
    }

    .table-bordered > thead > tr > th{
        border:none;
        border-bottom: 1px solid #ccc;
    }

    .table-bordered > tbody > tr > th{
        border:none;
        border-left: 1px solid #ddd;
    }

    .table-bordered > tfoot > tr > th{
        border:none;
        /*border-bottom:;: 1px solid #ddd;*/
    }

    .table-bordered > thead > tr > td{
        border:none;
        /*border-bottom:;: 1px solid #ddd;*/
    }




    tr th {
        color: white;
    }

    a, tr {
        color: black;
    }

    th {
        color: white;
    }

    .summary {
        color: white;
    }

    th a {
        color: white;
    }

    .control-label {
        color: white;
    }

    #selectsclass {
        height: 200px;
    }
    .btn-primary {
        background-color: #cbac7b;
        border-color: #cbac7b;
        color: #ffffff;
    }

    .btn-primary:hover {
        background-color: #c4a069;
        border-color: #c4a069;
        color: #ffffff;
    }

    h1{
        color:white;
    }

    .form-control{
        padding: 0px;
        margin: 0px;
    }


</style>

<script>




    function setCookie(key, value) {
        var expires = new Date();
        expires.setTime(expires.getTime() + (1 * 24 * 60 * 60 * 1000));
        document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
    }

    function getCookie(key) {
        var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
        return keyValue ? keyValue[2] : null;
    }

    function eraseCookie(name) {
        document.cookie = name + '=; Max-Age=0'
    }


    function logout(){

        eraseCookie("user");

    }



    $('#logout').click(function () {
        logout();
        window.location.replace("//localhost:8888/agendamento/pages/login.php");
    });

    $('#cadastro').click(function () {
        window.location.replace("cadastro.html");
    });
    $('#ferramentas').click(function () {

        window.location.replace("ferramentas.html");
    });
    $('#home').click(function () {
        logout();
        window.location.replace("//localhost:8888/agendamento/pages/index.php");
    });



    // Retrieve
   nome =  localStorage.getItem("nome");
    user =  localStorage.getItem("user");
    perfil =  localStorage.getItem("perfil");
    id =  localStorage.getItem("id");
    idfilial =  localStorage.getItem("idfilial");



    if (nome !== null) {
        $('#nome').text(nome).innerHTML;


//        if (perfil == 1 || perfil == 3) {
//            document.getElementById("filial").style.display = "block";
//        } else {
//            document.getElementById("cadastros_link_nao").style.display = "none";
//        }
//
//        filial(idfilial);
//
//        getFiliais();

    }
    else {

       // window.location.replace("//localhost:8888/agendamento/pages/login.php");
    }


    //funcao onchange filial nome
    function myFunction() {

        var e = document.getElementById("filialchange");
        var value = e.options[e.selectedIndex].value;
        var text = e.options[e.selectedIndex].text;
        //document.getElementById("fili").innerHTML = " Filial = "+ text;
    }

    //funcao retorna nome da filial
    function filial(x) {


        var url = "//localhost:8888/agendamentoAPI/web/index.php?r=divino/filial-cliente&id=" + x;
        var jqxhr = $.get(url
            , function (data) {
                //escrevendo a filial
                // var div = document.getElementById("fili");
                // div.innerText = " Filial = "+ data.result[0].Nome;

                if (data.error === false) {


                }
                else {
                    alert("Humm..... " + JSON.stringify(data));
                }

            })
            .done(function () {

                return;
                //                    alert("second success");
            })
            .fail(function () {
                alert("error getting clientes...");
            })
            .always(function () {
                //                    alert("finished");
            });

        // Perform other work here ...

        // Set another completion function for the request above
        jqxhr.always(function () {
            //                alert("second finished");

        });


    }


    //verifica se é mastert

    //Funcao que retorna filial


    function GetParam(name) {
        var start = location.search.indexOf("?" + name + "=");
        if (start < 0) start = location.search.indexOf("&" + name + "=");
        if (start < 0) return '';
        start += name.length + 2;
        var end = location.search.indexOf("&", start) - 1;
        if (end < 0) end = location.search.length;
        var result = '';
        for (var i = start; i <= end; i++) {
            var c = location.search.charAt(i);
            result = result + (c == '+' ? ' ' : c);
        }
        return unescape(result);
    }


    //        alert(GetParam('usuario'));


    function enviaFilial() {
        var usuario = JSON.parse(getCookie("user"));

        var id = usuario.result[0].ID;

        //            var url = "//localhost:8888/divinoAPI/web/index.php?r=divino/login&user=divino&pin=123";
        var url = "//localhost:8888/agendamentoAPI/web/index.php?r=divino/muda-filial"
            + "&filial=" + $('#filialchange option:selected').val()
            + "&id_do_cara=" + id;


        var jqxhr = $.get(url
            , function (data) {
                // alert("success " + JSON.stringify(data));

                setCookie("filial", JSON.stringify($('#filialchange option:selected').val()));

                //setcookie("usuario.result[0].Perfil;", $('#filialchange option:selected').val());


                //window.location.replace("ferramentas.html");
            })
            .done(function () {
                return;
                //                    alert("second success");
            })
            .fail(function () {
                alert("error");
            })
            .always(function () {
                //                    alert("finished");
            });

        // Perform other work here ...

        // Set another completion function for the request above
        jqxhr.always(function () {
            //                alert("second finished");
        });
    }


    function getFiliais() {

        console.log('chego');

        var url = "//localhost:8888/agendamentoAPI/web/index.php?r=divino/filial";
        var jqxhr = $.get(url
            , function (data) {
                if (data.error === false) {

                    //limpa o combo de clientes
                    $('#filialchange').empty();


                    //percorre array de usuarios
                    for (var i = 0, len = data.result.length; i < len; i++) {


                        var filial = JSON.parse(getCookie("filial"));


                        if (filial == data.result[i].ID) {
                            $('#filialchange').append('<option value="' + data.result[i].ID + '" selected="selected">' + data.result[i].Nome + '</option>');
                        } else {
                            $('#filialchange').append('<option value="' + data.result[i].ID + '" ">' + data.result[i].Nome + '</option>');

                        }


                    }


                }
                else {
                    alert("Humm..... " + JSON.stringify(data));
                }

            })
            .done(function () {
                //                    alert("second success");
            })
            .fail(function () {
                alert("error getting clientes...");
            })
            .always(function () {
                //                    alert("finished");
            });

        // Perform other work here ...

        // Set another completion function for the request above
        jqxhr.always(function () {
            //                alert("second finished");
        });

    }


    $('#filialchange').change(function () {
        enviaFilial();

    });

</script>

</body>
</html>
<?php $this->endPage() ?>


