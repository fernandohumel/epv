<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Profissional */

$this->title = 'Criar Profissional';
$this->params['breadcrumbs'][] = ['label' => 'Profissionals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profissional-create">

   <center> <h1><?= Html::encode($this->title) ?></h1></center>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
