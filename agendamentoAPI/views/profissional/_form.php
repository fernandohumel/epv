<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Filial;
use app\models\Profissao;

use yii\widgets\TimePicker;


/*   <?= $form->field($model, 'Telefone')->widget(\yii\widgets\MaskedInput::className(), [
    'mask' => '(99) 9-9999-9999',
]) ?>

    <?= $form->field($model, 'Nascimento', 'id' =>'Nascimento' )->widget(\yii\jui\DatePicker::className(), [
    //'language' => 'ru',
    'dateFormat' => 'yyyy-MM-dd',
]) ?>



/* @var $this yii\web\View */
/* @var $model app\models\Profissional */
/* @var $form yii\widgets\ActiveForm */
?>
 <script type="text/javascript" src="js/jquery.timepicker.js"></script>
<link href="js/jquery.timepicker.css" rel="stylesheet">
<script src="jquery-ui/jquery-ui.js"></script>
<link href="jquery-ui/jquery-ui.css" rel="stylesheet">
<div class="profissional-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Nome')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Telefone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Nascimento')->textInput(['maxlength' => true, 'id' => 'datepicker', 'class' => 'datepicker']) ?>


    <?= $form->field($model, 'HoraEntrada')->textInput(['maxlength' => true, 'id' => 'timepicker', 'class' => 'timepicker']) ?>

    <?= $form->field($model, 'HoraSaida')->textInput(['maxlength' => true, 'id' => 'timepicker1', 'class' => 'timepicker']) ?>


    <script>



        $('.timepicker').timepicker({
            'interval': 40,
            'step': '40',
            'timeFormat': 'H:i',
            'minTime': '9:00am',
            'maxTime': '9:00pm',
            'scrollDefault': 'now'

        });


        $('.timepicker1').timepicker({
            'interval': 40,
            'step': '40',
            'timeFormat': 'H:i',
            'minTime': '9:00am',
            'maxTime': '9:00pm',
            'scrollDefault': 'now'

        });

        

        $(".datepicker").datepicker({
            showOtherMonths: true,
            changeMonth: true,
            changeYear: true,
            selectOtherMonths: true,
            yearRange: "1920:2018",
            dateFormat: "yy-mm-dd",
            defaultDate: '1980-01-01'
        });
    </script>

    <?= $form->field($model, 'Usuario')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Senha')->textInput(['maxlength' => true]) ?>

    <? if ($_SESSION['perfil']==1 ||  $_SESSION['perfil']==3){?>

    <?=
    $form->field($model, 'Filial')
    ->dropDownList(
    ArrayHelper::map(Filial::find()->asArray()->all(), 'ID', 'Nome'), ['prompt' => 'Escolha...']
)
    ?>
    <? 
                                  }
    else{?>

    <?=$form->field($model, 'Filial')
        ->dropDownList( 
        array($_SESSION['filial']=>$_SESSION['filial']                 //Flat                  //options
             ));?>
    <?
        }

    ?>
    <?=
    $form->field($model, 'Profissao')
    ->dropDownList(
    ArrayHelper::map(Profissao::find()->asArray()->all(), 'ID', 'Nome'), ['prompt' => 'Escolha...']
)
    ?>

    <?=$form->field($model, 'almoco')
    ->dropDownList( 
    array('11:40'=>'11:40 as 13:00', '12:20'=>'12:20 as 13:40', '13:00'=>'13:00 as 14:20'),                   //Flat 
    ['prompt'=>'Selecione um Horario']                  //options
);?>

    
    
    
        <?=$form->field($model, 'DiaDiferenciado')
    ->dropDownList( 
    array('0'=>'Domingo', '6'=>'Sábado', '5'=>'Sexta', '4'=>'Quinta', '3'=>'Quarta', '2'=>'Terça', '1'=>'Segunda'),                     //Flat 
    ['prompt'=>'Escolha o dia Diferenciado']                  //options
);?>
    

    <?=$form->field($model, 'folga1')
    ->dropDownList( 
    array('0'=>'Domingo', '6'=>'Sábado', '5'=>'Sexta', '4'=>'Quinta', '3'=>'Quarta', '2'=>'Terça', '1'=>'Segunda'),                     //Flat 
    ['prompt'=>'Escolha a folga1']                  //options
);?>


    <?=$form->field($model, 'folga2')
    ->dropDownList( 
    array('0'=>'Domingo', '6'=>'Sábado', '5'=>'Sexta', '4'=>'Quinta', '3'=>'Quarta', '2'=>'Terça', '1'=>'Segunda'),                  //Flat 
    ['prompt'=>'Escolha a folga2']                  //options
);?>
    
    
    

    <?=$form->field($model, 'Status')
    ->dropDownList( 
    array('1'=>'Ativo', '8'=>'Desativado'),                  //Flat 
    ['prompt'=>'Selecione um Status']                  //options
);?>





    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Criar' : 'Atualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
