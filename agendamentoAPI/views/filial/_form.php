<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Filial;
use app\models\Usuario;

/* @var $this yii\web\View */
/* @var $model app\models\Filial */
/* @var $form yii\widgets\ActiveForm */




?>

<div class="filial-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Nome')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Telefone')->textInput(['maxlength' => true]) ?>

    <?=
    $form->field($model, 'Gerente')
        ->dropDownList(
            ArrayHelper::map(Usuario::find()->asArray()->all(), 'ID', 'Nome'), ['prompt' => 'Escolha...']
        )
    ?>



    <?=$form->field($model, 'Status')
    ->dropDownList( 
    array('1'=>'Ativo', '8'=>'Desativado'),                  //Flat 
    ['prompt'=>'Selecione um Status']                  //options
);?>
    


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Criar' : 'Atualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
