<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UsuarioSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="usuario-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>



    <?= $form->field($model, 'Nome') ?>

    <?= $form->field($model, 'Telefone') ?>

    <?= $form->field($model, 'Email') ?>

    <?= $form->field($model, 'Usuario') ?>

    <?php // echo $form->field($model, 'Senha') ?>

    <?php // echo $form->field($model, 'Nascimento') ?>

    <?php // echo $form->field($model, 'Filial') ?>

    <?php // echo $form->field($model, 'Perfil') ?>

    <?php // echo $form->field($model, 'Status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
