<?php
use app\models\Filial;
use app\models\Status;
use app\models\Perfil;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Usuario */
/* @var $form yii\widgets\ActiveForm */
?>
<script type="text/javascript" src="js/jquery.timepicker.js"></script>
<link href="js/jquery.timepicker.css" rel="stylesheet">
<script src="jquery-ui/jquery-ui.js"></script>
<link href="jquery-ui/jquery-ui.css" rel="stylesheet">
<div class="usuario-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Nome')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Telefone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Usuario')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Senha')->textInput(['maxlength' => true]) ?>

           <?= $form->field($model, 'Nascimento')->textInput(['maxlength' => true, 'id' => 'datepicker', 'class' => 'datepicker']) ?>


    <script>
        $(".datepicker").datepicker({
            showOtherMonths: true,
            changeMonth: true,
            changeYear: true,
            selectOtherMonths: true,
            yearRange: "1920:2018",
            dateFormat: "yy-mm-dd",
            defaultDate: '1980-01-01'
        });
</script>

    <? if ($_SESSION['perfil']==1){?>

    <?=
    $form->field($model, 'Perfil')
    ->dropDownList(
    ArrayHelper::map(Perfil::find()->asArray()->all(), 'ID', 'Nome'), ['prompt' => 'Escolha...']
)
    ?>
    <? 
                                  }
    else{?>

    <?=$form->field($model, 'Perfil')
        ->dropDownList( 
        array('2'=>'Gerente'),                   //Flat 
        ['prompt'=>'Selecione um Perfil']                  //options
    );?><?
    }

    ?>




    <?=
        $form->field($model, 'Filial')
        ->dropDownList(
        ArrayHelper::map(Filial::find()->asArray()->all(), 'ID', 'Nome'), ['prompt' => 'Escolha...']
    )
    ?>
   

    <?=$form->field($model, 'Status')
    ->dropDownList( 
    array('1'=>'Ativo', '8'=>'Desativado'),                  //Flat 
    ['prompt'=>'Selecione um Status']                  //options
);?>
    
    


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Criar' : 'Atualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
