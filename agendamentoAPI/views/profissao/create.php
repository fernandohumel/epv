<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Profissao */

$this->title = 'Criar Profissão';
$this->params['breadcrumbs'][] = ['label' => 'Profissaos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profissao-create">

    <center><h1><?= Html::encode($this->title) ?></h1></center>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
