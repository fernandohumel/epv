<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Profissao */

$this->title = 'Atualizar Profissão' ;
$this->params['breadcrumbs'][] = ['label' => 'Profissaos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profissao-update">

   <center> <h1><?= Html::encode($this->title) ?></h1></center>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
