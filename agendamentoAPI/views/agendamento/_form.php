<?php


use app\models\Cliente;
use app\models\Profissional;
use app\models\Filial;
use app\models\Usuario;
use app\models\Status;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Agendamento */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="agendamento-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Data')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Hora')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Total')->textInput(['maxlength' => true]) ?>

    <?=
    $form->field($model, 'Cliente')
    ->dropDownList(
    ArrayHelper::map(Cliente::find()->asArray()->all(), 'ID', 'Nome'), ['prompt' => 'Escolha...']
)
    ?>

       <?=
    $form->field($model, 'Profissional')
    ->dropDownList(
    ArrayHelper::map(Profissional::find()->asArray()->all(), 'ID', 'Nome'), ['prompt' => 'Escolha...']
)
    ?>

       <?=
    $form->field($model, 'Gerente')
    ->dropDownList(
    ArrayHelper::map(Usuario::find()->asArray()->all(), 'ID', 'Nome'), ['prompt' => 'Escolha...']
)
    ?>


    <?=
    $form->field($model, 'Filial')
    ->dropDownList(
    ArrayHelper::map(Filial::find()->asArray()->all(), 'ID', 'Nome'), ['prompt' => 'Escolha...']
)
    ?>


    <?=
    $form->field($model, 'Status')
    ->dropDownList(
    ArrayHelper::map(Status::find()->asArray()->all(), 'ID', 'Nome'), ['prompt' => 'Escolha...']
)
    ?>






    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
