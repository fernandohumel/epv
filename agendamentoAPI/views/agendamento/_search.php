<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AgendamentoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="agendamento-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'Data') ?>

    <?= $form->field($model, 'Hora') ?>

    <?= $form->field($model, 'Total') ?>

    <?= $form->field($model, 'Cliente') ?>

    <?php // echo $form->field($model, 'Profissional') ?>

    <?php // echo $form->field($model, 'Gerente') ?>

    <?php // echo $form->field($model, 'GerenteCancelamento') ?>

    <?php // echo $form->field($model, 'Filial') ?>

    <?php // echo $form->field($model, 'Status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
