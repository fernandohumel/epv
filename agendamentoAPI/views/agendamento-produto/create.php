<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AgendamentoProduto */

$this->title = 'Create Agendamento Produto';
$this->params['breadcrumbs'][] = ['label' => 'Agendamento Produtos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agendamento-produto-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
