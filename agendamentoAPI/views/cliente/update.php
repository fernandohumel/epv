<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Cliente */

$this->title = 'Atualizar Cliente';
$this->params['breadcrumbs'][] = ['label' => 'Clientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];

?>
<div class="cliente-update">

    <center><h1><?= Html::encode($this->title) ?></h1></center>
    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
