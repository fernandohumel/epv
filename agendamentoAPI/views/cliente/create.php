<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Cliente */

$this->title = 'Criar Cliente';
$this->params['breadcrumbs'][] = ['label' => 'Clientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cliente-create">


   <center> <h1><?= Html::encode($this->title) ?></h1></center>
    
       
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
