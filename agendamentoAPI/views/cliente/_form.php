<?php
use app\models\Filial;
use app\models\Status;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Cliente */
/* @var $form yii\widgets\ActiveForm */
?>
<script src="divinobarbearia.com.br/jquery-ui/jquery-ui.js"></script>
<link href="divinobarbearia.com.br/jquery-ui/jquery-ui.css" rel="stylesheet">
<link rel="stylesheet" href="divinobarbearia.com.br/css/bootstrap-theme.css">
<link rel="stylesheet" href="divinobarbearia.com.br/css/bootstrap.css">
<link rel="stylesheet" href="divinobarbearia.com.br/css/main.css">


<script>
    $(".datepicker").datepicker({
        showOtherMonths: true,
        changeMonth: true,
        changeYear: true,
        selectOtherMonths: true,
        yearRange: "1920:2018",
        dateFormat: "yy-mm-dd",
        defaultDate: '1980-01-01'

    });
</script>


<?php $form = ActiveForm::begin(); ?>

<div class="col-md-12">

<div class="col-md-3">


</div>


    <div class="col-md-6">


            <?= $form->field($model, 'Nome')->textInput(['maxlength' => true]) ?>


            <?= $form->field($model, 'Telefone')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'CPF')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'Aniversario')->textInput(['maxlength' => true, 'tabindex' => '-1', 'id' => 'datepicker']) ?>


            <?= $form->field($model, 'Email')->textInput(['maxlength' => true]) ?>


            <?= $form->field($model, 'Usuario')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'Senha')->textInput(['maxlength' => true]) ?>
            <? if ($_SESSION['perfil'] == 1 || $_SESSION['perfil'] == 3){ ?>

            <?= $form->field($model, 'Filial')->dropDownList(ArrayHelper::map(Filial::find()->asArray()->all(), 'ID', 'Nome'), ['prompt' => 'Escolha...']) ?>
            <?
            }
            else{
            ?>

            <?= $form->field($model, 'Filial', ['options' => ['class' => 'selectsclass']])->dropDownList(array($_SESSION['filial'] => $_SESSION['filial']                 //Flat                  //options
                )); ?>
            <?
            }
            ?>


            <?= $form->field($model, 'Status',  ['options' => ['class' => 'selectsclass']])->dropDownList(array('1' => 'Ativo', '8' => 'Desativado'),                  //Flat
                    ['prompt' => 'Selecione um Status']                  //options
                ); ?>


            <?= $form->field($model, 'Bonus')->textInput(['maxlength' => true]) ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Criar' : 'Atualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>




</div>


    <?php ActiveForm::end(); ?>


