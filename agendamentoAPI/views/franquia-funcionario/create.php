<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FranquiaFuncionario */

$this->title = 'Create Franquia Funcionario';
$this->params['breadcrumbs'][] = ['label' => 'Franquia Funcionarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="franquia-funcionario-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
