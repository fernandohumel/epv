
<?php

/* @var $this yii\web\View */

use yii\helpers\Html;



$content_type = 'application/json';
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE");
header("Access-Control-Allow-Headers: Authorization");
header('Content-type: ' . $content_type);





//$this->title = '';
?>
<div class="dashboard-index">

    <script type="text/javascript" src="js/jquery.timepicker.js"></script>
    <link href="js/jquery.timepicker.css" rel="stylesheet">
    <script src="jquery-ui/jquery-ui.js"></script>
    <link href="jquery-ui/jquery-ui.css" rel="stylesheet">




    <!-- ezequiel 1-->

    <div class="containe-fluid">
        <div class="cadastro" style="margin-left:5%;margin-right:5%;">


            <? if ($_SESSION["perfil"]==1){?> 
            <!-- Example row of columns -->
            <div class="row">

                <div class="col-md-4" >
                    <a href="//localhost:8888/agendamentoAPI/web/index.php?r=cliente%2Findex">
                        <div class="fundo">  
                            Clientes
                        </div>
                    </a>  
                </div>
                <div class="col-md-4">
                    <a href="//localhost:8888/agendamentoAPI/web/index.php?r=filial%2Findex">
                        <div class="fundo">  
                            Filiais
                        </div>
                    </a>  
                </div>
                <div class="col-md-4">  
                    <a href="//localhost:8888/agendamentoAPI/web/index.php?r=produto%2Findex">
                        <div class="fundo">  
                            Produtos
                        </div>
                    </a>  
                </div>
                <div class="col-md-4"> 
                    <a href="//localhost:8888/agendamentoAPI/web/index.php?r=profissao%2Findex">
                        <div class="fundo">  
                            Profissões
                        </div>
                    </a>  
                </div>

                <div class="col-md-4">  
                    <a href="//localhost:8888/agendamentoAPI/web/index.php?r=profissional%2Findex">
                        <div class="fundo">  
                            Profissionais
                        </div>
                    </a>  
                </div>
                <div class="col-md-4">  
                    <a href="//localhost:8888/agendamentoAPI/web/index.php?r=usuario%2Findex">
                        <div class="fundo">  
                            Usuários
                        </div>
                    </a>  
                </div>



                <?}?>


                <!-- gerente 0-->
                <? if ($_SESSION["perfil"]==2){?> 

                <div class="col-md-4">
                    <a href="//localhost:8888/agendamentoAPI/web/index.php?r=cliente%2Findex">
                        <div class="fundo">  
                            Clientes
                        </div>
                    </a>    
                </div>

                <div class="col-md-4">  
                    <a href="//localhost:8888/agendamentoAPI/web/index.php?r=produto%2Findex">
                        <div class="fundo">  
                            Produtos
                        </div>
                    </a>  
                </div>

                <div class="col-md-4">  
                    <a href="//localhost:8888/agendamentoAPI/web/index.php?r=profissional%2Findex">
                        <div class="fundo">  
                            Profissionais
                        </div>
                    </a>  
                </div>

                <?}?>

                <? if ($_SESSION["perfil"]==3){?> 

                <div class="col-md-4" >
                    <a href="//localhost:8888/agendamentoAPI/web/index.php?r=cliente%2Findex">
                        <div class="fundo">  
                            Clientes
                        </div>
                    </a>  
                </div>
                <div class="col-md-4">
                    <a href="//localhost:8888/agendamentoAPI/web/index.php?r=filial%2Findex">
                        <div class="fundo">  
                            Filiais
                        </div>
                    </a>  
                </div>
                <div class="col-md-4">  
                    <a href="//localhost:8888/agendamentoAPI/web/index.php?r=produto%2Findex">
                        <div class="fundo">  
                            Produtos
                        </div>
                    </a>  
                </div>
                <div class="col-md-4"> 
                    <a href="//localhost:8888/agendamentoAPI/web/index.php?r=profissao%2Findex">
                        <div class="fundo">  
                            Profissões
                        </div>
                    </a>  
                </div>

                <div class="col-md-4">  
                    <a href="//localhost:8888/agendamentoAPI/web/index.php?r=profissional%2Findex">
                        <div class="fundo">  
                            Profissionais
                        </div>
                    </a>  
                </div>
                <div class="col-md-4">  
                    <a href="//localhost:8888/agendamentoAPI/web/index.php?r=usuario%2Findex">
                        <div class="fundo">  
                            Usuários
                        </div>
                    </a>  
                </div>


            </div>
        </div>
    </div>

    <?}?>
    <!--    --><? //= Html::a('Profile', ['user/view', 'id' => $id], ['class' => 'profile-link']) ?>
    <!--    --><? //= Html::a('Profile', ['user/view', 'id' => $id], ['class' => 'profile-link']) ?>


</div>


<script language="javascript" type="text/javascript">

    $(document).ready(function () {

        var usuario = JSON.parse(getCookie("user"));
        //        var usuario = JSON.parse(GetParam('usuario'));
        //        alert(usuario[0].Nome);


        //se nao tiver o cookie setado eh pq user nao ta logado e volta pra index
        if (usuario !== null && parseInt(usuario.result[0].ID) > 0) {
            $('#nome').text(usuario.result[0].Nome);
        }
        else {
            window.location.replace("index.html");
        }
    });

    function logout(){

        eraseCookie("user");

    }


    $('#logout').click(function () {
        logout();
        window.location.replace("index.html");

    });


</script>
