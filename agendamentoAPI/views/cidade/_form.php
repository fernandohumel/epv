<?php

use app\models\Estado;
use app\models\Status;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Cidade */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cidade-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Nome')->textInput(['maxlength' => true]) ?>

    <?=
    $form->field($model, 'Estado')
        ->dropDownList(
            ArrayHelper::map(Estado::find()->asArray()->all(), 'ID', 'Nome'), ['prompt' => 'Escolha...']
        )
    ?>
    <?=
    $form->field($model, 'Status')
        ->dropDownList(
            ArrayHelper::map(Status::find()->asArray()->all(), 'ID', 'Nome'), ['prompt' => 'Escolha...']
        )
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Criar' : 'Atualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
