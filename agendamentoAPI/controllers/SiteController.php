<?php

namespace app\controllers;

use Yii;
use yii\bootstrap\Alert;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;



class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {

        // Turn off all error reporting
        error_reporting(0);

        //        if (!Yii::$app->user->isGuest) {
//            return $this->goHome();
//
//        }


        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post())) {

            $post = Yii::$app->request->post();

            $username = $post["LoginForm"]["username"];
            $pin = $post["LoginForm"]["password"];

            //set POST variables
            $url = 'http://speedvisionh.com.br/OPAY/user.php?request=login';

            $params = array(
                'login' => urlencode($username),
                'pin' => urlencode($pin)

            );


            $retorno = $this->httpPost($url, $params);

            $json = \GuzzleHttp\json_decode($retorno);


            //teve errors
            if ($json->error == "true") {


                //"{"error":"true","message":"Nenhum usu\u00e1rio encontrado com esses dados","infor":{"tipoError":"usuarioNaoEncontrado"}}"

                return $this->render('login', [
                    'model' => $model,
                    'json' => $json
                ]);

            } //se nao teve erro, achou user e prossegue proxima tela
            else {

                //verifica se eh tipo loja
                //{ ["error"]=> string(5) "false" ["message"]=> string(0) "" ["infor"]=> object(stdClass)#86 (4) { ["tokenUser"]=> string(31) "8E9060ADACA043F26230CDF944CD123" ["user"]=> string(7) "winnex5" ["tipoUser"]=> string(4) "loja" ["nomeUser"]=> string(9) "Winnex+ 5" } }
                if ($json->infor->tipoUser == "loja") {

//                    Yii::$app->runAction('dashboard/opaydashboard');
//                     return Yii::$app->runAction('dashboard/opaydashboard', ['param1'=>'value1', 'param2'=>'value2']);


//                    $cat = Yii::$app->createController('dashboard');//returns array containing controller instance and action index.
//                    $cat = $cat[0]; //get the controller instance.
//                    return $cat->actionOpaydashboard($json); //use a public method.

//                    Yii::$app->response->redirect(['dashboard/opaydashboard', 'login' => $retorno]);

                    return Yii::$app->response->redirect(Url::to(['dashboard/opaydashboard', 'token' => $json->infor->tokenUser, 'user' => $json->infor->user]));

                    //Yii::$app->request->redirect(Yii::$app->createAbsoluteUrl("dashboard/opaydashboard"), ['login' => $json]);


                } else {
                    echo "apenas usuários tipo loja podem usar o dashboard.... sinto muito...";
                }


            }

        } else {
            return $this->render('login', [
                'model' => $model,
                'json' => null
            ]);
        }


    }


    function httpPost($url, $params)
    {
        $postData = '';
        //create name value pairs seperated by &
        foreach ($params as $k => $v) {
            $postData .= $k . '=' . $v . '&';
        }
        $postData = rtrim($postData, '&');

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, count($postData));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

        $output = curl_exec($ch);

        curl_close($ch);
        return $output;

    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }


}
