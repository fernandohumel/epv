<?php

namespace app\controllers;

use app\models\Historico;

use app\models\RetornoAPI;
use MCrypt;
use yii\web\Controller;


include "../MCrypt.php";

//
//
//
//$content_type = 'application/json';
//header("Access-Control-Allow-Origin: *");
//header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE");
//header("Access-Control-Allow-Headers: Authorization");
//header('Content-type: ' . $content_type);
//
//
//$http_origin = $_SERVER['HTTP_ORIGIN'];
//
//$allowed_domains = array(
//    '*',
//    '//localhost',
//    '//localhost:8888/agendamentoAPI',
//);
//
//if (in_array($http_origin, $allowed_domains))
//{
//    header("Access-Control-Allow-Origin: $http_origin");
//}
//


//local
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type");






class MyController extends Controller
{


    public $modelClass = 'app\models\Usuarios';


    public $K_STATUS_ATIVO = 1;
    public $K_STATUS_DESATIVADO = 8;
    public $K_STATUS_CANCELADO = 6;
    public $K_STATUS_FINALIZADO = 7;

    //Usados no RetornoAPI
    public $K_TYPE_STRING = "STRING";
    public $K_TYPE_BOOLEAN = "BOOLEAN";
    public $K_TYPE_ARRAY = "ARRAY";
    public $K_TYPE_OBJECT = "OBJECT";



    private function tokenAuthentication($token)
    {
        $historico = new Historico();
        //$historico->IP = Yii::app()->request->getUserHostAddress();
        $historico->IP = $this->getRealIpAddr();
        $historico->Extra = "token: " . $token . ";function:tokenAuthentication;";
        $historico->save(false);

        //TODO Autenticar com token as transacoes
        return true;
    }


    function getRealIpAddr()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) //to check ip is pass from proxy
        {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    //gera um ID unico para ser utilizado no sistema
    function generate_uuid()
    {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
                       mt_rand(0, 0xffff), mt_rand(0, 0xffff),
                       mt_rand(0, 0xffff),
                       mt_rand(0, 0x0C2f) | 0x4000,
                       mt_rand(0, 0x3fff) | 0x8000,
                       mt_rand(0, 0x2Aff), mt_rand(0, 0xffD3), mt_rand(0, 0xff4B)
                      );

    }

    function dadosValidos($dados)
    {

        if (!is_null($dados) && strlen($dados) > 0)
            return true;
        else
            return false;


    }


    function getNextToken($token)
    {


        if ($this->tokenAuthentication($token)) {
            //precisa achar o usuario com o token atual

            //precisa gerar um novo token para este usuario

            //precisa gravar token novo na base de dados para proxima transacao

            return $this->generate_uuid();
        } else {
            return "";
        }
    }


    function Valido($dados = "", $tipo = "OBJECT")
    {
        $rAPI = new RetornoAPI();
        $rAPI->error = false;
        $rAPI->errorMessage = '';
        $rAPI->type = $tipo;
        $rAPI->result = $dados;
        $rAPI->nextToken = $this->getNextToken('');
        return $rAPI->getJsonResult(true);
    }


    function Invalido($dados = "")
    {
        $rAPI = new RetornoAPI();
        $rAPI->error = true;
        $rAPI->errorMessage = 'Dados Invalidos. ' . $dados;
        $rAPI->type = $this->K_TYPE_STRING;
        $rAPI->result = '';
        $rAPI->nextToken = $this->getNextToken('');
        return $rAPI->getJsonResult(true);
    }

    //funcao generica para enviar um POST

    function httpPost($url, $params)
    {
        $postData = '';
        //create name value pairs seperated by &
        foreach ($params as $k => $v) {
            $postData .= $k . '=' . $v . '&';
        }
        $postData = rtrim($postData, '&');

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, count($postData));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

        $output = curl_exec($ch);

        curl_close($ch);
        return $output;

    }


    /**
     * List of allowed domains.
     * Note: Restriction works only for AJAX (using CORS, is not secure).
     *
     * @return array List of domains, that can access to this API
     */
    public static function allowedDomains()
    {
        return [
            '*',                        // star allows all domains
            //            'http://test1.example.com',
            //            'http://test2.example.com',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [

            // For cross-domain AJAX request
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    // restrict access to domains:
                    'Origin' => static::allowedDomains(),
                    'Access-Control-Request-Method' => ['POST', 'GET', 'PUT', 'DELETE', 'OPTIONS'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age' => 86400,                 // Cache (seconds)
                    'Access-Control-Allow-Headers' => ['Origin', 'Content-Type', 'X-Auth-Token', 'Authorization'],                 // Cache (seconds)
                ],
            ],

        ]);
    }


}