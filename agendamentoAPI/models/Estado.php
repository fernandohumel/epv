<?php

namespace app\models;

use Yii;

/**
* This is the model class for table "Estado".
*
    * @property integer $ID
    * @property string $Nome
    * @property integer $Status
*/
class Estado extends \yii\db\ActiveRecord
{
/**
* @inheritdoc
*/
public static function tableName()
{
return 'Estado';
}

/**
* @inheritdoc
*/
public function rules()
{
return [
            [['Nome'], 'required'],
            [['Status'], 'integer'],
            [['Nome'], 'string', 'max' => 255],
        ];
}

/**
* @inheritdoc
*/
public function attributeLabels()
{
return [
    'ID' => 'ID',
    'Nome' => 'Nome',
    'Status' => 'Status',
];
}
}
