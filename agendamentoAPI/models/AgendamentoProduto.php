<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "AgendamentoProduto".
 *
 * @property integer $ID
 * @property integer $Agendamento
 * @property integer $Produto
 * @property integer $Status
 */
class AgendamentoProduto extends Modelos
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'AgendamentoProduto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Agendamento', 'Produto', 'Valor','Custo'], 'required'],
            [['Agendamento', 'Produto', 'Status', 'Valor', 'Custo', 'Gratuito'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Agendamento' => 'Agendamento',
            'Produto' => 'Produto',
            'Status' => 'Status',
            'Valor' => 'Valor',
            'Custo' => 'Custo',
            'Gratuito' => 'Gratuito',
        ];
    }


    public function getAgendamento()
    {
        // a comment has one customer
        return $this->hasOne(Agendamento::className(), ['ID' => 'Agendamento']);
    }


    public function getProduto()
    {
        // a comment has one customer
        return $this->hasOne(Produto::className(), ['ID' => 'Produto']);
    }


    public function getStatus()
    {
        // a comment has one customer
        return $this->hasOne(Status::className(), ['ID' => 'Status'])->select(['Nome']);
    }


}
