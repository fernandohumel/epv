<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Produto;

/**
* ProdutoSearch represents the model behind the search form about `app\models\Produto`.
*/
class ProdutoSearch extends Produto
{
/**
* @inheritdoc
*/
public function rules()
{
return [
[['ID', 'Estoque', 'EstoqueMinimo', 'EstoqueMaximo', 'Categoria', 'Servico', 'Filial', 'Status'], 'integer'],
            [['Nome', 'Foto'], 'safe'],
            [['Custo', 'Venda'], 'number'],
];
}

/**
* @inheritdoc
*/
public function scenarios()
{
// bypass scenarios() implementation in the parent class
return Model::scenarios();
}

/**
* Creates data provider instance with search query applied
*
* @param array $params
*
* @return ActiveDataProvider
*/
public function search($params)
{
$query = Produto::find();

// add conditions that should always apply here

$dataProvider = new ActiveDataProvider([
'query' => $query,
]);

$this->load($params);

if (!$this->validate()) {
// uncomment the following line if you do not want to return any records when validation fails
// $query->where('0=1');
return $dataProvider;
}

// grid filtering conditions
$query->andFilterWhere([
            'ID' => $this->ID,
            'Custo' => $this->Custo,
            'Venda' => $this->Venda,
            'Estoque' => $this->Estoque,
            'EstoqueMinimo' => $this->EstoqueMinimo,
            'EstoqueMaximo' => $this->EstoqueMaximo,
            'Categoria' => $this->Categoria,
            'Servico' => $this->Servico,
            'Filial' => $this->Filial,
            'Status' => $this->Status,
        ]);

        $query->andFilterWhere(['like', 'Nome', $this->Nome])
            ->andFilterWhere(['like', 'Foto', $this->Foto]);

return $dataProvider;
}
}
