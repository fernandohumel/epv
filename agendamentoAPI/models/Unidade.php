<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Unidade".
 *
 * @property integer $ID
 * @property string $Nome
 * @property integer $Status
 */
class Unidade extends Modelos
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Unidade';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Nome'], 'required'],
            [['Status'], 'integer'],
            [['Nome'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Nome' => 'Nome',
            'Status' => 'Status',
        ];
    }
}
