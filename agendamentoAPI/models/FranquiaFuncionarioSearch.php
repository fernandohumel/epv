<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FranquiaFuncionario;

/**
* FranquiaFuncionarioSearch represents the model behind the search form about `app\models\FranquiaFuncionario`.
*/
class FranquiaFuncionarioSearch extends FranquiaFuncionario
{
/**
* @inheritdoc
*/
public function rules()
{
return [
[['ID', 'Franquia', 'Funcionario', 'Status'], 'integer'],
            [['Telefone'], 'safe'],
];
}

/**
* @inheritdoc
*/
public function scenarios()
{
// bypass scenarios() implementation in the parent class
return Model::scenarios();
}

/**
* Creates data provider instance with search query applied
*
* @param array $params
*
* @return ActiveDataProvider
*/
public function search($params)
{
$query = FranquiaFuncionario::find();

// add conditions that should always apply here

$dataProvider = new ActiveDataProvider([
'query' => $query,
]);

$this->load($params);

if (!$this->validate()) {
// uncomment the following line if you do not want to return any records when validation fails
// $query->where('0=1');
return $dataProvider;
}

// grid filtering conditions
$query->andFilterWhere([
            'ID' => $this->ID,
            'Franquia' => $this->Franquia,
            'Funcionario' => $this->Funcionario,
            'Status' => $this->Status,
        ]);

        $query->andFilterWhere(['like', 'Telefone', $this->Telefone]);

return $dataProvider;
}
}
