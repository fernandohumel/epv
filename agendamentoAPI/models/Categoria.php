<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Categoria".
 *
 * @property integer $ID
 * @property string $Nome
 * @property integer $Status
 */
class Categoria extends Modelos
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Categoria';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Nome'], 'required'],
            [['Status'], 'integer'],
            [['Nome'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Nome' => 'Nome',
            'Status' => 'Status',
        ];
    }


    public function getStatus()
    {
        // a comment has one customer
        return $this->hasOne(Status::className(), ['ID' => 'Status'])->select(['Nome']);
    }
}
