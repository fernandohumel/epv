<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Endereco;

/**
* EnderecoSearch represents the model behind the search form about `app\models\Endereco`.
*/
class EnderecoSearch extends Endereco
{
/**
* @inheritdoc
*/
public function rules()
{
return [
[['ID', 'Franquia', 'Cidade', 'Status'], 'integer'],
            [['Rua', 'Numero', 'CEP', 'Referencia'], 'safe'],
];
}

/**
* @inheritdoc
*/
public function scenarios()
{
// bypass scenarios() implementation in the parent class
return Model::scenarios();
}

/**
* Creates data provider instance with search query applied
*
* @param array $params
*
* @return ActiveDataProvider
*/
public function search($params)
{
$query = Endereco::find();

// add conditions that should always apply here

$dataProvider = new ActiveDataProvider([
'query' => $query,
]);

$this->load($params);

if (!$this->validate()) {
// uncomment the following line if you do not want to return any records when validation fails
// $query->where('0=1');
return $dataProvider;
}

// grid filtering conditions
$query->andFilterWhere([
            'ID' => $this->ID,
            'Franquia' => $this->Franquia,
            'Cidade' => $this->Cidade,
            'Status' => $this->Status,
        ]);

        $query->andFilterWhere(['like', 'Rua', $this->Rua])
            ->andFilterWhere(['like', 'Numero', $this->Numero])
            ->andFilterWhere(['like', 'CEP', $this->CEP])
            ->andFilterWhere(['like', 'Referencia', $this->Referencia]);

return $dataProvider;
}
}
