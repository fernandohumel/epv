<?php
/**
 * @copyright Copyright (c) 2014-2015 2amigOS! Consulting Group LLC
 * @link http://2amigos.us
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 */

namespace app\models;


class RetornoAPI
{

    //Boolean
    public $error;
    //String
    public $errorMessage;

    //can be of type: String, Boolean, Array, Object
    public $type;

    //object or list of objects for returning statement, can be string, false,  object or list of objects
    public $result;

    //token para ser enviado na proxima transacao
    public $nextToken;

    function getJsonResult($jsonFormat = false)
    {

        if ($jsonFormat == true) {

            \Yii::$app->response->format = 'json';
            return $this;
        } else {

            return json_encode($this);

        }

    }

}

