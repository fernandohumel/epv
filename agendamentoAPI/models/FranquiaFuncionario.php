<?php

namespace app\models;

use Yii;

/**
* This is the model class for table "FranquiaFuncionario".
*
    * @property integer $ID
    * @property string $Telefone
    * @property integer $Franquia
    * @property integer $Funcionario
    * @property integer $Status
*/
class FranquiaFuncionario extends \yii\db\ActiveRecord
{
/**
* @inheritdoc
*/
public static function tableName()
{
return 'FranquiaFuncionario';
}

/**
* @inheritdoc
*/
public function rules()
{
return [
            [['Telefone', 'Franquia', 'Funcionario'], 'required'],
            [['Franquia', 'Funcionario', 'Status'], 'integer'],
            [['Telefone'], 'string', 'max' => 255],
        ];
}

/**
* @inheritdoc
*/
public function attributeLabels()
{
return [
    'ID' => 'ID',
    'Telefone' => 'Telefone',
    'Franquia' => 'Franquia',
    'Funcionario' => 'Funcionario',
    'Status' => 'Status',
];
}
}
