<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Usuario;

/**
* UsuarioSearch represents the model behind the search form about `app\models\Usuario`.
*/
class UsuarioSearch extends Usuario
{
/**
* @inheritdoc
*/
public function rules()
{
return [
[['ID', 'Filial', 'Perfil', 'Status'], 'integer'],
            [['Nome', 'Telefone', 'Email', 'Usuario', 'Senha', 'Nascimento'], 'safe'],
];
}

/**
* @inheritdoc
*/
public function scenarios()
{
// bypass scenarios() implementation in the parent class
return Model::scenarios();
}

/**
* Creates data provider instance with search query applied
*
* @param array $params
*
* @return ActiveDataProvider
*/
public function search($params)
{
$query = Usuario::find();

// add conditions that should always apply here

$dataProvider = new ActiveDataProvider([
'query' => $query,
]);

$this->load($params);

if (!$this->validate()) {
// uncomment the following line if you do not want to return any records when validation fails
// $query->where('0=1');
return $dataProvider;
}

// grid filtering conditions
$query->andFilterWhere([
            'ID' => $this->ID,
            'Filial' => $this->Filial,
            'Perfil' => $this->Perfil,
            'Status' => $this->Status,
        ]);

        $query->andFilterWhere(['like', 'Nome', $this->Nome])
            ->andFilterWhere(['like', 'Telefone', $this->Telefone])
            ->andFilterWhere(['like', 'Email', $this->Email])
            ->andFilterWhere(['like', 'Usuario', $this->Usuario])
            ->andFilterWhere(['like', 'Senha', $this->Senha])
            ->andFilterWhere(['like', 'Nascimento', $this->Nascimento]);

return $dataProvider;
}
}
