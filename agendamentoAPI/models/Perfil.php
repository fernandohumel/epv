<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Perfil".
 *
 * @property integer $ID
 * @property string $Nome
 * @property integer $Gerente
 * @property integer $Status
 */
class Perfil extends Modelos
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Perfil';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Nome'], 'required'],
            [['Gerente', 'Status'], 'integer'],
            [['Nome'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Nome' => 'Nome',
            'Gerente' => 'Gerente',
            'Status' => 'Status',
        ];
    }


    public function getStatus()
    {
        // a comment has one customer
        return $this->hasOne(Status::className(), ['ID' => 'Status'])->select(['Nome']);
    }


}
