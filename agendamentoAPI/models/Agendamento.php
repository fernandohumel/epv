<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Agendamento".
 *
 * @property integer $ID
 * @property string $Data
 * @property string $Hora
 * @property string $Total
 * @property integer $Cliente
 * @property integer $Profissional
 * @property integer $Gerente
 * @property integer $GerenteCancelamento
 * @property integer $Filial
 * @property integer $Status
  * @property integer $FormaPagamento
 */
class Agendamento extends Modelos
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Agendamento';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Data', 'Hora', 'Total', 'Cliente', 'Profissional', 'Gerente', 'GerenteCancelamento', 'Filial'], 'required'],
            [['Total'], 'number'],
            ['Data', 'date', 'format'=>'yyyy-MM-dd'],
            [['Cliente', 'Profissional', 'Gerente', 'GerenteCancelamento', 'Filial', 'Status'], 'integer'],
            [['Data', 'Hora'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Data' => 'Data',
            'Hora' => 'Hora',
            'Total' => 'Total',
            'Cliente' => 'Cliente',
            'Profissional' => 'Profissional',
            'Gerente' => 'Gerente',
            'GerenteCancelamento' => 'Gerente Cancelamento',
            'Filial' => 'Filial',
            'Status' => 'Status',
             'Justificativa' => 'Justificativa',
            'FormaPagamento' => 'FormaPagamento',
            'Gratuito' => 'Gratuito',
        ];
    }


    public function DataCalendario()
    {
        return $this->Data . 'T' . $this->Hora . ":00";
    }

    public function getCliente()
    {
        // a comment has one customer
        return $this->hasOne(Cliente::className(), ['ID' => 'Cliente']);
        //        return $this->hasOne(Cliente::className(), ['ID' => 'Cliente'])->select(['Nome']);
    }


    public function getProfissional()
    {
        // a comment has one customer
        return $this->hasOne(Profissional::className(), ['ID' => 'Profissional'])->select(['Nome']);
    }

    public function getGerente()
    {
        // a comment has one customer
        return $this->hasOne(Usuario::className(), ['ID' => 'Gerente'])->select(['Nome']);
    }

    public function getGerentecancelamento()
    {
        // a comment has one customer
        return $this->hasOne(Usuario::className(), ['ID' => 'GerenteCancelamento'])->select(['Nome']);
    }


    public function getFilial()
    {
        // a comment has one customer
        return $this->hasOne(Filial::className(), ['ID' => 'Filial'])->select(['Nome']);
    }


    public function getStatus()
    {
        // a comment has one customer
        return $this->hasOne(Status::className(), ['ID' => 'Status'])->select(['Nome']);
    }

    public function getProdutos()
    {
        return $this->hasMany(AgendamentoProduto::className(), ['Agendamento' => 'ID']);
    }


    //devolve o valor total dos produtos deste agendamento
    public function getTotal()
    {

        $r = 0;
        foreach ($this->produtos as $p) {
            $r += $p->produto->Venda;
        }

        return $r;

    }





    //devolve o valor total dos produtos deste agendamento
    public function getTotalTempo()
    {

        $r = 0;
        foreach ($this->produtos as $p) {
            $r += $p->produto->Tempo;
        }

        return $r;

    }



}
