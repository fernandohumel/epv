<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Balcao".
 *
 * @property integer $ID
 * @property string $Data
 * @property string $Total
 * @property integer $Gerente
 * @property integer $Filial
 * @property integer $Status
  * @property integer $FormaPagamento
 */
class Balcao extends Modelos
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Balcao';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Data', 'Filial'], 'required'],
            [['Total'], 'number'],
            ['Data', 'date', 'format'=>'yyyy-MM-dd'],
            [[ 'Gerente', 'Filial', 'Status'], 'integer'],
            [['Data'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Data' => 'Data',
            'Total' => 'Total',
            'Gerente' => 'Gerente',
            'Filial' => 'Filial',
            'Status' => 'Status',
            'FormaPagamento' => 'FormaPagamento',
        ];
    }


    public function getGerente()
    {
        // a comment has one customer
        return $this->hasOne(Usuario::className(), ['ID' => 'Gerente'])->select(['Nome']);
    }



    public function getFilial()
    {
        // a comment has one customer
        return $this->hasOne(Filial::className(), ['ID' => 'Filial'])->select(['Nome']);
    }


    public function getStatus()
    {
        // a comment has one customer
        return $this->hasOne(Status::className(), ['ID' => 'Status'])->select(['Nome']);
    }

    public function getProdutos()
    {
        return $this->hasMany(BalcaoProduto::className(), ['Balcao' => 'ID']);
    }


    //devolve o valor total dos produtos deste Balcao
    public function getTotal()
    {

        $r = 0;
        foreach ($this->produtos as $p) {
            $r += $p->produto->Venda;
        }

        return $r;

    }



}
