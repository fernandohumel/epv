<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ProfissionalHorario;

/**
* ProfissionalHorarioSearch represents the model behind the search form about `app\models\ProfissionalHorario`.
*/
class ProfissionalHorarioSearch extends ProfissionalHorario
{
/**
* @inheritdoc
*/
public function rules()
{
return [
[['ID', 'Profissional', 'Status'], 'integer'],
            [['Data', 'Hora'], 'safe'],
];
}

/**
* @inheritdoc
*/
public function scenarios()
{
// bypass scenarios() implementation in the parent class
return Model::scenarios();
}

/**
* Creates data provider instance with search query applied
*
* @param array $params
*
* @return ActiveDataProvider
*/
public function search($params)
{
$query = ProfissionalHorario::find();

// add conditions that should always apply here

$dataProvider = new ActiveDataProvider([
'query' => $query,
]);

$this->load($params);

if (!$this->validate()) {
// uncomment the following line if you do not want to return any records when validation fails
// $query->where('0=1');
return $dataProvider;
}

// grid filtering conditions
$query->andFilterWhere([
            'ID' => $this->ID,
            'Profissional' => $this->Profissional,
            'Status' => $this->Status,
        ]);

        $query->andFilterWhere(['like', 'Data', $this->Data])
            ->andFilterWhere(['like', 'Hora', $this->Hora]);

return $dataProvider;
}
}
