<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Filial".
 *
 * @property integer $ID
 * @property string $Nome
 * @property string $Email
 * @property string $Telefone
 * @property integer $Gerente
 * @property integer $Unidade
 * @property integer $Status
 */
class Filial extends Modelos
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Filial';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Nome', 'Email', 'Telefone', 'Gerente'], 'required'],
            [['Gerente', 'Unidade', 'Status'], 'integer'],
            [['Nome', 'Email', 'Telefone'], 'string', 'max' => 255],
            // the email attribute should be a valid email address
            ['Email', 'email'],
            ['Status', 'default', 'value' => 1],
            [['Telefone'], 'integer'],
            [['Telefone'], 'string', 'min' => 8, 'max' => 11],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Nome' => 'Nome',
            'Email' => 'Email',
            'Telefone' => 'Telefone',
            'Gerente' => 'Gerente',
            'Status' => 'Status',
        ];
    }




    public function getGerente()
    {
        // a comment has one customer
        return $this->hasOne(Usuario::className(), ['ID' => 'Gerente'])->select(['Nome']);
    }



    public function getStatus()
    {
        // a comment has one customer
        return $this->hasOne(Status::className(), ['ID' => 'Status'])->select(['Nome']);
    }


}
