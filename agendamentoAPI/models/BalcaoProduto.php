<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "BalcaoProduto".
 *
 * @property integer $ID
 * @property integer $Balcao
 * @property integer $Produto
 * @property integer $Status
  * @property integer $Valor
   * @property integer $Custo
 */
class BalcaoProduto extends Modelos
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'BalcaoProduto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Balcao', 'Produto', 'Valor','Custo'], 'required'],
            [['Balcao', 'Produto', 'Status', 'Valor', 'Custo'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Balcao' => 'Balcao',
            'Produto' => 'Produto',
            'Status' => 'Status',
            'Valor' => 'Valor',
            'Custo' => 'Custo',
        ];
    }


    public function getBalcao()
    {
        // a comment has one customer
        return $this->hasOne(Balcao::className(), ['ID' => 'Balcao']);
    }


    public function getProduto()
    {
        // a comment has one customer
        return $this->hasOne(Produto::className(), ['ID' => 'Produto']);
    }


    public function getStatus()
    {
        // a comment has one customer
        return $this->hasOne(Status::className(), ['ID' => 'Status'])->select(['Nome']);
    }


}
