<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Status".
 *
 * @property integer $ID
 * @property string $Nome
 * @property string $Valor
 */
class Status extends Modelos
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Nome', 'Valor'], 'required'],
            [['Nome', 'Valor'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Nome' => 'Nome',
            'Valor' => 'Valor',
        ];
    }
}
