<?php

namespace app\models;

use Yii;

/**
* This is the model class for table "Endereco".
*
    * @property integer $ID
    * @property string $Rua
    * @property string $Numero
    * @property string $CEP
    * @property string $Referencia
    * @property integer $Franquia
    * @property integer $Cidade
    * @property integer $Status
*/
class Endereco extends \yii\db\ActiveRecord
{
/**
* @inheritdoc
*/
public static function tableName()
{
return 'Endereco';
}

/**
* @inheritdoc
*/
public function rules()
{
return [
            [['Rua', 'Numero', 'CEP', 'Franquia', 'Cidade'], 'required'],
            [['Franquia', 'Cidade', 'Status'], 'integer'],
            [['Rua', 'Referencia'], 'string', 'max' => 255],
            [['Numero'], 'string', 'max' => 10],
            [['CEP'], 'string', 'max' => 12],
        ];
}

/**
* @inheritdoc
*/
public function attributeLabels()
{
return [
    'ID' => 'ID',
    'Rua' => 'Rua',
    'Numero' => 'Numero',
    'CEP' => 'Cep',
    'Referencia' => 'Referencia',
    'Franquia' => 'Franquia',
    'Cidade' => 'Cidade',
    'Status' => 'Status',
];
}
}
