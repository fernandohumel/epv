<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Json;


class MyModels extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {

        $funcao = 'update';

        if ($insert == self::EVENT_BEFORE_INSERT)
            $funcao = 'insert';

        //$insert ? self::EVENT_BEFORE_INSERT : self::EVENT_BEFORE_UPDATE,
        if (parent::beforeSave($insert)) {

            // Place your custom code here
            $h = new Historico();
            $h->IP = $this->getRealIpAddr();
            $j = json_encode($this);
            $h->UsuarioID = 1;
            $h->Application = $this->className();
            $h->Device = $funcao;
            $h->Extra = "model: " . $j;
            $h->save(false);

            return true;
        } else {
            return false;
        }
    }

    public function afterDelete()
    {

        // Place your custom code here
        $h = new Historico();
        $h->IP = $this->getRealIpAddr();
        $j = json_encode($this);
        $h->UsuarioID = 1; //TODO: obter id do usuario logado
        $h->Application = $this->className();
        $h->Device = 'delete';
        $h->Extra = "model: " . $j;
        $h->save(false);

        return parent::afterDelete();

    }

    function generateRandomString($length = 10)
    {
        $r = substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length / strlen($x)))), 1, $length);

        $r = mb_strtoupper($r);
        return $r;

        //ja tem no bd, procura outro
//        if ($this->temUniqueNOBD($r)) {
//            $this->generateRandomString($length);
//        } else {
//            return $r;
//        }

    }

//    //procura o codigo na tabela de transacoe para nao duplicar
//    function temUniqueNOBD($uuid)
//    {
//        $t = UsuarioTransacao::find()
//            ->Where(['Codigo' => $uuid])
//            ->one();
//
//        //nao achou no banco de dados
//        if ($t == null)
//            return false;
//        //achou no banco de dados
//        else {
//            return true;
//        }
//
//    }

    function getRealIpAddr()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) //to check ip is pass from proxy
        {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    function getJsonResult($jsonFormat = false)
    {

        if ($jsonFormat == true) {
            \Yii::$app->response->format = 'json';
            return $this;
        } else {
            return json_encode($this);
        }

    }
}
