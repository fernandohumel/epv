<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Profissional".
 *
 * @property integer $ID
 * @property string $Nome
 * @property string $Telefone
 * @property string $Email
 * @property string $Nascimento
 * @property string $Usuario
 * @property string $Senha
 * @property integer $Profissao
 * @property integer $Filial
 * @property integer $Status
 */
class Profissional extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Profissional';
    }

    /**
     * @inheritdoc
     */
    public function rules()
        
        
    {
        return [
            [['Nome', 'Telefone', 'Email', 'Nascimento', 'Usuario', 'Senha', 'Profissao','almoco','folga1','folga2','HoraEntrada','HoraSaida', 'Filial'], 'required'],
            [['Profissao', 'Filial', 'Status'], 'integer'],
            [['Nome', 'Telefone', 'Email', 'Nascimento', 'Usuario', 'Senha'], 'string', 'max' => 255],
            ['Nascimento', 'date',  'format'=>'yyyy-MM-dd'],

            ['Email', 'email'],
            [['Telefone'], 'integer'],
            [['Telefone'], 'string', 'min' => 8, 'max' => 11],
        ];
    


    }

    

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Nome' => 'Nome',
            'Telefone' => 'Telefone',
            'Email' => 'Email',
            'Nascimento' => 'Nascimento',
            'Usuario' => 'Usuario',
            'Senha' => 'Senha',
            'Profissao' => 'Profissao',
            'Filial' => 'Filial',
            'Status' => 'Status',
            'Almoco' => 'almoco',
            'Folga1' => 'folga1',
            'Folga2' => 'folga2',
        ];
    }

    public function getFilial()
    {
        // a comment has one customer
        return $this->hasOne(Filial::className(), ['ID' => 'Filial']);
    }


    public function getStatus()
    {
        // a comment has one customer
        return $this->hasOne(Status::className(), ['ID' => 'Status']);
    }


    public function getAgendamentos()
    {
        return $this->hasMany(Agendamento::className(), ['Profissional' => 'ID']);
    }


}
