<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Historico;

/**
* HistoricoSearch represents the model behind the search form about `app\models\Historico`.
*/
class HistoricoSearch extends Historico
{
/**
* @inheritdoc
*/
public function rules()
{
return [
[['ID', 'UsuarioID'], 'integer'],
            [['Application', 'Device', 'TimeStamp', 'IP', 'Extra'], 'safe'],
];
}

/**
* @inheritdoc
*/
public function scenarios()
{
// bypass scenarios() implementation in the parent class
return Model::scenarios();
}

/**
* Creates data provider instance with search query applied
*
* @param array $params
*
* @return ActiveDataProvider
*/
public function search($params)
{
$query = Historico::find();

// add conditions that should always apply here

$dataProvider = new ActiveDataProvider([
'query' => $query,
]);

$this->load($params);

if (!$this->validate()) {
// uncomment the following line if you do not want to return any records when validation fails
// $query->where('0=1');
return $dataProvider;
}

// grid filtering conditions
$query->andFilterWhere([
            'ID' => $this->ID,
            'UsuarioID' => $this->UsuarioID,
            'TimeStamp' => $this->TimeStamp,
        ]);

        $query->andFilterWhere(['like', 'Application', $this->Application])
            ->andFilterWhere(['like', 'Device', $this->Device])
            ->andFilterWhere(['like', 'IP', $this->IP])
            ->andFilterWhere(['like', 'Extra', $this->Extra]);

return $dataProvider;
}
}
