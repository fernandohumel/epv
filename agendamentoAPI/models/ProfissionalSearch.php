<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Profissional;

/**
* ProfissionalSearch represents the model behind the search form about `app\models\Profissional`.
*/
class ProfissionalSearch extends Profissional
{
/**
* @inheritdoc
*/
public function rules()
{
return [
[['ID', 'Profissao', 'Filial', 'Status'], 'integer'],
            [['Nome', 'Telefone', 'Email', 'Nascimento', 'Usuario', 'Senha'], 'safe'],
];
}

/**
* @inheritdoc
*/
public function scenarios()
{
// bypass scenarios() implementation in the parent class
return Model::scenarios();
}

/**
* Creates data provider instance with search query applied
*
* @param array $params
*
* @return ActiveDataProvider
*/
public function search($params)
{
$query = Profissional::find();

// add conditions that should always apply here

$dataProvider = new ActiveDataProvider([
'query' => $query,
]);

$this->load($params);

if (!$this->validate()) {
// uncomment the following line if you do not want to return any records when validation fails
// $query->where('0=1');
return $dataProvider;
}

// grid filtering conditions
$query->andFilterWhere([
            'ID' => $this->ID,
            'Profissao' => $this->Profissao,
            'Filial' => $this->Filial,
            'Status' => $this->Status,
        ]);

        $query->andFilterWhere(['like', 'Nome', $this->Nome])
            ->andFilterWhere(['like', 'Telefone', $this->Telefone])
            ->andFilterWhere(['like', 'Email', $this->Email])
            ->andFilterWhere(['like', 'Nascimento', $this->Nascimento])
            ->andFilterWhere(['like', 'Usuario', $this->Usuario])
            ->andFilterWhere(['like', 'Senha', $this->Senha]);

return $dataProvider;
}
}
