<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Balcao;

/**
* BalcaoSearch represents the model behind the search form about `app\models\Balcao`.
*/
class BalcaoSearch extends Balcao
{
/**
* @inheritdoc
*/
public function rules()
{
return [
[['ID', 'Gerente', 'Filial', 'Status'], 'integer'],
            [['Data'], 'safe'],
            [['Total'], 'number'],
];
}

/**
* @inheritdoc
*/
public function scenarios()
{
// bypass scenarios() implementation in the parent class
return Model::scenarios();
}

/**
* Creates data provider instance with search query applied
*
* @param array $params
*
* @return ActiveDataProvider
*/
public function search($params)
{
$query = Balcao::find();

// add conditions that should always apply here

$dataProvider = new ActiveDataProvider([
'query' => $query,
]);

$this->load($params);

if (!$this->validate()) {
// uncomment the following line if you do not want to return any records when validation fails
// $query->where('0=1');
return $dataProvider;
}

// grid filtering conditions
$query->andFilterWhere([
            'ID' => $this->ID,
            'Total' => $this->Total,
            'Gerente' => $this->Gerente,
            'Filial' => $this->Filial,
            'Status' => $this->Status,
        ]);

        $query->andFilterWhere(['like', 'Data', $this->Data]);

return $dataProvider;
}
}
