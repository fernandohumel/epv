<?php

namespace app\models;

use Yii;

/**
* This is the model class for table "Cidade".
*
    * @property integer $ID
    * @property string $Nome
    * @property integer $Estado
    * @property integer $Status
*/
class Cidade extends \yii\db\ActiveRecord
{
/**
* @inheritdoc
*/
public static function tableName()
{
return 'Cidade';
}

/**
* @inheritdoc
*/
public function rules()
{
return [
            [['Nome', 'Estado'], 'required'],
            [['Estado', 'Status'], 'integer'],
            [['Nome'], 'string', 'max' => 255],
        ];
}

/**
* @inheritdoc
*/
public function attributeLabels()
{
return [
    'ID' => 'ID',
    'Nome' => 'Nome',
    'Estado' => 'Estado',
    'Status' => 'Status',
];
}
}
