<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Usuario".
 *
 * @property integer $ID
 * @property string $Nome
 * @property string $Telefone
 * @property string $Email
 * @property string $Usuario
 * @property string $Senha
 * @property string $Nascimento
 * @property integer $Filial
 * @property integer $Perfil
 * @property integer $Status
 */
class Usuario extends Modelos
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Usuario';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Nome', 'Telefone', 'Email', 'Usuario', 'Senha', 'Nascimento', 'Filial', 'Perfil', 'Status'], 'required'],
            [['Filial', 'Perfil', 'Status'], 'integer'],
            [['Nome',  'Email', 'Usuario', 'Senha', 'Nascimento'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Nome' => 'Nome',
            'Telefone' => 'Telefone',
            'Email' => 'Email',
            'Usuario' => 'Usuario',
            'Senha' => 'Senha',
            'Nascimento' => 'Nascimento',
            'Filial' => 'Filial',
            'Perfil' => 'Perfil',
            'Status' => 'Status',
        ];
    }


    public function getPerfil()
    {
        // a comment has one customer
        return $this->hasOne(Perfil::className(), ['ID' => 'Perfil']);
    }

    public function getFilial()
    {
        // a comment has one customer
        return $this->hasOne(Filial::className(), ['ID' => 'Filial']);
    }


    public function getStatus()
    {
        // a comment has one customer
        return $this->hasOne(Status::className(), ['ID' => 'Status'])->select(['Nome']);
    }


}
