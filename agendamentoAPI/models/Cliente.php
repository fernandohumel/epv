<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Cliente".
 *
 * @property integer $ID
 * @property string $Nome
 * @property string $Telefone
 * @property string $Email
 * @property string $Foto
 * @property string $Social
 * @property string $Usuario
 * @property string $Senha
 * @property integer $Filial
 * @property integer $Status
 * @property integer $CPF
 * @property integer $Aniversario
 */
class Cliente extends Modelos
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Cliente';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Nome', 'CPF','Aniversario','Status',   'Telefone', 'Email',  'Usuario', 'Senha', 'Filial'], 'required'],
            [['Filial', 'Status', 'Bonus'], 'integer'],
            [['Nome', 'Telefone', 'Email',  'Usuario', 'Senha'], 'string', 'max' => 255],
            ['Email', 'email'],



        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Nome' => 'Nome',
            'Telefone' => 'Telefone',
            'Email' => 'Email',
            'Usuario' => 'Usuario',
            'Senha' => 'Senha',
            'Filial' => 'Filial',
            'Status' => 'Status',
            'CPF' => 'CPF',
            'Aniversario' => 'Aniversario',
            'Bonus' => 'Bonus',

        ];
    }


    public function getFilial()
    {
        // a comment has one customer
        return $this->hasOne(Filial::className(), ['ID' => 'Filial'])->select(['Nome']);
    }


    public function getStatus()
    {
        // a comment has one customer
        return $this->hasOne(Status::className(), ['ID' => 'Status'])->select(['Nome']);
    }

    public function getAgendamentos()
    {
        return $this->hasMany(Agendamento::className(), ['Cliente' => 'ID']);
    }


}
