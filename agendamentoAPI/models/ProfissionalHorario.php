<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ProfissionalHorario".
 *
 * @property integer $ID
 * @property string $Data
 * @property string $Hora
 * @property integer $Profissional
 * @property integer $Status
 */
class ProfissionalHorario extends Modelos
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ProfissionalHorario';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Data', 'Hora', 'Profissional'], 'required'],
            [['Profissional', 'Status'], 'integer'],
            [['Data', 'Hora'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Data' => 'Data',
            'Hora' => 'Hora',
            'Profissional' => 'Profissional',
            'Status' => 'Status',
        ];
    }
}
