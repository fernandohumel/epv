<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\BalcaoProduto;

/**
* BalcaoProdutoSearch represents the model behind the search form about `app\models\BalcaoProduto`.
*/
class BalcaoProdutoSearch extends BalcaoProduto
{
/**
* @inheritdoc
*/
public function rules()
{
return [
[['ID', 'Balcao', 'Produto', 'Status'], 'integer'],
];
}

/**
* @inheritdoc
*/
public function scenarios()
{
// bypass scenarios() implementation in the parent class
return Model::scenarios();
}

/**
* Creates data provider instance with search query applied
*
* @param array $params
*
* @return ActiveDataProvider
*/
public function search($params)
{
$query = BalcaoProduto::find();

// add conditions that should always apply here

$dataProvider = new ActiveDataProvider([
'query' => $query,
]);

$this->load($params);

if (!$this->validate()) {
// uncomment the following line if you do not want to return any records when validation fails
// $query->where('0=1');
return $dataProvider;
}

// grid filtering conditions
$query->andFilterWhere([
            'ID' => $this->ID,
            'Balcao' => $this->Balcao,
            'Produto' => $this->Produto,
            'Status' => $this->Status,
        ]);

return $dataProvider;
}
}
