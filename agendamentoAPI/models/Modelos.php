<?php
/**
 * Created by PhpStorm.
 * User: wiki
 * Date: 27/10/17
 * Time: 09:47
 */

namespace app\models;


class Modelos extends \yii\db\ActiveRecord
{

    function getJsonResult($jsonFormat = false)
    {
        if ($jsonFormat == true) {
            \Yii::$app->response->format = 'json';
            return $this;
        } else {
            return json_encode($this);
        }
    }

}