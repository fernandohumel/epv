<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Produto".
 *
 * @property integer $ID
 * @property string $Nome
 * @property string $Custo
 * @property string $Venda
 * @property integer $Estoque
 * @property integer $EstoqueMinimo
 * @property integer $EstoqueMaximo
 * @property integer $Categoria
 * @property integer $Servico
 * @property integer $Tempo
 * @property integer $Filial
 * @property string $Foto
 * @property integer $Status
  * @property integer $Cortesia
 */
class Produto extends Modelos
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Produto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Nome', 'Custo','Venda','Status',  'Filial',  'Servico', 'Cortesia'], 'required'],
            [['Custo', 'Venda', 'Estoque', 'EstoqueMinimo'], 'number'],
            [[ 'Categoria', 'Servico', 'Tempo',  'Estoque',  'EstoqueMaximo',  'EstoqueMinimo', 'Status'], 'integer'],
            [['Nome', 'Cortesia'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Nome' => 'Nome',
            'Custo' => 'Custo',
            'Venda' => 'Venda',
            'Estoque' => 'Estoque',
            'EstoqueMaximo' => 'EstoqueMaximo',
            'EstoqueMinimo' => 'EstoqueMinimo',
            
            'Filial' => 'Filial',
            'Servico' => 'Categoria',
          
            'Tempo' => 'Tempo (minutos)',
            'Status' => 'Status',
            'Cortesia' => 'Cortesia',
        ];
    }

    public function getCategoria()
    {
        // a comment has one customer
        return $this->hasOne(Categoria::className(), ['ID' => 'Categoria'])->select(['Nome']);
    }

    public function getFilial()
    {
        // a comment has one customer
        return $this->hasOne(Filial::className(), ['ID' => 'Filial'])->select(['Nome']);
    }


    public function getStatus()
    {
        // a comment has one customer
        return $this->hasOne(Status::className(), ['ID' => 'Status'])->select(['Nome']);
    }

}
