<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Historico".
 *
 * @property integer $ID
 * @property integer $UsuarioID
 * @property string $Application
 * @property string $Device
 * @property string $TimeStamp
 * @property string $IP
 * @property string $Extra
 */
class Historico extends Modelos
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Historico';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['UsuarioID'], 'integer'],
            [['TimeStamp'], 'safe'],
            [['Extra'], 'string'],
            [['Application', 'Device', 'IP'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'UsuarioID' => 'Usuario ID',
            'Application' => 'Application',
            'Device' => 'Device',
            'TimeStamp' => 'Time Stamp',
            'IP' => 'Ip',
            'Extra' => 'Extra',
        ];
    }
}
