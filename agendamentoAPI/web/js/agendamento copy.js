$(document).ready(function () {
    console.log('Ready disparado');

    var clienteSelecionado = 0;
    var profissionalSelecionado = 0;
    var dadosAgendamento = [];
    var dataSelecionada = "";
    var usuario = JSON.parse(getCookie("user"));
    var filial = usuario.result[0].Filial;

    var ultimoAgendamento = 0;

    var dialog, form,
        // From http://www.whatwg.org/specs/web-apps/current-work/multipage/states-of-the-type-attribute.html#e-mail-state-%28type=email%29
        emailRegex = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
        name = $("#name"),
        email = $("#email"),
        password = $("#password"),
        allFields = $([]).add(name).add(email).add(password),
        tips = $(".validateTips");


    //se nao tiver o cookie setado eh pq user nao ta logado e volta pra index
    if (usuario !== null && parseInt(usuario.result[0].ID) > 0) {
        $('#nome').text(usuario.result[0].Nome);
    }
    else {
        window.location.replace("index.html");
    }


    $('#logout').click(function () {
        logout();
        window.location.replace("index.html");
    });

    function escapeHtml(text) {
        var map = {
            '&': '&amp;',
            '<': '&lt;',
            '>': '&gt;',
            '"': '&quot;',
            "'": '&#039;'
        };

        return text.replace(/[&<>"']/g, function (m) {
            return map[m];
        });
    }

    function dados() {
        return dadosAgendamento;
    }

    function getAgendamentosCalendario() {


        // var teste = [
        //     {
        //         id: 1,
        //         title: 'Agendamento Gustavo',
        //         start: '2017-10-29T10:30:00',
        //         end: '2017-10-29T12:30:00'
        //     }
        //
        // ];


        //OBTER DADOS DA API
        // var url = "www.moradialzer.com.br/divinoAPI/web/index.php?r=divino/agendamentos-calendario&filial=1";
        var url = "www.moradialzer.com.br/FTP/agendamentoAPI/web/index.php?r=divino/agendamentos-calendario&filial=" + filial;
        var jqxhr = $.get(url
            , function (data) {

                dadosAgendamento = data;

                iniciaCalendario();

                // if (data.error === false) {
                //     //percorre array de usuarios
                //     for (var i = 0, len = data.result.length; i < len; i++) {
                //         $('#combobox-cliente').append('<option value="' + data.result[i].ID + '" selected="selected">' + data.result[i].Nome + '</option>');
                //     }
                // }
                // else {
                //     alert("Humm..... " + JSON.stringify(data));
                // }

            })
        .done(function () {
//                    alert("second success");
})
        .fail(function () {
            alert("error");
        })
        .always(function () {
//                    alert("finished");
});

// Perform other work here ...

// Set another completion function for the request above
jqxhr.always(function () {
//                alert("second finished");
});


        //FAZER PARSER E RETORNAR ESTRUTURA DE JSON PARA CALENDARIO


    }

    function RetornaAgendamento() {

                 //www.moradialzer.com.br/FTP/agendamentoAPI/web/index.php?r=divino/retorna-agendamento&ultimoAgendamento=1
                 var url = "www.moradialzer.com.br/FTP/agendamentoAPI/web/index.php?r=divino/retorna-agendamento"
                 + "&ultimoAgendamento=" + ultimoAgendamento;


        //TODO: obter produtos

        var jqxhr = $.get(url
            , function (data) {

        //inserir valor no input
        
        if (data.error === false) {
                     //percorre array de usuarios
                     for (var i = 0, len = data.result.length; i < len; i++) {
                        $('#combobox-cliente-atualizar').val(data.result[i].cliente.Nome);
                        //inserir valor no combobox
                        // $('#combobox-cliente-atualizar').append('<option value="' + data.result[i].ID + '" selected="selected">' + data.result[i].cliente.Nome + '</option>');


                        //colocar profisisonal como selected!
                        //alert( data.result[i].Profissional);
                        $('#profissional-atualizar option[value="'+data.result[i].Profissional+'"]').attr('selected','selected'); 


                        //preeenche data
                        $('#datepicker-atualiza').val(data.result[i].Data);

                        //preeenche hora
                        $('#timepicker-atualiza').val(data.result[i].Hora);

                        //adiciona produto
                        $("#table-produtos-atualizar tbody").append("<tr>" +
                            "<td>" + data.result[i].produtos.ID + "</td>" +
                            "<td>" + data.result[i].produtos.Nome + "</td>" +
                            "<td>" + data.result[i].produtos.valor + "</td>" +
                            "<td>" + data.result[i].produtos.Tempo + "</td>" +
                            "<td>" + "<div class='div-remove'>Remover</div>" + "</td>" +
                            "</tr>");






                    }
                }
                else {
                    alert("Humm..... " + JSON.stringify(data));
                } 
        // $('#combobox-cliente').append('<option>fernando</option>');
        //coloca cliente no combobox
        //$('#combobox-cliente').append('<option>' + data.result.ID + '</option>');
        //$('#combobox-cliente').append('<option>' + data.result.cliente.Nome + '</option>');



    })
        .done(function () {
//                    alert("second success");
})
        .fail(function () {
            alert("error");
        })
        .always(function () {
//                    alert("finished");
});

// Perform other work here ...

// Set another completion function for the request above
jqxhr.always(function () {
//                alert("second finished");
});


}


function getClientes() {

    var url = "www.moradialzer.com.br/FTP/agendamentoAPI/web/index.php?r=divino/clientes&filial=" + filial;
    var jqxhr = $.get(url
        , function (data) {
            if (data.error === false) {

                    //limpa o combo de clientes
                    $('#combobox-cliente').empty();

                    //Add a empty option
                    $('#combobox-cliente').append(' <option value="">Selecione um cliente...</option>');

                    //percorre array de usuarios
                    for (var i = 0, len = data.result.length; i < len; i++) {
                        $('#combobox-cliente').append('<option value="' + data.result[i].ID + '" selected="selected">' + data.result[i].Nome + '</option>');
                    }

                    $('#combobox-cliente option[value=0]').attr('selected', 'selected');

                }
                else {
                    alert("Humm..... " + JSON.stringify(data));
                }

            })
    .done(function () {
//                    alert("second success");
})
    .fail(function () {
        alert("error getting clientes...");
    })
    .always(function () {
//                    alert("finished");
});

// Perform other work here ...

// Set another completion function for the request above
jqxhr.always(function () {
//                alert("second finished");
});

}

function getProdutos() {
    var url = "www.moradialzer.com.br/FTP/agendamentoAPI/web/index.php?r=divino/produtos&filial=" + filial;
    var jqxhr = $.get(url
        , function (data) {
                // alert("success " + JSON.stringify(data));

                if (data.error === false) {


                    //percorre array de usuarios

                    for (var i = 0, len = data.result.length; i < len; i++) {
                        // someFn(data.result[i]);


                        //TODO: adicionar os objetos em uma lista por indice para consultas futuras e gravacoes


                        $('#produtos').append('<option value="' + data.result[i].ID + '" selected="selected">' + data.result[i].Nome + '</option>');


                    }
                    //TODO: popula campo de clientes!


                }
                else {
                    alert("Humm..... " + JSON.stringify(data));
                }

            })
    .done(function () {
//                    alert("second success");
})
    .fail(function () {
        alert("error");
    })
    .always(function () {
//                    alert("finished");
});

// Perform other work here ...

// Set another completion function for the request above
jqxhr.always(function () {
//                alert("second finished");
});

}

function getProfissionais() {
    var url = "www.moradialzer.com.br/FTP/agendamentoAPI/web/index.php?r=divino/profissionais&filial=" + filial;
    var jqxhr = $.get(url
        , function (data) {
                // alert("success " + JSON.stringify(data));
                if (data.error === false) {
                    //percorre array de usuarios
                    for (var i = 0, len = data.result.length; i < len; i++) {
                        $('#profissional').append('<option value="' + data.result[i].ID + '" selected="selected">' + data.result[i].Nome + '</option>');
                        $('#profissional-atualizar').append('<option value="' + data.result[i].ID + '" >' + data.result[i].Nome + '</option>');
                    }
                }
                else {
                    alert("Humm..... " + JSON.stringify(data));
                }

            })
    .done(function () {
//                    alert("second success");
})
    .fail(function () {
        alert("error ao recuperar profisisonais");
    })
    .always(function () {
//                    alert("finished");
});

// Perform other work here ...

// Set another completion function for the request above
jqxhr.always(function () {
//                alert("second finished");
});

}

function iniciaCalendario() {


    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        defaultDate: getDefaultData(),
            navLinks: true, // can click day/week names to navigate views
            selectable: true,
            selectHelper: true,
            select: function (start, end) {
                calendarSelect(start, end);
            },
            unselect: function (jsEvent, view) {

            },
            editable: true,
            eventLimit: true, // allow "more" link when too many events
            dayClick: function (date, jsEvent, view) {

               


        //reinicar para novo agendamento

        //limpa os campos!
        $('#combobox-cliente').empty();
       // $("#table-produtos-atualizar").empty();
 



        //carrega dados
        getClientes();
        getProdutos();
        getProfissionais();



        //reseta form para Novo Agendamento

        dialog = $("#dialog-agendamento").dialog({
            zIndex: 100,
            autoOpen: false,
            dialogClass: 'mydialog',
            // appendTo: $("body"),
            height: screen.height - (screen.height * 0.4),
            width: screen.width - (screen.width * 0.3),
            modal: true,
            buttons: {
                "Agendar": function () {
                    if (validaAddAgendamento()) {
                        addAgendamento();
                    }
                    else {
                        alert('Informe todos os campos para incluir um agendamento.');
                    }
                },
                Cancel: function () {
                    dialog.dialog("close");
                }
            },
            close: function () {
                form[0].reset();
                allFields.removeClass("ui-state-error");
            }
        });



                //Quando clica em um DIA


                // alert('Clicked on: ' + date.format());

                // alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);

                // alert('Current view: ' + view.name);

                // change the day's background color just for fun
                // $(this).css('background-color', 'red');

            },
            eventClick: function (calEvent, jsEvent, view) {

                $("#dialog-alert-agendamento").dialog("open");

                ultimoAgendamento = calEvent.id;

                // alert('Event: ' + calEvent.id);
                // alert('Abrir agendamento ID: ' + calEvent.id + " - Nome: " + calEvent.title);
                // alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
                // alert('View: ' + view.name);

                // change the border color just for fun
                $(this).css('border-color', 'red');

            },
            events: dados()
        });

}

function addAgendamentoCalendario() {

//            var title = 'Agendamento: ' + $('#combobox-cliente option:selected').text();
        //var title = $('#combobox-cliente option:selected').text();
        var title = $('#combobox-cliente option:selected').text() + ' - ' + $('#profissional option:selected').text();

        var start = $('#datepicker').val();
        var horaStart = $('#timepicker').val().slice(0, 5);
        var horaEnd = $('#timepicker').val().slice(0, 5);

        start = mudaData(start, horaStart);

        //TODO: Calcular hora final com base na somatoria dos produtos  e servicos do agendamento
        var end = $('#datepicker').val();

        var hours = horaEnd.slice(0, 2);
        var minutes = horaEnd.slice(3, 5);
        var time = moment(hours + ':' + minutes, 'HH:mm');
        //moment({hours: hours, minutes: minutes});
        time.add(60, 'm');
        console.log(time.format("HH:mm"));

        end = mudaData(end, time.format("HH:mm"));
        //TODO: add Produtos e Servicos Time ADD for END HOUR

        var eventData;
        if (title) {
            eventData = {
                title: title,
                start: start,
                end: end
            };
            $('#calendar').fullCalendar('renderEvent', eventData, false); // stick? = true
        }
        $('#calendar').fullCalendar('unselect');


        dialog.dialog('close');
        // $("#dialog-agendamento").dialog('close');

    }

    function formataData(data) {
        //receber DD/MM/YYYY = 01/12/2017
        //ENVIA YYYY-MM-DD
        var datnova = data.slice(6, 10) + '-' + data.slice(3, 5) + '-' + data.slice(0, 2);
        return datnova;
    }

    function formataHora(data) {
        //receber 11:00 AM
        //ENVIA 11:00
        var horanova = data.slice(0, 5);
        return horanova;
    }

    function addAgendamento() {

        var produtos = "1,2";
        //www.moradialzer.com.br/divinoAPI/web/index.php?r=divino/novo-agendamento&data=2017-10-30&hora=10:00&cliente=1&profissional=1&gerente=1&filial=1&produtos=1,2
        var url = "www.moradialzer.com.br/FTP/agendamentoAPI/web/index.php?r=divino/novo-agendamento"
        + "&data=" + formataData($('#datepicker').val())
        + "&hora=" + formataHora($('#timepicker').val())
        + "&cliente=" + $('#combobox-cliente option:selected').val()
        + "&profissional=" + $('#profissional option:selected').val()
        + "&gerente=" + usuario.result[0].ID
        + "&filial=" + filial
        + "&produtos=" + produtos;

        //TODO: obter produtos

        var jqxhr = $.get(url
            , function (data) {
                // alert("success " + JSON.stringify(data));

                // if (parseInt(data[0].ID) > 0) {
                //     setCookie("user", JSON.stringify(data));
                //     window.location.replace("ferramentas.html");
                // }
                // else {
                //     alert("Humm..... " + JSON.stringify(data));
                // }

                //TODO: UPDATE UI conforme dados do agendamento, add to calendario
                //visually attach to calendar
                addAgendamentoCalendario();


            })
        .done(function () {
//                    alert("second success");
})
        .fail(function () {
            alert("error");
        })
        .always(function () {
//                    alert("finished");
});

// Perform other work here ...

// Set another completion function for the request above
jqxhr.always(function () {
//                alert("second finished");
});


}


function cancelAgendamento(agendamento) {

    var url = "www.moradialzer.com.br/FTP/agendamentoAPI/web/index.php?r=divino/cancelar-agendamento"
    + "&agendamento=" + agendamento
    + "&gerente=" + usuario.result[0].ID;


        //TODO: obter produtos

        var jqxhr = $.get(url
            , function (data) {
                // alert("success " + JSON.stringify(data));

                // if (parseInt(data[0].ID) > 0) {
                //     setCookie("user", JSON.stringify(data));
                //     window.location.replace("ferramentas.html");
                // }
                // else {
                //     alert("Humm..... " + JSON.stringify(data));
                // }

                //TODO: UPDATE UI conforme dados do agendamento, add to calendario


                $('#calendar').fullCalendar('removeEvents', [ultimoAgendamento]);

                //remove o agendamento do calendario


            })
        .done(function () {
//                    alert("second success");
})
        .fail(function () {
            alert("error cancelling agendamento");
        })
        .always(function () {
//                    alert("finished");
});

// Perform other work here ...

// Set another completion function for the request above
jqxhr.always(function () {
//                alert("second finished");
});


}


function getDefaultData() {

    var today = new Date();
    var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();

        if (dd < 10) {
            dd = '0' + dd
        }

        if (mm < 10) {
            mm = '0' + mm
        }

        today = yyyy + '-' + mm + '-' + dd;
//        today = mm + '/' + dd + '/' + yyyy;
//        document.write(today);


return today;
        //'2017-10-12'
    }

    function getNow() {
        return myFunction();
    }

    function addZero(i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    }

    function myFunction() {
        var d = new Date();
        var h = addZero(d.getHours());
        var m = addZero(d.getMinutes());
        var s = addZero(d.getSeconds());
//        return h + ":" + m + ":" + s;
//        return h + ":" + m;
return h + ":00";
}

function addProduto(id, nome, valor, tempo) {

        // var novo = "<p>" + nome + "</p>";

        $("#table-produtos tbody").append("<tr>" +
            "<td>" + id + "</td>" +
            "<td>" + nome + "</td>" +
            "<td>" + valor + "</td>" +
            "<td>" + tempo + "</td>" +
            "<td>" + "<div class='div-remove'>Remover</div>" + "</td>" +
            "</tr>");


        $('.div-remove').click(function () {
            $(this).parent().parent().empty()
        });


        // $('#lista-produtos').html($('#lista-produtos').html() + novo);

    }

    function calendarSelect(start, end) {

        //armazena data selecionada para preenchsar o campo datepicker DATA
        dataSelecionada = start.format('DD/MM/YYYY');
        $('#datepicker').val(dataSelecionada);

        dialog.dialog('open');

//        var title = prompt('Event Title:');
//        var eventData;
//        if (title) {
//            eventData = {
//                title: title,
//                start: start,
//                end: end
//            };
//            $('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
//        }
//        $('#calendar').fullCalendar('unselect');
}

function mudaData(data, hora) {

        //receber DD/MM/YYYY = 01/12/2017


        //ENVIA YYYY-MM-DD

        var datnova = data.slice(6, 10) + '-' + data.slice(3, 5) + '-' + data.slice(0, 2);

        //start: '2017-10-01T12:30:00',


        //adiciona o tempo a data...
        //TODO: tirar daqui pq o end deve ser a hora mais a soma dos produtos.... :D
        var retorno = datnova + 'T' + hora + ':00';

        return retorno;


    }

    function isBlank(str) {
        return (!str || /^\s*$/.test(str));
    }

    function validaAddProduto() {

        if (isBlank($('#produtos option:selected').val())) {
            return false;
        }
        else
            return true;

    }

    function validaAddCliente() {
        if (isBlank($('#nome-cliente').val()) && isBlank($('#telefone-cliente').val()) && isBlank($('#email-cliente').val())) {
            return false;
        }
        else
            return true;
    }

    function validaAddAgendamento() {

        //TODO: verificar produtos
        if (clienteSelecionado === 0 || profissionalSelecionado === 0 || isBlank($('#datepicker').val()) || isBlank($('#timepicker').val())) {
            return false;
        }
        else {
            return true;
        }

    }

    function gravaNovoCliente() {

        var usuario = escapeHtml($('#nome-cliente').val());
        //var str_esc = escape(str);
        //unescape(str_esc)

        usuario = usuario.replace(' ', '_');
        usuario = usuario.toUpperCase();

        var senha = usuario + "_2017";
        var social = "nenhum";
        var foto = usuario + ".png";
        var nascimento = "01012001";


        var url = "www.moradialzer.com.br/FTP/agendamentoAPI/web/index.php?r=divino/novo-cliente"
        + "&nome=" + $('#nome-cliente').val()
        + "&telefone=" + $('#telefone-cliente').val()
        + "&usuario=" + usuario
        + "&email=" + $('#email-cliente').val()
        + "&senha=" + senha
        + "&social=" + social
        + "&foto=" + foto
        + "&nascimento=" + nascimento
        + "&filial=" + filial;


        var jqxhr = $.get(url
            , function (data) {
                if (data.error === false) {
                    // alert("success " + JSON.stringify(data));
                    alert("Cadastrado com sucesso. " + usuario);
                    //UPDATE UI
                    getClientes();
                }
                //erro
                else {
                    alert(data.errorMessage);
                }
            })
        .done(function () {
//                    alert("second success");
})
        .fail(function () {
            alert("error 1");
        })
        .always(function () {
//                    alert("finished");
});

// Perform other work here ...

// Set another completion function for the request above
jqxhr.always(function () {
//                alert("second finished");
});

}

//inicia calendario com agendamentos
getAgendamentosCalendario();



$(function () {


    function updateTips(t) {
        tips
        .text(t)
        .addClass("ui-state-highlight");
        setTimeout(function () {
            tips.removeClass("ui-state-highlight", 1500);
        }, 500);
    }

    function checkLength(o, n, min, max) {
        if (o.val().length > max || o.val().length < min) {
            o.addClass("ui-state-error");
            updateTips("Length of " + n + " must be between " +
                min + " and " + max + ".");
            return false;
        } else {
            return true;
        }
    }

    function checkRegexp(o, regexp, n) {
        if (!( regexp.test(o.val()) )) {
            o.addClass("ui-state-error");
            updateTips(n);
            return false;
        } else {
            return true;
        }
    }


    function addUser() {
        var valid = true;
        allFields.removeClass("ui-state-error");

        valid = valid && checkLength(name, "username", 3, 16);
        valid = valid && checkLength(email, "email", 6, 80);
        valid = valid && checkLength(password, "password", 5, 16);

        valid = valid && checkRegexp(name, /^[a-z]([0-9a-z_\s])+$/i, "Username may consist of a-z, 0-9, underscores, spaces and must begin with a letter.");
        valid = valid && checkRegexp(email, emailRegex, "eg. ui@jquery.com");
        valid = valid && checkRegexp(password, /^([0-9a-zA-Z])+$/, "Password field only allow : a-z 0-9");

        if (valid) {
            $("#users tbody").append("<tr>" +
                "<td>" + name.val() + "</td>" +
                "<td>" + email.val() + "</td>" +
                "<td>" + password.val() + "</td>" +
                "</tr>");
            dialog.dialog("close");
        }
        return valid;
    }

    dialog = $("#dialog-agendamento").dialog({
        zIndex: 100,
        autoOpen: false,
        dialogClass: 'mydialog',
            // appendTo: $("body"),
            height: screen.height - (screen.height * 0.4),
            width: screen.width - (screen.width * 0.3),
            modal: true,
            buttons: {
                "Agendar": function () {
                    if (validaAddAgendamento()) {
                        addAgendamento();
                    }
                    else {
                        alert('Informe todos os campos para incluir um agendamento.');
                    }
                },
                Cancel: function () {
                    dialog.dialog("close");
                }
            },
            close: function () {
                form[0].reset();
                allFields.removeClass("ui-state-error");
            }
        });

    form = dialog.find("form").on("submit", function (event) {
        event.preventDefault();


    });

});

$("#datepicker-atualiza").datepicker({
    showOtherMonths: true,
    selectOtherMonths: true
});

//     $("#accordion-calendario").accordion({
//         active: 0,
//         // animate: 200,
//         collapsible: true,
//         heightStyle: "content",
// //            header: "> div > h3",
//     });


$('#timepicker-atualiza').timepicker({
    timeFormat: 'HH:mm a',
    interval: 30,
    minTime: '10',
    maxTime: '9:00pm',
    defaultTime: '09',
    startTime: '09:00',
    dynamic: false,
    dropdown: true,
    scrollbar: true
});

$("#datepicker").datepicker({
    showOtherMonths: true,
    selectOtherMonths: true
});

//     $("#accordion-calendario").accordion({
//         active: 0,
//         // animate: 200,
//         collapsible: true,
//         heightStyle: "content",
// //            header: "> div > h3",
//     });

$("#accordion-agendamento").accordion({
    active: 1,
        // animate: 400,
        collapsible: true,
        heightStyle: "content",
//            header: "> div > h3",
});


jQuery("#timepicker-atualiza").focus(function () {

    const $element = jQuery(this);

        // move timepicker-container from body to drupal modal
        const $bootstrap_modal_content = jQuery('.modal-content');
        const $timepicker_container = jQuery('.ui-timepicker-container');
        $timepicker_container.appendTo($bootstrap_modal_content);

        // get position top and left from active element
        var element_position = $element.position()

        // Get Height of  active Element
        var element_height = $element.outerHeight();

        // additional corrections
        var extra_height = 50;

        var new_top = element_position.top + element_height + extra_height;
        var new_left = element_position.left;

        $timepicker_container.css({
            // 'top': new_top,
            // 'left': new_left,
            'z-index': '3000' // overlap other input elements
        });
    });

jQuery("#timepicker").focus(function () {

    const $element = jQuery(this);

        // move timepicker-container from body to drupal modal
        const $bootstrap_modal_content = jQuery('.modal-content');
        const $timepicker_container = jQuery('.ui-timepicker-container');
        $timepicker_container.appendTo($bootstrap_modal_content);

        // get position top and left from active element
        var element_position = $element.position()

        // Get Height of  active Element
        var element_height = $element.outerHeight();

        // additional corrections
        var extra_height = 50;

        var new_top = element_position.top + element_height + extra_height;
        var new_left = element_position.left;

        $timepicker_container.css({
            // 'top': new_top,
            // 'left': new_left,
            'z-index': '3000' // overlap other input elements
        });
    });





//deixa eles mais bonitinhos com cara de jquery-ui
$('#nome-cliente').addClass("ui-widget ui-widget-content ui-corner-all");
$('#telefone-cliente').addClass("ui-widget ui-widget-content ui-corner-all");
$('#email-cliente').addClass("ui-widget ui-widget-content ui-corner-all");
$('#nome-cliente').addClass("ui-widget ui-widget-content ui-corner-all");
$('#timepicker').addClass("ui-widget ui-widget-content ui-corner-all");
$('#datepicker').addClass("ui-widget ui-widget-content ui-corner-all");




    //COMBOBOX NOME COM PESQUISA
    $(function () {
        $.widget("custom.combobox", {
            _create: function () {
                this.wrapper = $("<span>")
                .addClass("custom-combobox")
                .insertAfter(this.element);

                this.element.hide();
                this._createAutocomplete();
                this._createShowAllButton();
            },

            _createAutocomplete: function () {
                var selected = this.element.children(":selected"),
                value = selected.val() ? selected.text() : "";

                this.input = $("<input>")
                .appendTo(this.wrapper)
                .val(value)
                .attr("title", "")
                .addClass("custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left")
                .autocomplete({
                    delay: 0,
                    minLength: 0,
                    source: $.proxy(this, "_source")
                })
                .tooltip({
                    classes: {
                        "ui-tooltip": "ui-state-highlight"
                    }
                });

                this._on(this.input, {
                    autocompleteselect: function (event, ui) {
                        ui.item.option.selected = true;
                        this._trigger("select", event, {
                            item: ui.item.option
                        });
                    },

                    autocompletechange: "_removeIfInvalid"
                });
            },

            _createShowAllButton: function () {
                var input = this.input,
                wasOpen = false;

                $("<a>")
                .attr("tabIndex", -1)
                .attr("title", "Show All Items")
                .tooltip()
                .appendTo(this.wrapper)
                .button({
                    icons: {
                        primary: "ui-icon-triangle-1-s"
                    },
                    text: false
                })
                .removeClass("ui-corner-all")
                .addClass("custom-combobox-toggle ui-corner-right")
                .on("mousedown", function () {
                    wasOpen = input.autocomplete("widget").is(":visible");
                })
                .on("click", function () {
                    input.trigger("focus");

                        // Close if already visible
                        if (wasOpen) {
                            return;
                        }

                        // Pass empty string as value to search for, displaying all results
                        input.autocomplete("search", "");
                    });
            },

            _source: function (request, response) {
                var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                response(this.element.children("option").map(function () {
                    var text = $(this).text();
                    if (this.value && ( !request.term || matcher.test(text) ))
                        return {
                            label: text,
                            value: text,
                            option: this
                        };
                    }));
            },

            _removeIfInvalid: function (event, ui) {

                // Selected an item, nothing to do
                if (ui.item) {
                    return;
                }

                // Search for a match (case-insensitive)
                var value = this.input.val(),
                valueLowerCase = value.toLowerCase(),
                valid = false;
                this.element.children("option").each(function () {
                    if ($(this).text().toLowerCase() === valueLowerCase) {
                        this.selected = valid = true;
                        return false;
                    }
                });

                // Found a match, nothing to do
                if (valid) {
                    return;
                }

                // Remove invalid value
                this.input
                .val("")
                .attr("title", value + " didn't match any item")
                .tooltip("open");
                this.element.val("");
                this._delay(function () {
                    this.input.tooltip("close").attr("title", "");
                }, 2500);
                this.input.autocomplete("instance").term = "";
            },

            _destroy: function () {
                this.wrapper.remove();
                this.element.show();
            }
        });




$("#pagamento-cliente").combobox();
$("#produtos").combobox();


$("#combobox-cliente").combobox({
    select: function (event, ui) {
        clienteSelecionado = parseInt(ui.item.value);
    }
});

$("#profissional").combobox({
    select: function (event, ui) {
        profissionalSelecionado = parseInt(ui.item.value);
    }
});

$("#toggle").on("click", function () {
    $("#combobox-cliente").toggle();
});


});


$('#addProduto').click(function () {

    if (validaAddProduto()) {

            //TODO: obter de uma lista pelo index... pois precisa do TEMPO e VALOR tambem...

            //TODO: formatar valores de currency
            addProduto($("#produtos").val(), $("#produtos option:selected").text(), 35, 40);
        }
        else {
            alert('Selecione um produto antes de incluir.')
        }


    });

$('#salvar-cliente').click(function () {

        //TODO: validacao basica dos campos antes de enviar
        if (validaAddCliente()) {

            gravaNovoCliente();

        }
        else {
            alert('Informe todos os campos para incluir um cliente.')
        }


        //TODO: Send post to API Cadastrar Novo Cliente


        //TODO: UPDATE UI Conforme dados do cliente, add to combo pesquisa


    });


    //modal de editar ou cancelar um agendamento
    $("#dialog-alert-agendamento").dialog({
        autoOpen: false,
        hide: "puff",
        show: "slide",
        width: 400,
        height: 300
    });


        //modal de esconder agendamento editar
        $("#dialog-agendamento-atualizar").dialog({
            autoOpen: false,
            hide: "puff",

        });

    //modal de editar ou cancelar um agendamento
    $("#dialog-alert-cancelamento").dialog({
        autoOpen: false,
        hide: "puff",
        show: "slide",
        width: 400,
        height: 300
    });



    //novo agendamento






    $("#editar").click(function () {
        $("#dialog-alert-agendamento").dialog('close');



        //limpa os campos!
        $('#combobox-cliente-atualizar').empty();

        //carrega dados do agendamento
        RetornaAgendamento();


        dialog = $("#dialog-agendamento-atualizar").dialog({
            zIndex: 100,
            autoOpen: false,
            dialogClass: 'mydialog',
            // appendTo: $("body"),
            height: screen.height - (screen.height * 0.4),
            width: screen.width - (screen.width * 0.3),
            modal: true,
            buttons: {
                "Agendar": function () {
                    if (validaAddAgendamento()) {
                        AtualizaAgendamento(ultimoAgendamento);
                    }
                    else {
                        alert('Informe todos os campos para incluir um agendamento.');
                    }
                },
                Cancel: function () {
                    dialog.dialog("close");
                }
            },
            close: function () {
                form[0].reset();
                allFields.removeClass("ui-state-error");
            }
        });



        $("#dialog-agendamento-atualizar").dialog('open');


    });


    $("#finalizar").click(function () {

        $("#dialog-alert-agendamento").dialog('close');

    });

    $("#cancelar").click(function () {
        $("#dialog-alert-agendamento").dialog('close');



    });

    $("#nada").click(function () {

        $("#dialog-alert-agendamento").dialog('close');

    });


    $("#sim-cancelar").click(function () {

        $("#dialog-alert-cancelamento").dialog('close');


        cancelAgendamento(ultimoAgendamento);


    });

    $("#nao-cancelar").click(function () {

        $("#dialog-alert-cancelamento").dialog('close');

    });

    $("#nao-cancelar").click(function () {

        $("#dialog-alert-cancelamento").dialog('close');

    });



})
;