
$(document).ready(function () {
    var gratuito;


    var usuario = JSON.parse(getCookie("user"));
    //        var usuario = JSON.parse(GetParam('usuario'));
    //        alert(usuario[0].Nome);


    //se nao tiver o cookie setado eh pq user nao ta logado e volta pra index
    if (usuario !== null && parseInt(usuario.result[0].ID) > 0) {
        $('#nome').text(usuario.result[0].Nome);
    }
    else {
      //  window.location.replace("login.html");
    }
    //console.log('Ready disparado');
    $('#timepicker').val("");

    var clienteSelecionado = 0;
    var profissionalSelecionado = 0;
    var dadosAgendamento = [];
    var dataSelecionada = "";
    var usuario = JSON.parse(getCookie("user"));

    var myCookie = getCookie("filial");

    if (myCookie == null){
        var filial = usuario.result[0].Filial;
    }else{
        var filial =JSON.parse(getCookie("filial"));
    }



    var ultimoAgendamento = 0;

    var dialog, form,
        // From http://www.whatwg.org/specs/web-apps/current-work/multipage/states-of-the-type-attribute.html#e-mail-state-%28type=email%29
        emailRegex = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
        name = $("#name"),
        email = $("#email"),
        password = $("#password"),
        allFields = $([]).add(name).add(email).add(password),
        tips = $(".validateTips");


    //se nao tiver o cookie setado eh pq user nao ta logado e volta pra index
    if (usuario !== null && parseInt(usuario.result[0].ID) > 0) {
        $('#nome').text(usuario.result[0].Nome);
    }
    else {
        window.location.replace("index.html");
    }



    function escapeHtml(text) {
        var map = {
            '&': '&amp;',
            '<': '&lt;',
            '>': '&gt;',
            '"': '&quot;',
            "'": '&#039;'
        };

        return text.replace(/[&<>"']/g, function (m) {
            return map[m];
        });
    }

    function dados() {
        return dadosAgendamento;
    }

    function dataAtualFormatada(){
        var data = new Date();
        var dia = data.getDate();
        if (dia.toString().length == 1)
            dia = "0"+dia;
        var mes = data.getMonth()+1;
        if (mes.toString().length == 1)
            mes = "0"+mes;
        var ano = data.getFullYear();  
        return dia+"/"+mes+"/"+ano;
    }



    function getAgendamentosCalendario() {


        // var teste = [
        //     {
        //         id: 1,
        //         title: 'Agendamento Gustavo',
        //         start: '2017-10-29T10:30:00',
        //         end: '2017-10-29T12:30:00'
        //     }
        //
        // ];

        // alert(filial);

        //OBTER DADOS DA API
        // var url = "//localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/agendamentos-calendario&filial=1";
        var url = "//localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/agendamentos-calendario&filial=" + filial;
        //console.log(url);
        var jqxhr = $.get(url
                          , function (data) {

            dadosAgendamento = data;


            iniciaCalendario();

            // if (data.error === false) {
            //     //percorre array de usuarios
            //     for (var i = 0, len = data.result.length; i < len; i++) {
            //         $('#combobox-cliente').append('<option value="' + data.result[i].ID + '" selected="selected">' + data.result[i].Nome + '</option>');
            //     }
            // }
            // else {
            //     alert("Humm..... " + JSON.stringify(data));
            // }

        })
        .done(function () {
            //                    alert("second success");

        })
        .fail(function () {
            alert("error get agendamento");
        })
        .always(function () {
            //                    alert("finished");
        });

        // Perform other work here ...

        // Set another completion function for the request above
        jqxhr.always(function () {
            //                alert("second finished");
        });


        //FAZER PARSER E RETORNAR ESTRUTURA DE JSON PARA CALENDARIO
    }




    function getClientes() {

        var url = "//localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/clientes&filial=" + filial;
        var jqxhr = $.get(url
                          , function (data) {
            if (data.error === false) {

                //limpa o combo de clientes
                $('#combobox-cliente').empty();
                //Add a empty option
                $('#combobox-cliente').append(' <option value="">Selecione um cliente...</option>');
                //percorre array de usuarios
                for (var i = 0, len = data.result.length; i < len; i++) {
                    $('#combobox-cliente').append('<option value="' + data.result[i].ID + '" ">' + data.result[i].Nome + '</option>');
                }
                $('#combobox-cliente option[value=0]').attr('selected', 'selected');
            }
            else {
                alert("Humm..... " + JSON.stringify(data));
            }
        })
        .done(function () {
            //                    alert("second success");
        })
        .fail(function () {
            alert("error getting clientes...");
        })
        .always(function () {
            //                    alert("finished");
        });

        // Perform other work here ...

        // Set another completion function for the request above
        jqxhr.always(function () {
            //                alert("second finished");
        });

    }

    function getProdutos(filial) {
        var url = "//localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/produtos&filial=" + filial;
        var jqxhr = $.get(url
                          , function (data) {
            // alert("success " + JSON.stringify(data));
            //console.log(data);

            if (data.error === false) {
                //percorre array de usuarios
                for (var i = 0, len = data.result.length; i < len; i++) {
                    // someFn(data.result[i]);
                    //TODO: adicionar os objetos em uma lista por indice para consultas futuras e gravacoes
                    $('#produtos').append('<option value="' + data.result[i].ID + '" ">' + data.result[i].Nome + '</option>');
                    //incluir tambem no produto-atualizar
                    $('#produtos-atualizar').append('<option value="' + data.result[i].ID + '" ">' + data.result[i].Nome + '</option>');
                }
                //TODO: popula campo de clientes!
            }
            else {
                alert("Humm..... " + JSON.stringify(data));
            }
        })
        .done(function () {
            //                    alert("second success");
        })
        .fail(function () {
            alert("error get produtos");
        })
        .always(function () {
            //                    alert("finished");
        });

        // Perform other work here ...

        // Set another completion function for the request above
        jqxhr.always(function () {
            //                alert("second finished");
        });

    }






    function getProfissionais() {
        var url = "//localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/profissionais&filial=" + filial;
        $('#profissional-atualizar').empty();
        $('#profissional').empty();
        var jqxhr = $.get(url
                          , function (data) {
            // alert("success " + JSON.stringify(data));
            if (data.error === false) {
                //percorre array de usuarios
                for (var i = 0, len = data.result.length; i < len; i++) {
                    $('#profissional').append('<option value="' + data.result[i].ID + '" ">' + data.result[i].Nome + '</option>');
                    $('#profissional-atualizar').append('<option value="' + data.result[i].ID + '" >' + data.result[i].Nome + '</option>');
                }
            }
            else {
                alert("Humm..... " + JSON.stringify(data));
            }

        })
        .done(function () {
            //                    alert("second success");
        })
        .fail(function () {
            alert("error ao recuperar profisisonais");
        })
        .always(function () {
            //                    alert("finished");
        });

        // Perform other work here ...

        // Set another completion function for the request above
        jqxhr.always(function () {
            //                alert("second finished");
        });

    }

    function chicao(){  

        var profissional = $('#profissional option:selected').val();
        var data = formataData($('#datepicker').val());
        var remote;

        ////localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/verifica-folga&profissional=1&data=2017-12-07
        var url = "//localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/chicao"
        + "&profissional=" + profissional
        + "&dia=" + data;


        $.ajax({
            url: url,
            data: {profissional:profissional, dia:data},
            type: 'GET',
            async: false
        }).success(function(data) {
            // alert(data);
            remote = data;

            //se for false nao pode
        }).error(function() {
            alert('falha verificafolgaatualizar');
        });

        return remote;

    }


    function iniciaCalendario() {


        $('#calendar').fullCalendar({

            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            defaultDate: getDefaultData(),
            navLinks: true, // can click day/week names to navigate views
            selectable: true,
            selectHelper: true,
            select: function (start, end) {
                calendarSelect(start, end);
            },
            unselect: function (jsEvent, view) {

            },

            editable: true,
            eventLimit: true, // allow "more" link when too many events
            dayClick: function (date, jsEvent, view) {



                //reseta horas 

                var horas = [];

                $("#timepicker").timepicker('remove');       
                $('#timepicker').timepicker({
                    'interval': 40,
                    'step': '40',
                    'timeFormat': 'H:i',
                    'minTime': '9:00am',
                    'maxTime': '11:00pm',
                    'scrollDefault': 'now',
                    'disableTimeRanges':horas

                });




                //reseta form para Novo Agendamento

                dialog = $("#dialog-agendamento").dialog({

           
                    zIndex: 80,
                    autoOpen: false,
                    dialogClass: 'mydialog',
                   resizable: true,
              
                    // appendTo: $("body"),
                    height: screen.height - (screen.height * 0.5),
                    width: screen.width - (screen.width * 0.5),
                    modal: true,
                    buttons: {
                        "Agendar": function () {
                            if (validaAddAgendamento()) {

                                var filial =JSON.parse(getCookie("filial"));

                                var horafim = calculaprodutos();
                                var regras =regrasHorario(horafim,filial);






                                //receber horario inicio
                                var horario = formataHora($('#timepicker').val());

                                var dia = formataData($('#datepicker').val());

                                var chicaoalerta = chicao();

                                var diahoje = getDefaultData();

                                //console.log(diahoje);
                                // console.log(diahoje);

                                if (diahoje <= dia){


                                    if (regras ==true) {



                                        //alert("pode cadastrar se for true... regras ="+regras);

                                        var servico = verificaservico();

                                        if (servico ==1) {
                                            var almoco = verificaalmoco(horafim);

                                            if (almoco ==true) {
                                                var folga = verificafolga();

                                                if (folga ==true) {

                                                    if(chicaoalerta == true){
                                                        //alert("pode cadastrar se for 1... servico ="+servico);
                                                        addAgendamento(); 
                                                    }
                                                    else{
                                                        alert("Esto Funcionario so Trabalha de Sexta e Sabado");
                                                    }

                                                }
                                                else{

                                                    alert("Profisisonal esta de folga'");
                                                }

                                            }
                                            else{

                                                $('.chicao').css({ display: "block" });
                                            }

                                        }
                                        else{

                                            $('.servico').css({ display: "block" });
                                        }

                                    }

                                    else{

                                        $('.regras').css({ display: "block" });
                                    }

                                }

                                else{

                                    alert("impossivel agendar em uma data anterior do dia de HOJE")
                                }

                            }
                            else {
                                $('.campoagendamento').css({ display: "block" });
                            }
                        },
                        Cancel: function () {
                            dialog.dialog("close");
                        }
                    },
                    
                    close: function () {
                        form[0].reset();
                        allFields.removeClass("ui-state-error");
                    }
                });





                //reinicar para novo agendamento

                //limpa os campos de atualizar
                //$("#table-produtos-atualizar tbody").empty();

                //limpa campos de novo agendamento
                //$('#combobox-cliente').empty();
                $("#table-produtos tbody").empty();


                //Quando clica em um DIA


                // alert('Clicked on: ' + date.format());

                // alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);

                // alert('Current view: ' + view.name);

                // change the day's background color just for fun
                // $(this).css('background-color', 'red');

            },
            eventClick: function (calEvent, jsEvent, view) {



                $("#dialog-alert-agendamento").dialog("open");

                ultimoAgendamento = calEvent.id;
                var borda;

                //reconpoes profissionais



                // alert('Event: ' + calEvent.id);
                // alert('Abrir agendamento ID: ' + calEvent.id + " - Nome: " + calEvent.title);
                // alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
                // alert('View: ' + view.name);

                // change the border color just for fun



                //  $(this).css('border-color', 'red');



            },
            events: dados()
        });

    }


    function addAgendamentoCalendarioAtualiza() {

        //            var title = 'Agendamento: ' + $('#combobox-cliente option:selected').text();
        //var title = $('#combobox-cliente option:selected').text();
        var title = $('#combobox-cliente-atualizar').val() + ' - ' + $('#profissional-atualizar option:selected').text();

        var start = $('#datepicker-atualiza').val();
        var horaStart = $('#timepicker-atualiza').val().slice(0, 5);
        var horaEnd = $('#timepicker-atualiza').val().slice(0, 5);

        start = mudaData(start, horaStart);

        //TODO: Calcular hora final com base na somatoria dos produtos  e servicos do agendamento
        var end = $('#datepicker-atualiza').val();

        var hours = horaEnd.slice(0, 2);
        var minutes = horaEnd.slice(3, 5);
        var time = moment(hours + ':' + minutes, 'HH:mm');
        //moment({hours: hours, minutes: minutes});
        time.add(60, 'm');
        //console.log(time.format("HH:mm"));

        end = mudaData(end, time.format("HH:mm"));
        //TODO: add Produtos e Servicos Time ADD for END HOUR

        var eventData;
        if (title) {
            eventData = {
                title: title,
                start: start,
                end: end
            };
            $('#calendar').fullCalendar('renderEvent', eventData, false); // stick? = true
        }
        $('#calendar').fullCalendar('unselect');


        dialog.dialog('close');
        // $("#dialog-agendamento").dialog('close');

    }

    function addAgendamentoCalendario() {

        //            var title = 'Agendamento: ' + $('#combobox-cliente option:selected').text();
        //var title = $('#combobox-cliente option:selected').text();
        var title = $('#combobox-cliente option:selected').text() + ' - ' + $('#profissional option:selected').text();

        var start = $('#datepicker').val();
        var horaStart = $('#timepicker').val().slice(0, 5);
        var horaEnd = $('#timepicker').val().slice(0, 5);

        start = mudaData(start, horaStart);

        //TODO: Calcular hora final com base na somatoria dos produtos  e servicos do agendamento
        var end = $('#datepicker').val();

        var hours = horaEnd.slice(0, 2);
        var minutes = horaEnd.slice(3, 5);
        var time = moment(hours + ':' + minutes, 'HH:mm');
        //moment({hours: hours, minutes: minutes});
        time.add(60, 'm');
        // console.log(time.format("HH:mm"));

        end = mudaData(end, time.format("HH:mm"));
        //TODO: add Produtos e Servicos Time ADD for END HOUR

        var eventData;
        if (title) {
            eventData = {
                title: title,
                start: start,
                end: end
            };
            $('#calendar').fullCalendar('renderEvent', eventData, false); // stick? = true
        }
        $('#calendar').fullCalendar('unselect');


        dialog.dialog('close');
        // $("#dialog-agendamento").dialog('close');

    }

    function formataDataRev(data) {
        //receber DD/MM/YYYY = 01/12/2017
        //ENVIA YYYY-MM-DD
        var datnova = data.slice(8, 10) + '/' + data.slice(5, 7) + '/' + data.slice(0, 4);
        return datnova;
    }

    function formataData(data) {
        //receber DD/MM/YYYY = 01/12/2017
        //ENVIA YYYY-MM-DD
        var datnova = data.slice(6, 10) + '-' + data.slice(3, 5) + '-' + data.slice(0, 2);
        return datnova;
    }

    function formataDataSQL(data) {
        //receber DD/MM/YYYY = 01/12/2017
        //ENVIA YYYY-MM-DD
        var datnova = data.slice(6, 10) + '/' + data.slice(3, 5) + '/' + data.slice(0, 2);
        return datnova;
    }

    function formataDataInv(data) {
        //receber YYYY/MM/DD
        //ENVIA MM-DD-YYYY
        var datnova = data.slice(5, 7) + '-' + data.slice(8,10) + '-' + data.slice(0, 4);
        return datnova;
    }

    function formataHora(data) {
        //receber 11:00 AM
        //ENVIA 11:00
        var horanova = data.slice(0, 5);
        return horanova;
    }

    function calculahora(horaseleciona, total){

        total -= 1;

        //tirando 1 minuto
        //total = total-1;
        var horaEnd = horaseleciona;
        var hours = horaEnd.slice(0, 2);
        var minutes = horaEnd.slice(3, 5);
        var time = moment(hours + ':' + minutes, 'HH:mm');
        //moment({hours: hours, minutes: minutes});
        time.add(total, 'm');
        horafim = time.format("HH:mm");

        return horafim;

    }

    function calculaprodutos() {

        var produtim = null;
        var total = 0;
        var total2 = 0;
        //Percorre as rows de uma table html
        $('#table-produtos tbody tr').each(function() { 
            var produtim = $(this).find('td:eq(3)');

            if(produtim.length > 0) { // verifica se ta okey
                total += parseInt(produtim.html().trim()); 

            }

        });

        //tr
        //total2 = Math.floor(total/60);
        // total = Math.floor(total % 60);
        //tempototal = total2+":"+ total;

        //recolher hora selecionada + tempo total = HoraFim
        var horaseleciona = $('#timepicker').val();
        horafim = calculahora(horaseleciona, total);
        return horafim;
    }

    function calculaprodutosatualizar() {

        var produtim = null;
        var total = 0;
        var total2 = 0;
        //Percorre as rows de uma table html
        $('#table-produtos-atualizar tbody tr').each(function() { 
            var produtim = $(this).find('td:eq(3)');

            if(produtim.length > 0) { // verifica se ta okey
                total += parseInt(produtim.html().trim()); 
            }
        });

        //tr
        //total2 = Math.floor(total/60);
        // total = Math.floor(total % 60);
        //tempototal = total2+":"+ total;

        //recolher hora selecionada + tempo total = HoraFim
        var horaseleciona = $('#timepicker-atualiza').val();

        horafim = calculahora(horaseleciona, total);
        return horafim;


    }


    function verificaservicoatualizar() {
        var contador;
        //Percorre as rows de uma table html
        $('#table-produtos-atualizar tbody tr').each(function(i) { 
            var produtim = $(this).find('td:eq(4)');

            if(produtim.length > 0) { // verifica se ta okey
                if(produtim.html().trim() ==1){
                    //alert("com produto servico");
                    contador =1;

                }
            }
        });

        return contador;
    }



    function verificaservico() {
        var contador;
        //Percorre as rows de uma table html
        $('#table-produtos tbody tr').each(function(i) { 
            var produtim = $(this).find('td:eq(4)');

            if(produtim.length > 0) { // verifica se ta okey
                if(produtim.html().trim() ==1){
                    //alert("com produto servico");
                    contador =1;
                }
            }
        });
        return contador;
    }



    function lertab() {

        var arr_resp = [];
        //Percorre as rows de uma table html
        $('#table-produtos tbody tr').each(function(i) { 
            var produtim = $(this).find('td:eq(0)');

            if(produtim.length > 0) { // verifica se ta okey
                arr_resp[i] = produtim.html().trim(); 
            }
        });
        return arr_resp;

    }

    function lertabatualizar() {

        var arr_resp = [];
        //Percorre as rows de uma table html
        $('#table-produtos-atualizar tbody tr').each(function(i) { 
            var produtim = $(this).find('td:eq(0)');

            if(produtim.length > 0) { // verifica se ta okey
                arr_resp[i] = produtim.html().trim(); 
            }
        });
        return arr_resp;

    }

    function verificaalmocoatualizar(horafim){  

        var profissional = $('#profissional-atualizar option:selected').val();
        var hora = formataHora($('#timepicker-atualiza').val());
        var remote;
        ////localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/verifica-almoco&profissional=1&hora=12:00&horafim=13:00
        var url = "//localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/verifica-almoco"
        + "&profissional=" + profissional
        + "&hora=" + hora
        + "&horafim=" + horafim;


        $.ajax({
            url: url,
            data: {profissional:profissional, hora:hora, horafim:horafim},
            type: 'GET',
            async: false
        }).success(function(data) {
            // alert(data);
            remote = data;

            //se for false nao pode
        }).error(function() {
            alert('falha verificaalmocoatualizar');
        });

        return remote;

    }




    function verificafolgaatualizar(){  

        var profissional = $('#profissional-atualizar option:selected').val();
        var data = formataData($('#datepicker-atualiza').val());
        var remote;

        ////localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/verifica-folga&profissional=1&data=2017-12-07
        var url = "//localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/verifica-folga"
        + "&profissional=" + profissional
        + "&data=" + data;


        $.ajax({
            url: url,
            data: {profissional:profissional, data:data},
            type: 'GET',
            async: false
        }).success(function(data) {
            // alert(data);
            remote = data;

            //se for false nao pode
        }).error(function() {
            alert('falha verificafolgaatualizar');
        });

        return remote;

    }




    function verificaalmoco(horafim){  

        var profissional = $('#profissional option:selected').val();
        var hora = formataHora($('#timepicker').val());
        var remote;

        ////localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/verifica-almoco&profissional=1&hora=12:00&horafim=13:00
        var url = "//localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/verifica-almoco"
        + "&profissional=" + profissional
        + "&hora=" + hora
        + "&horafim=" + horafim;


        $.ajax({
            url: url,
            data: {profissional:profissional, hora:hora, horafim:horafim},
            type: 'GET',
            async: false
        }).success(function(data) {
            // alert(data);
            remote = data;

            //se for false nao pode
        }).error(function() {
            alert('falha verificaalmoco');
        });

        return remote;

    }


    function verificafolga(){  

        var profissional = $('#profissional option:selected').val();
        var data = formataData($('#datepicker').val());
        var remote;

        ////localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/verifica-folga&profissional=1&data=2017-12-07
        var url = "//localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/verifica-folga"
        + "&profissional=" + profissional
        + "&data=" + data;


        $.ajax({
            url: url,
            data: {profissional:profissional, data:data},
            type: 'GET',
            async: false
        }).success(function(data) {
            // alert(data);
            remote = data;

            //se for false nao pode
        }).error(function() {
            alert('falha verificafolga');
        });

        return remote;

    }


    function regrasHorarioatualizar(horafim, ultimoAgendamento, filial){  

        var filial =JSON.parse(getCookie("filial"));

        var profissional = $('#profissional-atualizar option:selected').val();
        var data = formataData($('#datepicker-atualiza').val());
        var hora = formataHora($('#timepicker-atualiza').val());
        var remote;

        ////localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/regras-horario&id=1&profissional=1&data=2017-12-07&hora=9:00&horafim=11:00
        var url = "//localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/regras-horario";





        $.ajax({
            url: url,
            data: {profissional:profissional,  filial:filial, data:data, hora:hora, horafim:horafim, id:ultimoAgendamento},
            type: 'GET',
            async: false
        }).success(function(data) {
            // alert(data);
            remote = data;

            //se for false nao pode
        }).error(function() {
            alert('falha ao salvar');
        });

        return remote;

    }


    function regrasHorario(horafim, id, filial){  

        var filial =JSON.parse(getCookie("filial"));

        var profissional = $('#profissional option:selected').val();
        var data = formataData($('#datepicker').val());
        var hora = formataHora($('#timepicker').val());
        var remote;

        ////localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/regras-horario&profissional=1&data="2017-11-02"&hora="10:00"&horafim="11:20
        var url = "//localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/regras-horario";
        //console.log(filial);

        $.ajax({
            url: url,
            data: {profissional:profissional, filial:filial, data:data, hora:hora, horafim:horafim, id:0},
            type: 'GET',
            async: false
        }).success(function(data) {
            // alert(data);
            remote = data;

            //se for false nao pode
        }).error(function() {
            alert('falha nas regras horario');
        });

        return remote;

    }


    function addAgendamento() {


        var horafim = calculaprodutos();
        var produtos = lertab();


        ////localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/novo-agendamento&data=2017-10-30&hora=10:00&cliente=1&profissional=1&gerente=1&filial=1&produtos=1,2
        var url = "//localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/novo-agendamento"
        + "&data=" + formataData($('#datepicker').val())
        + "&hora=" + formataHora($('#timepicker').val())
        + "&horafim=" + horafim
        + "&cliente=" + $('#combobox-cliente option:selected').val()
        + "&profissional=" + $('#profissional option:selected').val()
        + "&gerente=" + usuario.result[0].ID
        + "&filial=" + filial
        + "&produtos=" + produtos;

        //console.log(url);



        //TODO: obter produtos

        var jqxhr = $.get(url
                          , function (data) {
            // alert("success " + JSON.stringify(data));

            // if (parseInt(data[0].ID) > 0) {
            //     setCookie("user", JSON.stringify(data));
            //     window.location.replace("ferramentas.html");
            // }
            // else {
            //     alert("Humm..... " + JSON.stringify(data));
            // }

            //TODO: UPDATE UI conforme dados do agendamento, add to calendario
            //visually attach to calendar
            addAgendamentoCalendario();
            window.location.reload();



        })
        .done(function () {
            //                    alert("second success");
        })
        .fail(function () {
            alert("error add agendamento");
        })
        .always(function () {
            //                    alert("finished");
        });

        // Perform other work here ...

        // Set another completion function for the request above
        jqxhr.always(function () {
            //                alert("second finished");
            //window.location.reload()
        });


    }



    function AtualizaAgendamento(ultimoAgendamento) {


        var horafim = calculaprodutosatualizar();
        var produtos = lertabatualizar();

        //console.log(horafim);

        ////localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/atualiza-agendamento&id=2&data=2017-10-30&hora=10:00&cliente=1&profissional=1&gerente=1&filial=1&produtos=1,2
        var url = "//localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/atualiza-agendamento"
        + "&id=" + ultimoAgendamento
        + "&data=" + formataData($('#datepicker-atualiza').val())
        + "&hora=" + formataHora($('#timepicker-atualiza').val())
        + "&horafim=" + horafim
        + "&cliente=" + $('#combobox-cliente-atualizar').text()
        + "&profissional=" + $('#profissional-atualizar option:selected').val()
        + "&gerente=" + usuario.result[0].ID
        + "&filial=" + filial
        + "&produtos=" + produtos;

        //console.log(url);

        //TODO: obter produtos

        var jqxhr = $.get(url
                          , function (data) {
            // alert("success " + JSON.stringify(data));

            // if (parseInt(data[0].ID) > 0) {
            //     setCookie("user", JSON.stringify(data));
            //     window.location.replace("ferramentas.html");
            // }
            // else {
            //     alert("Humm..... " + JSON.stringify(data));
            // }

            //TODO: UPDATE UI conforme dados do agendamento, add to calendario
            //visually attach to calendar
            $('#calendar').fullCalendar('removeEvents', [ultimoAgendamento]);
            addAgendamentoCalendarioAtualiza();
            window.location.reload();


        })
        .done(function () {
            //                    alert("second success");
        })
        .fail(function () {
            alert("error atualiza agendamento");
        })
        .always(function () {
            //                    alert("finished");
        });

        // Perform other work here ...

        // Set another completion function for the request above
        jqxhr.always(function () {
            //                alert("second finished");
        });


    }

    function finalizaAgendamento(agendamento) {

        var url = "//localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/finaliza-agendamento"
        + "&agendamento=" + agendamento
        + "&gerente=" + usuario.result[0].ID;


        //TODO: obter produtos

        var jqxhr = $.get(url
                          , function (data) {
            // alert("success " + JSON.stringify(data));

            // if (parseInt(data[0].ID) > 0) {
            //     setCookie("user", JSON.stringify(data));
            //     window.location.replace("ferramentas.html");
            // }
            // else {
            //     alert("Humm..... " + JSON.stringify(data));
            // }

            //TODO: UPDATE UI conforme dados do agendamento, add to calendario


            $('#calendar').fullCalendar('removeEvents', [ultimoAgendamento]);

            //remove o agendamento do calendario


        })
        .done(function () {
            //                    alert("second success");
        })
        .fail(function () {
            alert("error cancelling agendamento");
        })
        .always(function () {
            //                    alert("finished");
        });

        // Perform other work here ...

        // Set another completion function for the request above
        jqxhr.always(function () {
            //                alert("second finished");
        });


    }


    function finalizaPagamento(pagamento,ultimoAgendamento) {

        var url = "//localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/finaliza-pagamento"
        + "&pagamento=" + pagamento
        + "&ultimoAgendamento="+ ultimoAgendamento;


        //TODO: obter produtos

        var jqxhr = $.get(url
                          , function (data) {


        })
        .done(function () {
            //                    alert("second success");
        })
        .fail(function () {
            alert("error cancelling agendamento");
        })
        .always(function () {
            //                    alert("finished");
        });

        // Perform other work here ...

        // Set another completion function for the request above
        jqxhr.always(function () {
            //                alert("second finished");
        });


    }


    function cancelAgendamento(agendamento, justificativa) {

        var url = "//localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/cancelar-agendamento"
        + "&agendamento=" + agendamento
        + "&justificativa=" + justificativa
        + "&gerente=" + usuario.result[0].ID;


        //TODO: obter produtos

        var jqxhr = $.get(url
                          , function (data) {
            // alert("success " + JSON.stringify(data));

            // if (parseInt(data[0].ID) > 0) {
            //     setCookie("user", JSON.stringify(data));
            //     window.location.replace("ferramentas.html");
            // }
            // else {
            //     alert("Humm..... " + JSON.stringify(data));
            // }

            //TODO: UPDATE UI conforme dados do agendamento, add to calendario


            $('#calendar').fullCalendar('removeEvents', [ultimoAgendamento]);

            //remove o agendamento do calendario


        })
        .done(function () {
            //                    alert("second success");
        })
        .fail(function () {
            alert("error cancelling agendamento");
        })
        .always(function () {
            //                    alert("finished");
        });

        // Perform other work here ...

        // Set another completion function for the request above
        jqxhr.always(function () {
            //                alert("second finished");
        });


    }


    function getDefaultData() {

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();

        if (dd < 10) {
            dd = '0' + dd
        }

        if (mm < 10) {
            mm = '0' + mm
        }

        today = yyyy + '-' + mm + '-' + dd;
        //        today = mm + '/' + dd + '/' + yyyy;
        //        document.write(today);


        return today;
        //'2017-10-12'
    }

    function getNow() {
        return myFunction();
    }

    function addZero(i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    }

    function myFunction() {
        var d = new Date();
        var h = addZero(d.getHours());
        var m = addZero(d.getMinutes());
        var s = addZero(d.getSeconds());
        //        return h + ":" + m + ":" + s;
        //        return h + ":" + m;
        return h + ":00";
    }

    function addProduto(id, nome, valor, tempo, servico) {

        // var novo = "<p>" + nome + "</p>";

        $("#table-produtos tbody").append("<tr>" +
                                          "<td style=' display: none;'>" + id + "</td>" +
                                          "<td>" + nome + "</td>" +
                                          "<td>" + valor + "</td>" +
                                          "<td>" + tempo + "</td>" +
                                          "<td>" + servico + "</td>" +
                                          "<td>" + "<div class='div-remove'>Remover</div>" + "</td>" +
                                          "</tr>");


        $('.div-remove').click(function () {
            $(this).parent().parent().empty()
        });

        $("#table-produtos-atualizar tbody").append("<tr>" +
                                                    "<td style=' display: none;'>" + id + "</td>" +
                                                    "<td>" + nome + "</td>" +
                                                    "<td>" + valor + "</td>" +
                                                    "<td>" + tempo + "</td>" +
                                                    "<td>" + servico + "</td>" +
                                                    "<td>" + "<div class='div-remove-atualizar'>Remover</div>" + "</td>" +
                                                    "</tr>");


        $('.div-remove-atualizar').click(function () {
            $(this).parent().parent().remove()
        });



        // $('#lista-produtos').html($('#lista-produtos').html() + novo);

    }

    function calendarSelect(start, end) {

        //armazena data selecionada para preenchsar o campo datepicker DATA
        dataSelecionada = start.format('DD/MM/YYYY');
        $('#datepicker').val(dataSelecionada);


        dialog.dialog('open');

        $data = formataDataSQL(dataSelecionada);


        //ao clicar em uam data mostra profisisonais disponiveis
        DatepickerProfossionaisDisponiveis($data);




        //        var title = prompt('Event Title:');
        //        var eventData;
        //        if (title) {
        //            eventData = {
        //                title: title,
        //                start: start,
        //                end: end
        //            };
        //            $('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
        //        }
        //        $('#calendar').fullCalendar('unselect');
    }


    //funcao q clica em dia e retorna profisisonais
    function DatepickerProfossionaisDisponiveis(dataSelecionada){
        //dia = profisisonais


        //conulta profisisonais disponiveis na data
        var data = $('#datepicker').val();


        $idprofisisonais = actionProfDispDia(dataSelecionada);
        //console.log($idprofisisonais);
        getProfissionaisID($idprofisisonais);
        //clean profisisonais
        //funcao que mostra so os profisisonais


    }



    function DatepickerProfossionaisDisponiveisatualiza(dataSelecionada){
        //dia = profisisonais
        if($('#profissional-atualizar').val() != 'x' && $('#timepicker-atuliza').val() != null ){

            //conulta profisisonais disponiveis na data
            var data = $('#datepicker-atualiza').val();


            $idprofisisonais = actionProfDispDia(data);
            //console.log($idprofisisonais);
            getProfissionaisIDAtualiza($idprofisisonais);
            //clean profisisonais
            //funcao que mostra so os profisisonais

        }
    }



    function actionProfDispDia(dataSelecionada) {
        ////localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/prof-disp-dia&dia=2017-12-15
        var url = "//localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/prof-disp-dia";
        var remote;
        $.ajax({
            url: url,
            data: {dia:dataSelecionada},
            type: 'GET',
            async: false
        }).success(function(data) {
            if(data===null){
                remote = 0;

            }else{

                remote = data;
            }



            //se for false nao pode
        }).error(function() {
            alert('falha verificacao de dias disponiveis do profisisonal com hora');
        });

        return remote;
    }
    function mudaData(data, hora) {

        //receber DD/MM/YYYY = 01/12/2017
        //ENVIA YYYY-MM-DD
        var datnova = data.slice(6, 10) + '-' + data.slice(3, 5) + '-' + data.slice(0, 2);
        //start: '2017-10-01T12:30:00',
        //adiciona o tempo a data...
        //TODO: tirar daqui pq o end deve ser a hora mais a soma dos produtos.... :D
        var retorno = datnova + 'T' + hora + ':00';
        return retorno;
    }

    function isBlank(str) {
        return (!str || /^\s*$/.test(str));
    }

    function validaAddProduto() {

        if (isBlank($('#produtos option:selected').val())) {
            return false;
        }
        else
            return true;

    }

    function validaAddCliente() {
        if (isBlank($('#nome-cliente').val()) && isBlank($('#telefone-cliente').val()) && isBlank($('#email-cliente').val())) {
            return false;
        }
        else
            return true;
    }

    function validaAddAgendamento() {
        profissionalSelecionado = $("#profissional").val();
        clienteSelecionado = $("#combobox-cliente").val();

        //TODO: verificar produtos
        if (clienteSelecionado === 0 || profissionalSelecionado === 0 || profissionalSelecionado == 'x' || isBlank($('#datepicker').val()) || isBlank($('#timepicker').val())) {

            return false;
        }
        else {
            return true;
        }

    }


    function validaAddAgendamentoAtualizar() {
        clienteSelecionado = $("#combobox-cliente-atualizar").text();
        profissionalSelecionado = $("#profissional-atualizar").val();
        //alert(clienteSelecionado);
        //alert(profissionalSelecionado);
        //TODO: verificar produtos
        if (clienteSelecionado === 0 || profissionalSelecionado == "x" || isBlank($('#datepicker-atualiza').val()) || isBlank($('#timepicker-atualiza').val())) {

            return false;
        }
        else {
            return true;
        }

    }

    function gravaNovoCliente() {

        var usuario = escapeHtml($('#nome-cliente').val());
        //var str_esc = escape(str);
        //unescape(str_esc)

        usuario = usuario.replace(' ', '_');
        usuario = usuario.toUpperCase();

        var senha = usuario + "_2017";
        var social = "nenhum";
        var foto = usuario + ".png";
        var cpf= $('#aniversario-cliente').val();


        var url = "//localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/novo-cliente"
        + "&nome=" + $('#nome-cliente').val()
        + "&telefone=" + $('#telefone-cliente').val()
        + "&usuario=" + usuario
        + "&email=" + $('#email-cliente').val()
        + "&senha=" + senha
        + "&social=" + social
        + "&foto=" + foto
        + "&aniversario=" + $('#aniversario-cliente').val()
        + "&cpf=" + $('#cpf-cliente').val()
        + "&filial=" + filial;




        var jqxhr = $.get(url
                          , function (data) {
            if (data.error === false) {
                // alert("success " + JSON.stringify(data));
                alert("Cadastrado com sucesso. " + usuario);
                //UPDATE UI
                getClientes();
            }
            //erro
            else {
                alert(data.errorMessage);
            }
        })
        .done(function () {
            //                    alert("second success");
        })
        .fail(function () {
            alert("error grava novo cliente");
        })
        .always(function () {
            //                    alert("finished");
        });

        // Perform other work here ...

        // Set another completion function for the request above
        jqxhr.always(function () {
            //                alert("second finished");
        });

    }



    function ConsultaDiasDisponiveis() {

        ////localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/dias-disponiveis&profissional=1

        var url = "//localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/dias-disponiveis";
        var profissional = $('#profissional').val();
        var x = [];
        $.ajax({
            url: url,
            data: {profissional:profissional},
            type: 'GET',
            async: false
        }).success(function(data) {

            for (var i = 0, len = data.length; i < len; i++) {

                x[i] = formataDataInv(data[i]);

            }
            //console.log(x)
            setCookie("dias", JSON.stringify(x));
            //return x;
            //se for false nao pode
        }).error(function() {
            alert('falha verificacao de dias disponiveis do profisisonal');
        });

        return x;
    }

    function ConsultaDiasDisponiveisatualizar() {

        ////localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/dias-disponiveis&profissional=1

        var url = "//localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/dias-disponiveis";
        var profissional = $('#profissional').val();
        var x = [];
        $.ajax({
            url: url,
            data: {profissional:profissional},
            type: 'GET',
            async: false
        }).success(function(data) {

            for (var i = 0, len = data.length; i < len; i++) {

                x[i] = formataDataInv(data[i]);

            }
            //console.log(x)
            setCookie("dias", JSON.stringify(x));
            return x;
            //se for false nao pode
        }).error(function() {
            alert('falha verificacao de dias disponiveis do profisisonal');
        });

        return x;
    }









    function RetornaAgendamento() {

        ////localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/retorna-agendamento&ultimoAgendamento=1
        var url = "//localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/retorna-agendamento"
        + "&ultimoAgendamento=" + ultimoAgendamento;


        //TODO: obter produtos

        var jqxhr = $.get(url
                          , function (data) {

            //inserir valor no input

            if (data.error === false) {

                //console.log(data.result[0].Profissional);
                //percorre array de usuarios
                for (var i = 0, len = data.result.length; i < len; i++) {
                    $('#combobox-cliente-atualizar').val(data.result[0].cliente.Nome);
                    $('#combobox-cliente-atualizar').text(data.result[0].cliente.ID);

                    $('#combobox-cliente-telefone').val(data.result[0].cliente.Telefone);
                    //inserir valor no combobox
                    // $('#combobox-cliente-atualizar').append('<option value="' + data.result[i].ID + '" selected="selected">' + data.result[i].cliente.Nome + '</option>');


                    //colocar profisisonal como selected!


                    $('#profissional-atualizar option[value='+data.result[0].Profissional+']').attr('selected','selected'); 




                    //preeenche data
                    $('#datepicker-atualiza').val(formataDataRev(data.result[0].Data));


                    //preeenche hora
                    $('#timepicker-atualiza').val(data.result[0].Hora);

                    $("#table-produtos-atualizar tbody").empty();

                    for (var j = 0, len = data.result[0].produtos.length; j < len; j++) {


                        $("#table-produtos-atualizar tbody").append("<tr>" +
                                                                    "<td style=' display: none;'>" + data.result[0].produtos[j].produto.ID + "</td>" +
                                                                    "<td>" + data.result[0].produtos[j].produto.Nome + "</td>" +
                                                                    "<td>" + data.result[0].produtos[j].produto.Venda + "</td>" +
                                                                    "<td>" + data.result[0].produtos[j].produto.Tempo + "</td>" +
                                                                    "<td>" + data.result[0].produtos[j].produto.Servico + "</td>" +
                                                                    "<td>" + "<div class='div-remove-atualizar'>Remover</div>" + "</td>" +
                                                                    "</tr>");



                        $('.div-remove-atualizar').click(function () {
                            $(this).parent().parent().remove()
                        });

                    }




                }
            }
            else {
                alert("Humm..... " + JSON.stringify(data));
            } 
            // $('#combobox-cliente').append('<option>fernando</option>');
            //coloca cliente no combobox
            //$('#combobox-cliente').append('<option>' + data.result.ID + '</option>');
            //$('#combobox-cliente').append('<option>' + data.result.cliente.Nome + '</option>');



        })
        .done(function () {
            //                    alert("second success");
        })
        .fail(function () {
            alert("error retorna agendamento");
        })
        .always(function () {
            //                    alert("finished");
        });

        // Perform other work here ...

        // Set another completion function for the request above
        jqxhr.always(function () {
            //                alert("second finished");
            // window.location.reload()
        });


    }








    $(function () {



        //inicia calendario com agendamentos
        //carrega dados
        getClientes();
        getProdutos(filial);
        getProfissionais();
        getAgendamentosCalendario();


        function updateTips(t) {
            tips
                .text(t)
                .addClass("ui-state-highlight");
            setTimeout(function () {
                tips.removeClass("ui-state-highlight", 1500);
            }, 500);
        }

        function checkLength(o, n, min, max) {
            if (o.val().length > max || o.val().length < min) {
                o.addClass("ui-state-error");
                updateTips("Length of " + n + " must be between " +
                           min + " and " + max + ".");
                return false;
            } else {
                return true;
            }
        }

        function checkRegexp(o, regexp, n) {
            if (!( regexp.test(o.val()) )) {
                o.addClass("ui-state-error");
                updateTips(n);
                return false;
            } else {
                return true;
            }
        }


        function addUser() {
            var valid = true;
            allFields.removeClass("ui-state-error");

            valid = valid && checkLength(name, "username", 3, 16);
            valid = valid && checkLength(email, "email", 6, 80);
            valid = valid && checkLength(password, "password", 5, 16);

            valid = valid && checkRegexp(name, /^[a-z]([0-9a-z_\s])+$/i, "Username may consist of a-z, 0-9, underscores, spaces and must begin with a letter.");
            valid = valid && checkRegexp(email, emailRegex, "eg. ui@jquery.com");
            valid = valid && checkRegexp(password, /^([0-9a-zA-Z])+$/, "Password field only allow : a-z 0-9");

            if (valid) {
                $("#users tbody").append("<tr>" +
                                         "<td>" + name.val() + "</td>" +
                                         "<td>" + email.val() + "</td>" +
                                         "<td>" + password.val() + "</td>" +
                                         "</tr>");
                dialog.dialog("close");
            }
            return valid;
        }

        dialog = $("#dialog-agendamento").dialog({
            zIndex: 100,
            autoOpen: false,
            dialogClass: 'mydialog',
            // appendTo: $("body"),
            height: screen.height - (screen.height * 0.4),
            width: screen.width - (screen.width * 0.3),
            modal: true,
            buttons: {
                "Agendar": function () {
                    if (validaAddAgendamento()) {
                    }
                    else {
                        $('.campoagendamento').css({ display: "block" });
                    }
                },
                Cancel: function () {
                    dialog.dialog("close");
                }
            },
            close: function () {
                form[0].reset();
                allFields.removeClass("ui-state-error");
            }
        });

        form = dialog.find("form").on("submit", function (event) {
            event.preventDefault();


        });

    });

    $("#datepicker-atualiza").datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        beforeShowDay: DisableFeriados
    });

    //     $("#accordion-calendario").accordion({
    //         active: 0,
    //         // animate: 200,
    //         collapsible: true,
    //         heightStyle: "content",
    // //            header: "> div > h3",
    //     });


    $('#timepicker-atualiza').timepicker({
        'interval': 40,
        'step': '40',
        'timeFormat': 'H:i',
        'minTime': '9:00am',
        'maxTime': '11:00pm',
        'scrollDefault': 'now'
    });





    $('#timepicker').timepicker({
        'interval': 40,
        'step': '40',
        'timeFormat': 'H:i',
        'minTime': '9:00am',
        'maxTime': '11:00pm',
        'scrollDefault': 'now'

    });


    $("#aniversario-cliente").datepicker({
        showOtherMonths: true,
        changeMonth: true,
        changeYear: true,
        selectOtherMonths: true,
        beforeShowDay: DisableFeriados
    });



    //$('#timepicker').timepicker('destroy');


    //$('#timepicker').timepicker('setHoursDisabled', [10,11]);


    //$('#timepicker').timepicker('update');




    $("#datepicker").datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        beforeShowDay: DisableFeriados


    });





    //     $("#accordion-calendario").accordion({
    //         active: 0,
    //         // animate: 200,
    //         collapsible: true,
    //         heightStyle: "content",
    // //            header: "> div > h3",
    //     });

    $("#accordion-agendamento").accordion({
        active: 1,
        // animate: 400,
        collapsible: true,
        heightStyle: "content",
        //            header: "> div > h3",
    });


    function DisableFeriados(date) {

        var m = date.getMonth();
        var d = date.getDate();
        var y = date.getFullYear();

        // First convert the date in to the mm-dd-yyyy format 
        // Take note that we will increment the month count by 1 
        var currentdate = (m + 1) + '-' + d + '-' + y ;

        // We will now check if the date belongs to disableddates array for (var i = 0; i < disableddates.length; i++) {
        // Now check if the current date is in disabled dates array. 
        if ($.inArray(currentdate) != -1 ) {
            return [false];
        } 
        //desabilitando domingos
        var day = date.getDay();
        // If day == 1 then it is MOnday
        if (day == 0) {

            return [false] ; 

        } else { 

            return [true] ;
        }

        var weekenddate = $.datepicker.noWeekends(date);
        return weekenddate; 

    }








    //deixa eles mais bonitinhos com cara de jquery-ui
    $('#nome-cliente').addClass("ui-widget ui-widget-content ui-corner-all");
    $('#telefone-cliente').addClass("ui-widget ui-widget-content ui-corner-all");
    $('#email-cliente').addClass("ui-widget ui-widget-content ui-corner-all");
    $('#nome-cliente').addClass("ui-widget ui-widget-content ui-corner-all");
    $('#timepicker').addClass("ui-widget ui-widget-content ui-corner-all");
    $('#datepicker').addClass("ui-widget ui-widget-content ui-corner-all");




    //COMBOBOX NOME COM PESQUISA
    $(function () {
        $.widget("custom.combobox", {
            _create: function () {
                this.wrapper = $("<span>")
                    .addClass("custom-combobox")
                    .insertAfter(this.element);

                this.element.hide();
                this._createAutocomplete();
                this._createShowAllButton();
            },

            _createAutocomplete: function () {
                var selected = this.element.children(":selected"),
                    value = selected.val() ? selected.text() : "";

                this.input = $("<input>")
                    .appendTo(this.wrapper)
                    .val(value)
                    .attr("title", "")
                    .addClass("custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left")
                    .autocomplete({
                    delay: 0,
                    minLength: 0,
                    source: $.proxy(this, "_source")
                })
                    .tooltip({
                    classes: {
                        "ui-tooltip": "ui-state-highlight"
                    }
                });

                this._on(this.input, {
                    autocompleteselect: function (event, ui) {
                        ui.item.option.selected = true;
                        this._trigger("select", event, {
                            item: ui.item.option
                        });
                    },

                    autocompletechange: "_removeIfInvalid"
                });
            },

            _createShowAllButton: function () {
                var input = this.input,
                    wasOpen = false;

                $("<a>")
                    .attr("tabIndex", -1)
                    .attr("title", "Show All Items")
                    .tooltip()
                    .appendTo(this.wrapper)
                    .button({
                    icons: {
                        primary: "ui-icon-triangle-1-s"
                    },
                    text: false
                })
                    .removeClass("ui-corner-all")
                    .addClass("custom-combobox-toggle ui-corner-right")
                    .on("mousedown", function () {
                    wasOpen = input.autocomplete("widget").is(":visible");
                })
                    .on("click", function () {
                    input.trigger("focus");

                    // Close if already visible
                    if (wasOpen) {
                        return;
                    }

                    // Pass empty string as value to search for, displaying all results
                    input.autocomplete("search", "");
                });
            },

            _source: function (request, response) {
                var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                response(this.element.children("option").map(function () {
                    var text = $(this).text();
                    if (this.value && ( !request.term || matcher.test(text) ))
                        return {
                            label: text,
                            value: text,
                            option: this
                        };
                }));
            },

            _removeIfInvalid: function (event, ui) {

                // Selected an item, nothing to do
                if (ui.item) {
                    return;
                }

                // Search for a match (case-insensitive)
                var value = this.input.val(),
                    valueLowerCase = value.toLowerCase(),
                    valid = false;
                this.element.children("option").each(function () {
                    if ($(this).text().toLowerCase() === valueLowerCase) {
                        this.selected = valid = true;
                        return false;
                    }
                });

                // Found a match, nothing to do
                if (valid) {
                    return;
                }

                // Remove invalid value
                this.input
                    .val("")
                    .attr("title", value + " didn't match any item")
                    .tooltip("open");
                this.element.val("");
                this._delay(function () {
                    this.input.tooltip("close").attr("title", "");
                }, 2500);
                this.input.autocomplete("instance").term = "";
            },

            _destroy: function () {
                this.wrapper.remove();
                this.element.show();
            }








        });

        //pegando valores e para validar atualizar agendamento

        $("#produtos-atualizar").combobox();



        function getprodutoporidatualizar(id,filial){  
            ////localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/produtos-id&id=1
            var url = "//localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/produtos-id"
            + "&id=" + id
            + "&filial=" + filial;

            //TODO: obter produtos

            var jqxhr = $.get(url
                              , function (data) {


                if(data.result.Servico != 1){





                    if(parseInt(data.result.Estoque) <= parseInt(data.result.EstoqueMinimo)){

                        $('.estoqueminimo').css({ display: "block" });
                    }

                    if(parseInt(data.result.Estoque < 1)){

                        $('.semprodutoestoque').css({ display: "block" });
                    }
                    else{
                        addProduto($("#produtos-atualizar").val(), $("#produtos-atualizar option:selected").text(),data.result.Venda,data.result.Tempo, data.result.Servico);
                    }
                }
                else{
                    addProduto($("#produtos-atualizar").val(), $("#produtos-atualizar option:selected").text(),data.result.Venda,data.result.Tempo, data.result.Servico);
                }



            })
            .done(function () {
                //                    alert("second success");
            })
            .fail(function () {
                alert("error na adicao do produto");
            })
            .always(function () {
                //                    alert("finished");
            });

            // Perform other work here ...

            // Set another completion function for the request above
            jqxhr.always(function () {
                //                alert("second finished");
            });


        }




        $('#addProduto-atualizar').click(function () {


            if (validaAddProduto()) {
                //TODO: obter de uma lista pelo index... pois precisa do TEMPO e VALOR tambem...
                //TODO: formatar valores de currency
                //recebe id do produto selecionado
                var id =$("#produtos-atualizar option:selected").val();


                //getproduto por id e adc ele com a funcao add dentro do get
                getprodutoporidatualizar(id,filial);

            }
            else {

                $('.selecioneproduto').css({ display: "block" });


            }



        });



        //pegando valores e para validar novo agendamento
        $("#pagamento-cliente").combobox();
        $("#produtos").combobox();





        $("#combobox-cliente").combobox({
            select: function (event, ui) {
                clienteSelecionado = parseInt(ui.item.value);
            }
        });




        $("#toggle").on("click", function () {
            $("#combobox-cliente").toggle();
        });




        //funcao timepicker dasable date
        function TimePickerDatasDisponiveis(dias){
            var disableddates = ConsultaDiasDisponiveis();

            if(disableddates.length>0){
                //console.log("verdadeiro");
                $("#datepicker").datepicker("destroy");
                $("#datepicker").datepicker({
                    showOtherMonths: true,
                    selectOtherMonths: true,


                    beforeShowDay: DisableSpecificDates
                }); 
                $("#datepicker").datepicker("refresh");

            }
            else{

                $("#datepicker").datepicker("destroy");
                $("#datepicker").datepicker({
                    showOtherMonths: true,
                    selectOtherMonths: true,


                    beforeShowDay: DisableFeriados
                }); 
                $("#datepicker").datepicker("refresh");

            }



        }

        //funcao timepicker dasable date
        function TimePickerDatasDisponiveisatualizar(dias){
            var disableddates = ConsultaDiasDisponiveisatualizar();

            if(disableddates.length>0){
                //console.log("verdadeiro");
                $("#datepicker-atualiza").datepicker("destroy");
                $("#datepicker-atualiza").datepicker({
                    showOtherMonths: true,
                    selectOtherMonths: true,


                    beforeShowDay: DisableSpecificDates
                }); 
                $("#datepicker-atualiza").datepicker("refresh");

            }
            else{

                $("#datepicker-atualiza").datepicker("destroy");
                $("#datepicker-atualiza").datepicker({
                    showOtherMonths: true,
                    selectOtherMonths: true,


                    beforeShowDay: DisableFeriados
                }); 
                $("#datepicker-atualiza").datepicker("refresh");

            }



        }



        //disibalitar dias do timepicker
        function DisableSpecificDates(date) {
            var dias = JSON.parse(getCookie("dias"));
            var disableddates = dias;


            var m = date.getMonth();
            var d = date.getDate();
            var y = date.getFullYear();

            // First convert the date in to the mm-dd-yyyy format 
            // Take note that we will increment the month count by 1 
            var currentdate = (m + 1) + '-' + d + '-' + y ;


            // We will now check if the date belongs to disableddates array 
            for (var i = 0; i < disableddates.length; i++) {
                // Now check if the current date is in disabled dates array. 
                if ($.inArray(currentdate, disableddates) != -1 ) {
                    return [false];
                } 
                //desabilitando domingos
                var day = date.getDay();
                // If day == 1 then it is MOnday
                if (day == 0) {

                    return [false] ; 

                } else { 

                    return [true] ;
                }
            }
            var weekenddate = $.datepicker.noWeekends(date);
            return weekenddate; 

        }










        //Regras para COMBOXBOX

        //Seleciona Profisisonal
        $("#profissional").change(function () {
            //funcao de consulta dos dias disponiveis do profisisonal
            // TimePickerDatasDisponiveis();
            //alert("profisisonal");
            //profisisonal + data
            if( $('#datepicker').val() != null)
            {
                DatepickerHorasDisponiveis();
                //alert("profissional e data sem funfar ");
            }

        });



        //Se Mudar dia Muda Profisisonais Disponiveis
        $('#datepicker').change(function () {
            $data = $('#datepicker').val();
            $data = formataDataSQL($data);
            DatepickerProfossionaisDisponiveis($data);
            //  alert("dia funfando");
        });

        //se muda hora NOT WORK
        $('#timepicker').on('changeTime', function() {

            if( $('#datepicker').val() != null){
                // DiaeHoraTime(filial);
            }

        });



        /**regras combobox atualizar */

        //Seleciona Profisisonal
        $("#profissional-atualizar").change(function () {

            if($('#datepicker-atualiza').val() != null && $('#profissional-atualizar').val() != 'x' )
            {
                DatepickerHorasDisponiveisAtualizar();
                //alert("profissional e data sem funfar ");
            }

        });



        //Se Mudar dia Muda Profisisonais Disponiveis
        $('#datepicker-atualiza').change(function () {
            $data = $('#datepicker-atualiza').val();
            $data = formataDataSQL($data);
            DatepickerProfossionaisDisponiveisatualiza($data);
            //alert("dia funfando");
        });

        //se muda hora 
        $('#timepicker-atualiza').on('changeTime', function() {

            //dia + hora
            if($('#datepicker-atualiza').val() != null){
                DiaeHoraTimeatualiza(filial);
            }
        });




        //consulta horas disponiveis
        function DatepickerHorasDisponiveis(){

            var horas =  ConsultaHorasDisponiveis();
            // console.log(horas);
            //:[['6:00am', '11:30am'], ['5:00pm', '8:00pm']]
            $("#timepicker").timepicker('remove');
            $('#timepicker').timepicker({
                'interval': 40,
                'step': '40',
                'timeFormat': 'H:i',
                'minTime': '9:00am',
                'maxTime': '11:00pm',
                'scrollDefault': 'now',
                'disableTimeRanges':horas
            });

        }

        //consulta horas disponiveis
        function DatepickerHorasDisponiveisAtualizar(){

            var horas =  ConsultaHorasDisponiveisatualizar();
            // console.log(horas);
            //:[['6:00am', '11:30am'], ['5:00pm', '8:00pm']]
            $("#timepicker-atualiza").timepicker('remove');
            $('#timepicker-atualiza').timepicker({
                'interval': 40,
                'step': '40',
                'timeFormat': 'H:i',
                'minTime': '9:00am',
                'maxTime': '11:00pm',
                'scrollDefault': 'now',
                'disableTimeRanges':horas
            });

        }



        //consulta horas disponiveis
        function ConsultaHorasDisponiveis() {
            ////localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/horas-disponiveis&profissional=1&dia=2017/12/07
            var url = "//localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/horas-disponiveis";
            var profissional = $('#profissional').val();
            var dia = $('#datepicker').val();
            dia = formataDataSQL(dia);

            // console.log(dia);



            $.ajax({
                url: url,
                data: {profissional:profissional, dia:dia},
                type: 'GET',
                async: false
            }).success(function(data) {
                remote = data;
                //console.log(data);

                //se for false nao pode
            }).error(function() {
                alert('falha verificacao de horas disponiveis do profisisonal');
            });
            return remote;
        }

        //consulta horas disponiveis Atualizar
        function ConsultaHorasDisponiveisatualizar() {
            ////localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/horas-disponiveis&profissional=1&dia=2017/12/07
            var url = "//localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/horas-disponiveis-atualizar";
            var profissional = $('#profissional-atualizar').val();
            var dia = $('#datepicker-atualiza').val();
            var hora = $('#timepicker-atualiza').val();
            dia = formataDataSQL(dia);


            $.ajax({
                url: url,
                data: {profissional:profissional, dia:dia, hora:hora},
                type: 'GET',
                async: false
            }).success(function(data) {
                remote = data;

                //se for false nao pode
            }).error(function() {
                alert('falha verificacao de hroas disponiveis do profisisonal');
            });
            return remote;
        }



        //Seleciona Hora e Dia *** falara seleciona Dia e Hora NOT WORK
        function DiaeHoraTime(filial) {

            //console.log(filial);

            var profisisonais =ConsultaProfissionaisDisponiveis(filial);



            //getProfissionaisID(profisisonais);
        }

        //Seleciona Hora e Dia *** falara seleciona Dia e Hora NOT WORK
        function DiaeHoraTimeatualiza(filial) {

            var dia = $('#datepicker-atualiza').val();
            dia = formataDataSQL(dia);
            var profisisonais =ConsultaProfissionaisDisponiveisatualiza(ultimoAgendamento, dia,filial);
            // alert(profisisonais);
            //getProfissionaisIDAtualiza(profisisonais);
        }






        function ConsultaProfissionaisDisponiveis(filial) {


            ////localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/profissionais-disponiveis&dia=2017/12/07&hora=09:00
            var url = "//localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/profissionais-disponiveis";
            var dia= $('#datepicker').val();
            var hora= $('#timepicker').val();


            dia = formataDataSQL(dia);

            $.ajax({
                url: url,
                data: {hora:hora, dia:dia, filia:filial},
                type: 'GET',
                async: false
            }).success(function(data) {
                //console.log(data);
                $('#profissional').empty();
                $('#profissional').append(' <option value="x">Selecione um Profisisonal...</option>');

                for (var i = 0, len = data.result.length; i < len; i++) {
                    $('#profissional').append('<option value="' + data.result[i].ID + '" >' + data.result[i].NOME + '</option>');
                }
                $('#profissional option[value=0]').attr('selected', 'selected');

            }).error(function() {
                alert('falha verificacao de  profisisonal disponivel na data e hora');
            });

        }


        function ConsultaProfissionaisDisponiveisatualiza(ultimoagendamento, dia,filial) {




            ////localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/profissionais-disponiveis&dia=2017/12/07&hora=09:00
            var url = "//localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/atualiza-profissionais-disp"
            + "&dia="+ $('#datepicker-atualiza').val()
            + "&hora="+ $('#timepicker-atualiza').val()
            + "&filial="+ filial
            + "&ultimoagendamento="+ ultimoagendamento
            + "&profissional=" + $('#profissional-atualizar').val();
            //console.log(url);


            var jqxhr = $.get(url
                              , function (data) {
                // alert("success " + JSON.stringify(data));
                if (data.error === false) {
                    //percorre array de usuarios
                    $('#profissional-atualizar').empty();
                    $('#profissional-atualizar').append(' <option value="x">Selecione um Profisisonal...</option>');

                    for (var i = 0, len = data.result.length; i < len; i++) {
                        $('#profissional-atualizar').append('<option value="' + data.result[i].ID + '" >' + data.result[i].NOME + '</option>');
                    }
                    $('#profissional option[value=0]').attr('selected', 'selected');
                }
                else {
                    alert("Humm..... " + JSON.stringify(data));
                }

            })
            .done(function () {
                //                    alert("second success");
            })
            .fail(function () {
                alert("error ao recuperar profisisonais");
            })
            .always(function () {
                //                    alert("finished");
            });

            // Perform other work here ...

            // Set another completion function for the request above
            jqxhr.always(function () {
                //                alert("second finished");
            });

        }

    });


    //funcao q clica em dia e retorna profisisonais
    function getProfissionaisID(idprofisisonais) {
        var url = "//localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/profissionais&filial=" + filial;
        var jqxhr = $.get(url
                          , function (data) {
            // alert("success " + JSON.stringify(data));
            if (data.error === false) {
                //percorre array de usuarios
                $('#profissional').empty();
                $('#profissional').append(' <option value="x">Selecione um Profisisonal...</option>');



                for (var j = 0, len = idprofisisonais.result.length; j < len; j++) {

                    for (var i = 0, len = data.result.length; i < len; i++) {

                        if(idprofisisonais.result[j].ID == data.result[i].ID){
                            $('#profissional').append('<option value="' + data.result[i].ID + '" >' + data.result[i].Nome + '</option>');


                        }

                    }
                }

                $('#profissional option[value=0]').attr('selected', 'selected');


            }
            else {
                alert("Humm..... " + JSON.stringify(data));
            }

        })
        .done(function () {
            //                    alert("second success");
        })
        .fail(function () {
            alert("error ao recuperar profisisonais");
        })
        .always(function () {
            //                    alert("finished");
        });

        // Perform other work here ...

        // Set another completion function for the request above
        jqxhr.always(function () {
            //                alert("second finished");
        });

    }

    //funcao q clica em dia e retorna profisisonais
    function getProfissionaisIDAtualiza(idprofisisonais) {
        var url = "//localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/profissionais&filial=" + filial;
        var jqxhr = $.get(url
                          , function (data) {
            // alert("success " + JSON.stringify(data));
            if (data.error === false) {
                //percorre array de usuarios
                $('#profissional-atualizar').empty();

                //                $('#profissional-atualizar').append(' <option value="x">Selecione um Profisisonal...</option>');


                for (var j = 0, len = idprofisisonais.result.length; j < len; j++) {
                    for (var i = 0, len = data.result.length; i < len; i++) {


                        if(idprofisisonais.result[j].ID == data.result[i].ID){

                            $('#profissional-atualizar').append('<option value="' + data.result[i].ID + '" >' + data.result[i].Nome + '</option>');

                        }

                    }
                }

                //                $('#profissional-atualizar option[value=0]').attr('selected', 'selected');


            }
            else {
                alert("Humm..... " + JSON.stringify(data));
            }

        })
        .done(function () {
            //                    alert("second success");
        })
        .fail(function () {
            alert("error ao recuperar profisisonais");
        })
        .always(function () {
            //                    alert("finished");
        });

        // Perform other work here ...

        // Set another completion function for the request above
        jqxhr.always(function () {
            //                alert("second finished");
        });

    }


    function getprodutoporid(id,filial){  
        ////localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/produtos-id&id=1
        var url = "//localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/produtos-id"
        + "&id=" + id
        + "&filial=" + filial;


        //TODO: obter produtos

        var jqxhr = $.get(url
                          , function (data) {


            if(data.result.Servico != 1){






                if(parseInt(data.result.Estoque) <= parseInt(data.result.EstoqueMinimo)){

                    $('.estoqueminimo').css({ display: "block" });
                }

                if(parseInt(data.result.Estoque) < 1){

                    $('.semprodutoestoque').css({ display: "block" });
                }
                else{
                    addProduto($("#produtos").val(), $("#produtos option:selected").text(),data.result.Venda,data.result.Tempo, data.result.Servico);
                }
            }
            else{
                addProduto($("#produtos").val(), $("#produtos option:selected").text(),data.result.Venda,data.result.Tempo, data.result.Servico);
            }


        })
        .done(function () {
            //                    alert("second success");
        })
        .fail(function () {
            alert("error na adicao do produto");
        })
        .always(function () {
            //                    alert("finished");
        });

        // Perform other work here ...

        // Set another completion function for the request above
        jqxhr.always(function () {
            //                alert("second finished");
        });


    }



    function agendamentoGratuito(ultimoagendamento){  

        ////localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/produtos-id&id=1
        var url = "//localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/agendamento-gratuito"
        + "&id=" + ultimoagendamento;

        //console.log(url);

        //TODO: obter produtos

        var jqxhr = $.get(url
                          , function (data) {
            gratuito = data.result;
            //console.log(gratuito);


            if(gratuito === 1 ){

                finalizaAgendamento(ultimoagendamento);
                $("#agendamento-gratuito").dialog('open');

                //abre modal de agendamento gratuito
                //aparece outro dialog como Este servico foi gratuito pela sua fidelidade!!

            }

            else{

                // mostrar valor final no dialog-alert-pagamento
                //alert("Agendamento Pago");

                $("#total-produtos").val(gratuito);

                $("#total-produtex").html(gratuito);
                $("#total-produtex").val(gratuito);

                $("#dialog-alert-pagamento").dialog('open');

            }







        })
        .done(function () {

            //                    alert("second success");
        })
        .fail(function () {
            alert("error no agendamento gratuito");
        })
        .always(function () {

            //                    alert("finished");
        });

        // Perform other work here ...

        // Set another completion function for the request above
        jqxhr.always(function () {
            //                alert("second finished");

        });



    }




    $('#addProduto').click(function () {

        if (validaAddProduto()) {
            //TODO: obter de uma lista pelo index... pois precisa do TEMPO e VALOR tambem...
            //TODO: formatar valores de currency
            //recebe id do produto selecionado
            var id =$("#produtos option:selected").val();


            //getproduto por id e adc ele com a funcao add dentro do get

            getprodutoporid(id,filial);

        }
        else {
            $('.selecioneproduto').css({ display: "block" });
        }
    });




    $('#salvar-cliente').click(function () {
        //TODO: validacao basica dos campos antes de enviar
        if (validaAddCliente()) {
            gravaNovoCliente();

        }
        else {
            $('.informecliente').css({ display: "block" });
        }
    });


    //modal de editar ou cancelar um agendamento
    $("#dialog-alert-agendamento").dialog({
        autoOpen: false,
        hide: "puff",
        show: "slide",
        width: 400,
        height: 300
    });


    //modal de esconder agendamento editar
    $("#dialog-agendamento-atualizar").dialog({
        autoOpen: false,
        hide: "puff",

    });



    //modal de editar ou cancelar um agendamento
    $("#dialog-alert-cancelamento").dialog({
        autoOpen: false,
        hide: "puff",
        show: "slide",
        width: 400,
        height: 300
    });

    //modal de editar ou cancelar um agendamento
    $("#dialog-cancelamento-justificativa").dialog({
        autoOpen: false,
        hide: "puff",
        show: "slide",
        width: 400,
        height: 300
    });




    $("#agendamento-gratuito").dialog({
        autoOpen: false,
        hide: "puff",
        show: "slide",
        width: 400,
        height: 300
    });

    //modal de editar ou cancelar um agendamento
    $("#dialog-alert-finalizar").dialog({
        autoOpen: false,
        hide: "puff",
        show: "slide",
        width: 400,
        height: 300
    });


    //modal de editar ou cancelar um agendamento
    $("#dialog-alert-pagamento").dialog({
        autoOpen: false,
        hide: "puff",
        show: "slide",
        width: 400,
        height: 300
    });



    $("#editar").click(function () {
        $("#dialog-alert-agendamento").dialog('close');

        //limpa os campos!
        $("#combobox-cliente-atualizar tbody").empty();
        $("#table-produtos-atualizar tbody").empty();

        //carrega dados do agendamento
        RetornaAgendamento();


        dialog = $("#dialog-agendamento-atualizar").dialog({
            zIndex: 100,
            autoOpen: false,
            dialogClass: 'mydialog',
            // appendTo: $("body"),
            height: screen.height - (screen.height * 0.4),
            width: screen.width - (screen.width * 0.3),
            modal: true,
            buttons: {
                "Agendar": function () {
                    if (validaAddAgendamentoAtualizar()) {

                        var filial =JSON.parse(getCookie("filial"));

                        var horafim = calculaprodutosatualizar();
                        var regras =regrasHorarioatualizar(horafim, ultimoAgendamento, filial);

                        var dia = formataData($('#datepicker-atualiza').val());

                        var diahoje = getDefaultData();


                        if (diahoje <= dia){

                            if (regras ==true) {
                                //alert("pode cadastrar se for true... regras ="+regras);

                                var servico = verificaservicoatualizar();

                                if (servico ==1) {
                                    var almoco = verificaalmocoatualizar(horafim);

                                    if (almoco ==true) {
                                        var folga = verificafolgaatualizar();

                                        if (folga ==true) {
                                            //alert("pode cadastrar se for 1... servico ="+servico);
                                            AtualizaAgendamento(ultimoAgendamento);

                                        }
                                        else{

                                            alert("Profisisonal esta de folga'");
                                        }

                                    }
                                    else{

                                        alert("Profisisonal em Horario de almoco");
                                    }


                                }
                                else{

                                    $('.servico').css({ display: "block" });
                                }

                            }

                            else{

                                alert("Este Profissional com este Servico Nessa Hora está indisponivel!");
                            }

                        }

                        else{

                            alert("impossivel agendar em uma data anterior do dia de HOJE")
                        }

                    }
                    else {
                        $('.campoagendamento').css({ display: "block" });
                    }
                },
                Cancel: function () {
                    dialog.dialog("close");
                }
            },
            close: function () {
                form[0].reset();
                allFields.removeClass("ui-state-error");
            }
        });



        $("#dialog-agendamento-atualizar").dialog('open');


    });


    $("#finalizar").click(function () {

        $("#dialog-alert-agendamento").dialog('close');
        $("#dialog-alert-finalizar").dialog('open');


    });

    $("#cancelar").click(function () {
        $("#dialog-alert-agendamento").dialog('close');
        $("#dialog-alert-cancelamento").dialog('open');

    });

    $("#nada").click(function () {

        $("#dialog-alert-agendamento").dialog('close');

    });




    $("#sim-finalizar").click(function () {

        $("#dialog-alert-finalizar").dialog('close');

        //MAKER//funcao pega total do agendamento!!


        agendamentoGratuito(ultimoAgendamento); 




    });

    $("#pagamento-sem").click(function () {

        $("#agendamento-gratuito").dialog('close');

    });




    $("#pagamento-Dinheiro").click(function () {

        $("#dialog-alert-pagamento").dialog('close');
        finalizaAgendamento(ultimoAgendamento);
        //atualiza agendamento com pagamento no dinheiro
        var pagamento = "Dinheiro";
        finalizaPagamento(pagamento, ultimoAgendamento);
    });




    $("#pagamento-Debito").click(function () {
        $("#dialog-alert-pagamento").dialog('close');
        finalizaAgendamento(ultimoAgendamento);
        //atualiza agendamento com pagamento no cartao
        var pagamento = "Cartão Débito";
        finalizaPagamento(pagamento, ultimoAgendamento);

    });

    $("#pagamento-Credito").click(function () {
        $("#dialog-alert-pagamento").dialog('close');
        finalizaAgendamento(ultimoAgendamento);
        //atualiza agendamento com pagamento no cartao
        var pagamento = "Cartão Crédito";
        finalizaPagamento(pagamento, ultimoAgendamento);

    });


    $("#nao-finalizar").click(function () {

        $("#dialog-alert-finalizar").dialog('close');

    });

    $("#nao-justificativa").click(function () {

        $("#dialog-cancelamento-juntificativa").dialog('close');

    });


    $("#sim-justificativa").click(function () {

        var justificativa = $("#justificativa").val();


        if(justificativa){
            $("#dialog-cancelamento-justificativa").dialog('close');
            cancelAgendamento(ultimoAgendamento, justificativa);    
        }
        else{

            alert("Preencha a justificativa!");
        }

    });




    $("#sim-cancelar").click(function () {

        $("#dialog-alert-cancelamento").dialog('close');
        $("#dialog-cancelamento-justificativa").dialog('open');







    });

    $("#nao-cancelar").click(function () {

        $("#dialog-alert-cancelamento").dialog('close');

    });

    $("#nao-cancelar").click(function () {

        $("#dialog-alert-cancelamento").dialog('close');

    });




})
;