$(document).ready(function () {
    console.log('Ready disparado');

    var usuario = JSON.parse(getCookie("user"));
    var filial = JSON.parse(getCookie("filial"));




    function printDiv() {
        print()
    }


    function validaRel() {
        var inicio = $('#inicio').val();
        var fim = $('#fim').val();
        // var status = $('#status').val();
        var status = $('#combobox-status').val();

        if (notNull(inicio) && notNull(fim)) {
            return true;
        }
        else {
            return false;
        }
    }

    function notNull(a) {
        if (a != "" && !(a.match(/^\s+$/))) {
            return true;
        } else {
            return false;
        }
    }


    function getStatus() {

        var url = "//localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/status";


        var jqxhr = $.get(url
                          , function (data) {

            if (data.error === false) {

                //limpa o combo de clientes
                $('#combobox-status').empty();

                //Add a empty option
                $('#combobox-status').append(' <option value="0">Qualquer Status</option>');

                //percorre array de usuarios
                for (var i = 0, len = data.result.length; i < len; i++) {
                    $('#combobox-status').append('<option value="' + data.result[i].ID + '" selected="selected">' + data.result[i].Nome + '</option>');

                }

                // $('#combobox-status option[value=0]').attr('selected', 'selected');
                $('#combobox-status').val('0');

            }
            else {
                alert("Humm..... " + JSON.stringify(data));
            }


        })

        .done(function () {
            //                    alert("second success");
        })
        .fail(function () {
            alert("error getting agendamentos...");
        })
        .always(function () {
            //                    alert("finished");
        });

        // Perform other work here ...

        // Set another completion function for the request above
        jqxhr.always(function () {
            //                alert("second finished");
        });
    }

    function getProfissionais() {


        var url = "//localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/profissionais&filial=" + filial;
        //  console.log(url);

        var jqxhr = $.get(url
                          , function (data) {

            if (data.error === false) {

                //limpa o combo de profissionais
                $('#combobox-profissionais').empty();

                //Add a empty option
                $('#combobox-profissionais').append(' <option value="0">Todos os profissionais...</option>');

                //percorre array de usuarios
                for (var i = 0, len = data.result.length; i < len; i++) {
                    $('#combobox-profissionais').append('<option value="' + data.result[i].ID + '" selected="selected">' + data.result[i].Nome + '</option>');
                }

                // $('#combobox-status option[value=0]').attr('selected', 'selected');
                $('#combobox-profissionais').val('0');

            }
            else {
                alert("Humm..... " + JSON.stringify(data));
            }


        })

        .done(function () {
            //                    alert("second success");
        })
        .fail(function () {
            alert("error getting profissionais...");
        })
        .always(function () {
            //                    alert("finished");
        });

        // Perform other work here ...

        // Set another completion function for the request above
        jqxhr.always(function () {
            //                alert("second finished");
        });
    }


    getProfissionais();
    getStatus();


    function getRel() {

        var inicio = $('#inicio').val();
        var fim = $('#fim').val();
        var status = $('#combobox-status').val();

        // var url = "//localhost:8888/divinoAPI/web/index.php?r=divino/relatorio-agendamento&inicio=2017-11-08&fim=2017-11-14&status=1";
        var url = "//localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/relatorio-agendamento"
        + "&filial=" + filial
        + "&inicio=" + inicio
        + "&fim=" + fim
        + "&status=" + status
        ;


        var jqxhr = $.get(url
                          , function (data) {
            $("#resultado").hide().empty().append(data).fadeIn("slow");
        })
        .done(function () {
            //                    alert("second success");
        })
        .fail(function (pq) {

            //nao sei pq mas ao subir no servidores ele caiu aqui.. entao estou programando
            if (pq.readyState === 4) {
                $("#resultado").hide().empty().append(pq.responseText).fadeIn("slow");
            }
            else
                alert("Erro ao buscar relatorio de Agendamentos...." + JSON.stringify(pq));
        })
        .always(function () {
            //                    alert("finished");
        });

        // Perform other work here ...

        // Set another completion function for the request above
        jqxhr.always(function () {
            //                alert("second finished");
        });
    }


    function getAgendamentoGratuito() {

        var inicio = $('#inicio').val();
        var fim = $('#fim').val();
        var status = $('#combobox-status').val();

        // var url = "//localhost:8888/divinoAPI/web/index.php?r=divino/relatorio-agendamento&inicio=2017-11-08&fim=2017-11-14&status=1";
        var url = "//localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/relatorio-gratuito-agendamento"
        + "&filial=" + filial
        + "&inicio=" + inicio
        + "&fim=" + fim
        + "&status=" + status
        ;

        console.log(url);


        var jqxhr = $.get(url
                          , function (data) {
            $("#resultado").hide().empty().append(data).fadeIn("slow");
        })
        .done(function () {
            //                    alert("second success");
        })
        .fail(function (pq) {

            //nao sei pq mas ao subir no servidores ele caiu aqui.. entao estou programando
            if (pq.readyState === 4) {
                $("#resultado").hide().empty().append(pq.responseText).fadeIn("slow");
            }
            else
                alert("Erro ao buscar relatorio de Agendamentos...." + JSON.stringify(pq));
        })
        .always(function () {
            //                    alert("finished");
        });

        // Perform other work here ...

        // Set another completion function for the request above
        jqxhr.always(function () {
            //                alert("second finished");
        });
    }

    function getRelBalcao() {

        var inicio = $('#inicio').val();
        var fim = $('#fim').val();
        var status = $('#combobox-status').val();

        // var url = "//localhost:8888/divinoAPI/web/index.php?r=divino/relatorio-agendamento&inicio=2017-11-08&fim=2017-11-14&status=1";
        var url = "//localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/relatorio-balcao"
        + "&filial=" + filial
        + "&inicio=" + inicio
        + "&fim=" + fim
        + "&status=" + status
        ;
        console.log(url);

        var jqxhr = $.get(url
                          , function (data) {
            $("#resultado").hide().empty().append(data).fadeIn("slow");
        })
        .done(function () {
            //                    alert("second success");
        })
        .fail(function (pq) {

            //nao sei pq mas ao subir no servidores ele caiu aqui.. entao estou programando
            if (pq.readyState === 4) {
                $("#resultado").hide().empty().append(pq.responseText).fadeIn("slow");
            }
            else
                alert("Erro ao buscar relatorio de Agendamentos...." + JSON.stringify(pq));
        })
        .always(function () {
            //                    alert("finished");
        });

        // Perform other work here ...

        // Set another completion function for the request above
        jqxhr.always(function () {
            //                alert("second finished");
        });
    }


    function getRelTotall() {


        var inicio = $('#inicio').val();
        var fim = $('#fim').val();
        var status = $('#combobox-status').val();

        // var url = "//localhost:8888/divinoAPI/web/index.php?r=divino/relatorio-agendamento&inicio=2017-11-08&fim=2017-11-14&status=1";
        var url = "//localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/relatorio-geral"
        + "&filial=" + filial
        + "&inicio=" + inicio
        + "&fim=" + fim
        + "&status=" + status
        ;

        console.log(url);

        var jqxhr = $.get(url
                          , function (data) {
            $("#resultado").hide().empty().append(data).fadeIn("slow");
        })
        .done(function () {
            //                    alert("second success");
        })
        .fail(function (pq) {

            //nao sei pq mas ao subir no servidores ele caiu aqui.. entao estou programando
            if (pq.readyState === 4) {
                $("#resultado").hide().empty().append(pq.responseText).fadeIn("slow");
            }
            else
                alert("Erro ao buscar relatorio de Agendamentos...." + JSON.stringify(pq));
        })
        .always(function () {
            //                    alert("finished");
        });

        // Perform other work here ...

        // Set another completion function for the request above
        jqxhr.always(function () {
            //                alert("second finished");
        });
    }






    function getProdutosCortesia() {


        var inicio = $('#inicio').val();
        var fim = $('#fim').val();
        var status = $('#combobox-status').val();

        // var url = "//localhost:8888/divinoAPI/web/index.php?r=divino/relatorio-agendamento&inicio=2017-11-08&fim=2017-11-14&status=1";
        var url = "//localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/produto-cortesia"
        + "&filial=" + filial
        + "&inicio=" + inicio
        + "&fim=" + fim
        + "&status=" + status
        ;




        console.log(url);


        var jqxhr = $.get(url
                          , function (data) {
            $("#resultado").hide().empty().append(data).fadeIn("slow");
        })
        .done(function () {
            //                    alert("second success");
        })
        .fail(function (pq) {

            //nao sei pq mas ao subir no servidores ele caiu aqui.. entao estou programando
            if (pq.readyState === 4) {
                $("#resultado").hide().empty().append(pq.responseText).fadeIn("slow");
            }
            else
                alert("Erro ao buscar relatorio de Clientes..." + JSON.stringify(pq));
        })
        .always(function () {
            //                    alert("finished");
        });

        // Perform other work here ...

        // Set another completion function for the request above
        jqxhr.always(function () {
            //                alert("second finished");
        });
    }


    function getEstoque() {


        // var url = "//localhost:8888/divinoAPI/web/index.php?r=divino/relatorio-agendamento&inicio=2017-11-08&fim=2017-11-14&status=1";
        var url = "//localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/estoque"
        + "&filial=" + filial
        + "&inicio=" +  $('#inicio').val()
        + "&fim=" + $('#fim').val()
        + "&status=" +  $('#combobox-status').val()
        ;




        console.log(url);


        var jqxhr = $.get(url
                          , function (data) {
            $("#resultado").hide().empty().append(data).fadeIn("slow");
        })
        .done(function () {
            //                    alert("second success");
        })
        .fail(function (pq) {

            //nao sei pq mas ao subir no servidores ele caiu aqui.. entao estou programando
            if (pq.readyState === 4) {
                $("#resultado").hide().empty().append(pq.responseText).fadeIn("slow");
            }
            else
                alert("Erro ao buscar relatorio de Clientes..." + JSON.stringify(pq));
        })
        .always(function () {
            //                    alert("finished");
        });

        // Perform other work here ...

        // Set another completion function for the request above
        jqxhr.always(function () {
            //                alert("second finished");
        });
    }

    function getRelCliente() {


        // var url = "//localhost:8888/divinoAPI/web/index.php?r=divino/relatorio-cliente&filial=1";
        var url = "//localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/relatorio-cliente"
          + "&filial=" + filial
        + "&inicio=" +  $('#inicio').val()
        + "&fim=" + $('#fim').val()
        + "&status=" +  $('#combobox-status').val()
        ;


        //console.log(url);


        var jqxhr = $.get(url
                          , function (data) {
            $("#resultado").hide().empty().append(data).fadeIn("slow");
        })
        .done(function () {
            //                    alert("second success");
        })
        .fail(function (pq) {

            //nao sei pq mas ao subir no servidores ele caiu aqui.. entao estou programando
            if (pq.readyState === 4) {
                $("#resultado").hide().empty().append(pq.responseText).fadeIn("slow");
            }
            else
                alert("Erro ao buscar relatorio de Clientes..." + JSON.stringify(pq));
        })
        .always(function () {
            //                    alert("finished");
        });

        // Perform other work here ...

        // Set another completion function for the request above
        jqxhr.always(function () {
            //                alert("second finished");
        });
    }

    function getRelProfissional() {

        var inicio = $('#inicio').val();
        var fim = $('#fim').val();
        var status = $('#combobox-status').val();



        var url;

        //todos os profissionaisl
        if ($('#combobox-profissionais').val() ==="0" && $('#combobox-status').val() === "0")   {
            url = "//localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/relatorio-profissional"

                + "&filial=" + filial
                + "&inicio=" + inicio
                + "&fim=" + fim
                + "&status=" + status
            ;

        }


        else  {
            url = "//localhost:8888/FTP/agendamentoAPI/web/index.php?r=divino/relatorio-profissional"
               
                + "&profissional=" + $('#combobox-profissionais').val()
                            + "&filial=" + filial
                + "&inicio=" + inicio
                + "&fim=" + fim
                + "&status=" + status
        }

        console.log(url);

        var jqxhr = $.get(url
                          , function (data) {
            $("#resultado").hide().empty().append(data).fadeIn("slow");
        })
        .done(function () {
            //                    alert("second success");
        })
        .fail(function (pq) {

            //nao sei pq mas ao subir no servidores ele caiu aqui.. entao estou programando
            if (pq.readyState === 4) {
                $("#resultado").hide().empty().append(pq.responseText).fadeIn("slow");
            }
            else
                alert("Erro ao buscar relatorio de Profissionais..." + JSON.stringify(pq));
        })
        .always(function () {
            //                    alert("finished");
        });

        // Perform other work here ...

        // Set another completion function for the request above
        jqxhr.always(function () {
            //                alert("second finished");
        });
    }


    $("#agendamentos").click(function () {
        if (validaRel())
            getRel();
        else {
            alert('Informe as datas para  prosseguir...');
        }
    });



    $("#estoque").click(function () {
        if (validaRel())
            getEstoque();
        else {
            alert('Informe as datas para  prosseguir...');
        }
    });



    $("#agendamentogratuito").click(function () {
        if (validaRel())
            getAgendamentoGratuito();
        else {
            alert('Informe as datas para  prosseguir...');
        }
    });



    $("#balcao").click(function () {
        if (validaRel())
            getRelBalcao();
        else {
            alert('Informe as datas para  prosseguir...');
        }
    });

    $("#clientes").click(function () {
        getRelCliente();
    });

    $("#produtocortesia").click(function () {
        getProdutosCortesia();
    });



    $("#profissionais").click(function () {
        getRelProfissional();
    });

    $("#total").click(function () {
        getRelTotall();
    });

    $("#imprimir").click(function () {

       
            printDiv();
    


    });

    $("#inicio").datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        dateFormat: 'yy-mm-dd'
    });

    $("#fim").datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        dateFormat: 'yy-mm-dd'
    });


    function getStartAndEndOfWeek(date) {

        //Calculating the starting point

        var curr = date ? new Date(date) : new Date();
        var first = curr.getDate() - dayOfWeek(curr);

        var firstday, lastday;

        if (first < 1) {
            //Get prev month and year
            var k = new Date(curr.valueOf());
            k.setDate(1);
            k.setMonth(k.getMonth() - 1);
            var prevMonthDays = new Date(k.getFullYear(), (k.getMonth() + 1), 0).getDate();

            first = prevMonthDays - (dayOfWeek(curr) - curr.getDate());
            firstday = new Date(k.setDate(first));
            lastday = new Date(k.setDate(first + 6));
        } else {
            // First day is the day of the month - the day of the week
            firstday = new Date(curr.setDate(first));
            lastday = new Date(curr.setDate(first + 6));
        }

        return [firstday, lastday];
    }

    function dayOfWeek(date, firstDay) {
        var daysOfWeek = {
            sunday: 0,
            monday: 1,
            tuesday: 2,
            wednesday: 3,
            thursday: 4,
            friday: 5,
            saturday: 6,
        };
        firstDay = firstDay || "monday";
        var day = date.getDay() - daysOfWeek[firstDay];
        return (day < 0 ? day + 7 : day);
    }


    var curr = new Date();

    var lista = getStartAndEndOfWeek(curr);

    $("#inicio").val(lista[0].toISOString().split('T')[0]);

    $("#fim").val(lista[1].toISOString().split('T')[0]);
});