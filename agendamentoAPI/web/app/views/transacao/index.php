<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TransacaoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Transacaos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transacao-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Transacao', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            'UsuarioOrigemID',
            'UsuarioDestinoID',
            'TimeStamp',
            'Status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
