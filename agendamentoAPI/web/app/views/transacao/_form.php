<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Transacao */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="transacao-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'UsuarioOrigemID')->textInput() ?>

    <?= $form->field($model, 'UsuarioDestinoID')->textInput() ?>

    <?= $form->field($model, 'TimeStamp')->textInput() ?>

    <?= $form->field($model, 'Status')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
