<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UsuarioCota */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="usuario-cota-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'CotaID')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'UsuarioID')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'UsuarioIndicacaoID')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'TimeStamp')->textInput() ?>

    <?= $form->field($model, 'Status')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
