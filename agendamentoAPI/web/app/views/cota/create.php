<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Cota */

$this->title = 'Create Cota';
$this->params['breadcrumbs'][] = ['label' => 'Cotas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cota-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
