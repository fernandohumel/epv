
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title>Winco</title>

    <!-- Favicon -->
    <link rel="icon" href="img/core-img/favicon.ico">

    <!-- Core Stylesheet -->
    <link href="style.css" rel="stylesheet">

    <!-- Responsive CSS -->
    <link href="css/responsive.css" rel="stylesheet">

</head>
<body>
<!-- Preloader Start -->
<div id="preloader">
    <div class="colorlib-load"></div>
</div>
<!-- ***** Header Area Start ***** -->
<header class="header_area animated">
    <div class="container">
        <div class="row align-items-center">
            <!-- Menu Area Start -->
            <div class="col-12 col-lg-10">
                <div class="menu_area">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <!-- Logo -->
                        <a class="navbar-brand" href="#"><img src="img/scr-img/winco-logo.png"></a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ca-navbar"
                                aria-controls="ca-navbar" aria-expanded="false" aria-label="Toggle navigation"><span
                                    class="navbar-toggler-icon"></span></button>
                        <!-- Menu Area -->
                        <div class="collapse navbar-collapse" id="ca-navbar">
                            <ul class="navbar-nav ml-5" id="nav">
                                <li class="nav-item active"><a class="nav-link" href="#home"><?php echo txt1 ?></a></li>
                                <li class="nav-item"><a class="nav-link" href="#expand"><?php echo txt2 ?></a></li>
                                <li class="nav-item"><a class="nav-link" href="#opay"><?php echo txt3 ?></a></li>
                                <li class="nav-item"><a class="nav-link" href="#mercado"><?php echo txt4 ?></a></li>
                                <li class="nav-item"><a class="nav-link" href="#exchanges"><?php echo txt5 ?></a></li>
                                <li class="nav-item"><a class="nav-link" href="#blog"><?php echo txt6 ?></a></li>
                                <li class="nav-item"><a class="nav-link" href="#contact"><?php echo txt7 ?></a></li>
                            </ul>
                            <div class="sing-up-button d-lg-none">
                                <a href="#"><?php echo txt8 ?></a>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
            <!-- Signup btn -->
            <div class="col-12 col-lg-2">
                <div class="sing-up-button d-none d-lg-block">
                    <a href="#"><?php echo txt9 ?></a>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- ***** Header Area End ***** -->

<!-- ***** Wellcome Area Start ***** -->
<video autoplay muted loop id="homeVideo">
    <source src="img/bg-img/bg-home.mp4" type="video/mp4">
</video>
<section class="wellcome_area clearfix" id="home">
    <div class="container h-100">
        <div class="row h-100 align-items-center pb-5 pt-5 pt-md-5">
            <div class="col-lg-6 col-12 pb-5 mt-5 mt-lg-1">
                <div class="wellcome-heading">
                    <h4><?php echo txt10 ?></h4>
                    <h2><?php echo txt11 ?></h2>
                    <h3><?php echo txt12 ?></h3>
                    <p><?php echo txt13 ?></p>
                </div>
                <div class="get-start-area">
                    <!-- Form Start -->
                    <a class="btn btn-block btn-outline-primary text-white btn-rouded btn-lg"><?php echo txt14 ?></a>
                    <!-- Form End -->
                </div>
            </div>
            <div class="col-lg-6 col-12">
                <div class="row">
                    <div class="col-12 col-lg-9">
                        <div class="wow fadeInUp" data-wow-delay="0.5s">
                            <img class="cel-camada-2 wow fadeInUp" src="img/scr-img/camada-2.png" data-wow-delay="1.5s">
                            <img class="cel-camada-1 wow fadeInUp" src="img/scr-img/camada-1.png" data-wow-delay="2s">
                            <img class="img-bg-cel" src="img/scr-img/cel-opay.png" alt="">
                        </div>
                    </div>
                    <div class="col-3 d-none d-lg-block">
                        <div class="welcome-thumb wow fadeInDown" data-wow-delay="0.8s">
                            <img class="ml-5 info-img-home" src="img/scr-img/02.png" alt="">
                            <div class="infoHome text-center" style="margin-left: 80px">
                                <span class="text-white ">5</span>
                                <p class="text-white "><?php echo txt15 ?></p>
                            </div>
                        </div>
                        <div class="welcome-thumb wow fadeInDown mt-100" data-wow-delay="1s">
                            <img class="info-img-home" src="img/scr-img/02.png" alt="">
                            <div class="infoHome text-center">
                                <span class="text-white">+400M</span>
                                <p class="text-white "><?php echo txt16 ?></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3 offset-6 d-none d-lg-block">
                        <div class="welcome-thumb wow fadeInDown mt-5" data-wow-delay="1.3s">
                            <img class="info-img-home" src="img/scr-img/02.png" alt="">
                            <div class="infoHome text-center pt-2">
                                <span class="text-white">$ 0.03</span>
                                <p class="text-white "><?php echo txt17 ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ***** Wellcome Area End ***** -->
<section class="section-pre-body" id="#homePos">
    <div class="container">
        <div class="row" style="height: 300px;">
            <div class="col-12 col-md-3">
                <div class="p-2 p-sm-3">
                    <div class="circle-pre-body">
                        <img class="img-fluid mx-auto d-block align-middle pt-5"
                             src="img/scr-img/tools-and-utensils.png">
                    </div>
                    <h2 class="text-center"><?php echo txt18 ?></h2>
                    <p class="text-center"><?php echo txt19 ?></p>
                </div>
            </div>
            <div class="col-12 col-md-3">
                <div class="mt-50 p-2 p-sm-3">
                    <div class="circle-pre-body cpb-2 pt-3">
                        <img class="img-fluid mx-auto d-block align-middle pt-5"
                             src="img/scr-img/database-with-internet-conection.png">
                    </div>
                    <h2 class="text-center"><?php echo txt20 ?></h2>
                    <p class="text-center"><?php echo txt21 ?></p>
                </div>
            </div>
            <div class="col-12 col-md-3">
                <div class="mt-50 p-2 p-sm-3">
                    <div class="circle-pre-body cpb-2 pt-3">
                        <img class="img-fluid mx-auto d-block align-middle pt-5" src="img/scr-img/controls.png">
                    </div>
                    <h2 class="text-center"><?php echo txt22 ?></h2>
                    <p class="text-center"><?php echo txt23 ?></p>
                </div>
            </div>
            <div class="col-12 col-md-3">
                <div class="p-2 p-sm-3">
                    <div class="circle-pre-body">
                        <img class="img-fluid mx-auto d-block align-middle pt-5" src="img/scr-img/shield.png">
                    </div>
                    <h2 class="text-center"><?php echo txt24 ?></h2>
                    <p class="text-center"><?php echo txt25 ?></p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ***** Special Area Start ***** -->
<section class="special-area bg-white section_padding_150" id="expand" style="padding-bottom:100px">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <!-- Section Heading Area -->
                <div class="section-heading text-center">
                    <h2><?php echo txt26 ?></h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-5 mt-2">
                <h3 class="text-center text-md-right azul">
                    <?php echo txt27 ?></h3>
            </div>
            <div class="col-12 col-md-2">
                <img class="img-fluid mx-auto d-block" src="img/scr-img/divisor.png">
            </div>
            <div class="col-12 col-md-5 mt-2">
                <h3 class="roxo text-center text-md-left"><?php echo txt28 ?></h3>
            </div>
        </div>
    </div>
    <!-- Special Description Area -->
</section>
<!-- ***** Special Area End ***** -->

<!-- ***** Awesome Features Start ***** -->
<section class="awesome-feature-area bg-white pt-5 clearfix" id="opay">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <!-- Heading Text -->
                <div class="section-heading text-center">
                    <h2><?php echo txt29 ?></h2>
                    <!--<div class="line-shape"></div>-->
                    <h4 class="text-center azul mt-3">
                        <?php echo txt30 ?></h4>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-lg-6">
                <img class="img-fluid mx-auto d-block align-bottom" src="img/scr-img/mobile-hand.jpg">
            </div>
            <div class="col-12 col-lg-6 pb-5 pt-3">
                <div class="row">
                    <div class="col-12 col-sm-4">
                        <img class="img-fluid mx-auto d-block align-bottom" src="img/scr-img/foguete.png">
                    </div>
                    <div class="col-2">
                        <span class="numbers">1</span>
                    </div>
                    <div class="col-10 col-sm-6">
                        <h2 class="tit-opay"><?php echo txt65 ?></h2>
                        <p><?php echo txt31 ?></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-sm-4">
                        <img class="img-fluid mx-auto d-block align-bottom" src="img/scr-img/graficos.png">

                    </div>
                    <div class="col-2">
                        <span class="numbers">2</span>
                    </div>
                    <div class="col-10 col-sm-6">
                        <h2 class="tit-opay"><?php echo txt32 ?></h2>
                        <p><?php echo txt33 ?></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-sm-4">
                        <img class="img-fluid mx-auto d-block align-bottom" src="img/scr-img/phone-btn.png">

                    </div>
                    <div class="col-2">
                        <span class="numbers">3</span>
                    </div>
                    <div class="col-10 col-sm-6">
                        <h2 class="tit-opay"><?php echo txt34 ?></h2>
                        <p><?php echo txt35 ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="market_area clearfix">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="text-center text-white"><?php echo txt36 ?></h2>
            </div>
        </div>
        <div class="row mt-5 align-items-center ">
            <div class="col-12 col-lg-6">
            </div>
            <div class="col-1">
                <img class="img-fluid mx-auto d-block align-bottom" src="img/scr-img/box.png">
            </div>
            <div class="col-11 col-lg-5">
                <p class="text-white market_text bg-dark-op-5 p-5"><?php echo txt37 ?></p>
            </div>
        </div>
    </div>
</section>
<!-- ***** Cool Facts Area End ***** -->

<!-- ***** App Screenshots Area Start ***** -->
<section class="app-screenshots-area bg-white clearfix" id="crescimento">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-sm-6">
                <div class="section-heading text-center mt-5">
                    <img src="img/scr-img/icon.png" class="pb-4">
                    <h2>
                        <small><?php echo txt38 ?><BR><?php echo txt39 ?></small>
                    </h2>
                    <div class="pt-3 col-12 col-sm-8 m-auto">
                        <p class=""><?php echo txt40 ?></p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 accept-winco d-inline-block">

            </div>
        </div>
    </div>
</section>
<video autoplay muted loop id="homeVideo2">
    <source src="img/bg-img/winnlife.mp4" type="video/mp4">
    <source src="img/bg-img/winnlife.webm" type="video/webm">
</video>
<section class="app-screenshots-area clearfix" id="mercado">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-sm-6 d-inline-block">

            </div>
            <div class="col-12 col-sm-6 bg-dark-op-8">
                <div class=" text-center mt-5 mb-5">
                    <img src="img/scr-img/building-winco.png" class="pb-3 " style="max-width: 130px">
                    <h2 class="text-white">
                        <small><?php echo txt41 ?><br><?php echo txt42 ?></small>
                    </h2>
                    <div class="pt-3 col-12 col-sm-8 m-auto">
                        <p class="text-white">
                           <?php echo txt43 ?></p>
                        <a class="table-shadow text-white bg-gradient btn btn-outline-dark mt-3 btn-rouded" href="#"><?php echo txt44 ?><i class="fa fa-angle-double-right pl-1 pr-1" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section class="pricing-plane-area pt-5 clearfix" id="exchanges">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <!-- Heading Text  -->
                <div class="section-heading text-center">
                    <h2><?php echo txt45 ?></h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-2 offset-md-1">
                <div class="single-special text-center wow fadeInUp" data-wow-delay="0.2s">
                    <div class="single-icon">
                        <img class="logo-exchanges img-fluid m-auto" src="img/scr-img/southxchange_268x168.png">
                    </div>
                    <p><?php echo txt46 ?></p>
                    <a class="table-shadow text-white bg-gradient btn btn-block btn-outline-dark mt-3 btn-rouded"
                       href="#"><?php echo txt47 ?><i class="fa fa-exchange pl-1 pr-1" aria-hidden="true"></i></a>
                </div>
            </div>
            <div class="col-12 col-md-2">
                <div class="single-special text-center wow fadeInUp" data-wow-delay="0.2s">
                    <div class="single-icon">
                        <img class="logo-exchanges img-fluid m-auto pl-2 pr-2" src="img/scr-img/14bit-logo.png">
                    </div>
                    <p>WCO/BTC</p>
                    <a class="table-shadow text-white bg-gradient btn btn-block btn-outline-dark mt-3 btn-rouded"
                       href="#"><?php echo txt48 ?><i class="fa fa-exchange pl-1 pr-1" aria-hidden="true"></i></a>
                </div>
            </div>
            <div class="col-12 col-md-2">
                <div class="single-special text-center wow fadeInUp" data-wow-delay="0.2s">
                    <div class="single-icon">
                        <img class="logo-exchanges img-fluid m-auto" src="img/scr-img/logo.png">
                    </div>
                    <p><?php echo txt49 ?></p>
                    <a class="table-shadow text-white bg-gradient btn btn-block btn-outline-dark mt-3 btn-rouded"
                       href="#"><?php echo txt50 ?><i class="fa fa-exchange pl-1 pr-1" aria-hidden="true"></i></a>
                </div>
            </div>
            <div class="col-12 col-md-2">
                <div class="single-special text-center wow fadeInUp" data-wow-delay="0.2s">
                    <div class="single-icon">
                        <img class="logo-exchanges img-fluid m-auto" src="img/scr-img/xbit-logo.png">
                    </div>
                    <p><?php echo txt51 ?></p>
                    <a class="table-shadow text-white bg-gradient btn btn-block btn-outline-dark mt-3 btn-rouded"
                       href="#"><?php echo txt52 ?><i class="fa fa-exchange pl-1 pr-1" aria-hidden="true"></i></a>
                </div>
            </div>
            <div class="col-12 col-md-2">
                <div class="single-special text-center wow fadeInUp" data-wow-delay="0.2s">
                    <div class="single-icon">
                        <img class="logo-exchanges img-fluid m-auto" src="img/scr-img/simex-logo.png">
                    </div>
                    <p><?php echo txt53 ?></p>
                    <a class="table-shadow text-white bg-gradient btn btn-block btn-outline-dark mt-3 btn-rouded"
                       href="#"><?php echo txt54 ?><i class="fa fa-exchange pl-1 pr-1" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ***** Pricing Plane Area End ***** -->

<!-- ***** Client Feedback Area Start ***** -->
<section class="clients-feedback-area bg-white pt-5 pb-5 clearfix" id="blog">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

            </div>
        </div>


    </div>
</section>
<!-- ***** Client Feedback Area End ***** -->



</head>
<!--Copy the stuff between the body tags and add it to your webpage-->
<body>



<!-- ***** CTA Area Start ***** -->
<section class="bg-gradient  section_padding_50 clearfix">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 col-lg-8">
                <div class="membership-description">
                    <h2><?php echo txt57 ?></h2>
                    <span class="text-white">
                            <span class="colorWinco">4587.40000000 <b>WCO</b></span> to 0x81c0403e45cd6000898a90f1b8211659787f9455<br>
                            <span class="colorWinco">1402.71428571 <b>WCO</b></span> to 0xc1feef1792e6d06ba68ff4943c059a8efc87d984<br>
                            <span class="colorWinco">3045.20000000 <b>WCO</b></span> to 0x2127339a9d59c2dac087ec435f50c5bdf36b275e<br>
                            <span class="colorWinco">9288.00000000 <b>WCO</b></span> to 0xf7beefec8421c27bd41bcd2c1d9611a123027bca<br>
                            <span class="colorWinco">1731.20000000 <b>WCO</b></span> to 0xaa57641d5fb763de0a1022228e85e30e03b34a63<br>
                            <span class="colorWinco">2275197.40000000 <b>WCO</b></span> to 0x39920da2ea315d85cc7188835e3ffbffa8ebb53f<br>
                            <span class="colorWinco">33801.60000000 <b>WCO</b></span> to 0xef54ac5db812a06cd52499e97ff7362de97e708e<br>
                            <span class="colorWinco">6464.40000000 <b>WCO</b></span> to 0xa78a9d5bbaaab1aea1e8abc17e56e046c7c77da9<br>
                            <span class="colorWinco">6592.60000000 <b>WCO</b></span> to 0xf4cd46f2b978410cdb6d13c2105942b0ea7641d0<br>
                            <span class="colorWinco">60774.80000000 <b>WCO</b></span> to 0xc8906e3cd107f833666d12161dfd28a4437928f9<br>
                            <span class="colorWinco">2423.40000000 <b>WCO</b></span> to 0x511dc56a8f917e823990811445108d5901cdf7c2<br>
                            <span class="colorWinco">39898.80000000 <b>WCO</b></span> to 0x3b8873ed21846c4bb29755cd12ee8f9062df3595<br>
                            <span class="colorWinco">92879.20000000 <b>WCO</b></span> to 0xc89c215af496e4543f6a957654273aa34a9d4d6d<br>
                        </span>
                </div>
                <div class="get-started-button wow bounceInDown text-left mt-3 mb-3" data-wow-delay="0.5s">
                    <a href="#"><?php echo txt58 ?><i class="fa fa-angle-double-right pl-1 pr-1" aria-hidden="true"></i></a>
                </div>
            </div>
            <div class="col-12 col-lg-4">
                <script type="text/javascript">var cf_widget_size = "small"; var cf_widget_from = "WCO"; var cf_widget_to = "usd"; var cf_widget_name = "Winco"; var cf_clearstyle = true;</script>
                <script src="https://www.worldcoinindex.com/content/widgets/js/render_widget-min.js" type="text/javascript"></script>
            </div>
        </div>
    </div>
</section>

<!-- ***** Footer Area Start ***** -->
<footer class="footer-social-icon text-center pt-4 clearfix">
    <!-- footer logo -->
    <div class="footer-text ">
        <a class="img-fluid m-auto" href="#"><img class="logo-footer" src="img/scr-img/winco-logo-website.png"></a>
    </div>
    <!-- social icon-->
    <div class="footer-social-icon">
        <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a> <a href="#"><i class="fa fa-twitter"
                                                                                         aria-hidden="true"></i></a> <a href="#"> <i class="fa fa-telegram" aria-hidden="true"></i></a> <a href="#"><i
                    class="fa fa-youtube" aria-hidden="true"></i></a>
    </div>
    <div class="footer-menu">
        <nav>
            <ul>
                <li><a href="#"><?php echo txt59 ?></a></li>
                <li><a href="#"><?php echo txt60 ?></a></li>
                <li><a href="#"><?php echo txt61 ?></a></li>
                <li><a href="#"><?php echo txt62 ?></a></li>
                <li><a href="#"><?php echo txt63 ?></a></li>
            </ul>
        </nav>
    </div>
    <!-- Foooter Text-->
    <div class="copyright-text">
        <!-- ***** Removing this text is now allowed! This template is licensed under CC BY 3.0 ***** -->
        <p><?php echo txt64 ?></p>
    </div>
</footer>
<!-- ***** Footer Area Start ***** -->

<!-- Jquery-2.2.4 JS -->
<script src="js/jquery-2.2.4.min.js"></script>
<!-- Popper js -->
<script src="js/popper.min.js"></script>
<!-- Bootstrap-4 Beta JS -->
<script src="js/bootstrap.min.js"></script>
<!-- All Plugins JS -->
<script src="js/plugins.js"></script>
<!-- Slick Slider Js-->
<script src="js/slick.min.js"></script>
<!-- Footer Reveal JS -->
<script src="js/footer-reveal.min.js"></script>
<!-- Active JS -->
<script src="js/active.js"></script>


<script src="vue.min.js"></script>
<script src="vue-resource.min.js"></script>
<script src="script.js"></script>
<script>
    $(document).ready(function () {
        $('.single-special').find('a').hide();
        $('.single-special').mouseenter(function () {
            $(this).find('a').fadeIn(400);
        });
        $('.single-special').mouseleave(function () {
            $(this).find('a').fadeOut(400);
        });
        $('.read_more').attr('target', '_blank');


        $( '#get-another-quote-button' ).on( 'click', function ( e ) {
            e.preventDefault();
            $.ajax( {
                url: 'https://blog.winco.io//wp-json/wp/v2/posts',
                success: function ( data ) {
//                    data = json_decode(data);

                    for (i = 0; i < data.length; i++) {
//                        var post = data.shift(); // The data is an array of posts. Grab the first one.
                        $( '#quote-title' ).text( post.title );
                        $( '#quote-content' ).html( post.content );

                        // If the Source is available, use it. Otherwise hide it.
                        if ( typeof post.custom_meta !== 'undefined' && typeof post.custom_meta.Source !== 'undefined' ) {
                            $( '#quote-source' ).html( 'Source:' + post.link );
                        } else {
                            $( '#quote-source' ).text( '' );
                        }
                    }

                },
                cache: false
            } );
        } );
    });



</script>
</body>

</html>
