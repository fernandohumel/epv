<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Dvino Admin Panel" />
    <meta name="keywords" content="Admin, Dashboard, Divino, Sass, CSS3, HTML5, Responsive Dashboard, Responsive Admin Template, Admin Template, Best Admin Template, Bootstrap Template, Themeforest" />
    <meta name="author" content="Fernando Humel" />
    <link rel="shortcut icon" href="../img/white-logo.png" />
    <title>Divino Dashboard</title>

    <!-- Common CSS -->
    <link rel="stylesheet" href="../css/bootstrap.min.css" />
    <link rel="stylesheet" href="../fonts/icomoon/icomoon.css" />
    <link rel="stylesheet" href="../css/main.css" />

    <!-- Other CSS includes plugins - Cleanedup unnecessary CSS -->
    <!-- Chartist css -->
    <link href="../vendor/chartist/css/chartist.min.css" rel="stylesheet" />
    <link href="../vendor/chartist/css/chartist-custom.css" rel="stylesheet" />


    <link rel="stylesheet" href="../css/bootstrap-theme.css">
    <link rel="stylesheet" href="../css/bootstrap.css">
    <link rel="stylesheet" href="../css/main.css">
    <link href="../jquery-ui/jquery-ui.css" rel="stylesheet">
    <link href="../js/jquery.timepicker.css" rel="stylesheet">
    <link href="../js/wickedpicker/wickedpicker.min.css" rel="stylesheet">
    <link href="../js/fullcalendar/fullcalendar.css" rel="stylesheet">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


</head>
<body>

<!-- Loading starts -->
<div class="loading-wrapper">
    <div class="loading">
        <div class="img"></div>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- Loading ends -->

<!-- BEGIN .app-wrap -->
<div class="app-wrap">
    <!-- BEGIN .app-heading -->
    <header class="app-header">
        <div class="container-fluid">
            <div class="row gutters">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-3 col-4">
                    <a class="mini-nav-btn" href="#" id="app-side-mini-toggler">
                        <i class="icon-chevron-thin-left"></i>
                    </a>
                    <a href="#app-side" data-toggle="onoffcanvas" class="onoffcanvas-toggler" aria-expanded="true">
                        <i class="icon-chevron-thin-left"></i>
                    </a>
                </div>
                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-4">
                    <a href="index.php" class="logo">

                    </a>
                </div>
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-3 col-4">
                    <ul class="header-actions">

                        <li>

                            <div class="dropdown-menu dropdown-menu-right lg" aria-labelledby="todos">
                                <ul class="stats-widget">
                                    <li>
                                        <h4>$37895</h4>
                                        <p>Revenue <span>+2%</span></p>
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="87"
                                                 aria-valuemin="0" aria-valuemax="100" style="width: 87%">
                                                <span class="sr-only">87% Complete (success)</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <h4>4,897</h4>
                                        <p>Downloads <span>+39%</span></p>
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="65"
                                                 aria-valuemin="0" aria-valuemax="100" style="width: 65%">
                                                <span class="sr-only">65% Complete (success)</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <h4>2,219</h4>
                                        <p>Uploads <span class="text-secondary">-7%</span></p>
                                        <div class="progress">
                                            <div class="progress-bar bg-danger" role="progressbar" aria-valuenow="42"
                                                 aria-valuemin="0" aria-valuemax="100" style="width: 42%">
                                                <span class="sr-only">42% Complete (success)</span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="dropdown">
                            <a href="#" id="userSettings" class="user-settings" data-toggle="dropdown"
                               aria-haspopup="true">
                                <!--                                        <img class="avatar" src="img/user.png" alt="User Thumb" />-->
                                <span class="user-name" id="nome"></span>
                                <i class="icon-chevron-small-down"></i>
                            </a>
                            <div class="dropdown-menu lg dropdown-menu-right" aria-labelledby="userSettings">
                                <!--
                                                                        <ul class="user-settings-list">
                                                                            <li>
                                                                                <a href="profile.html">
                                                                                    <div class="icon">
                                                                                        <i class="icon-account_circle"></i>
                                                                                    </div>
                                                                                    <p>Profile</p>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="profile.html">
                                                                                    <div class="icon red">
                                                                                        <i class="icon-cog3"></i>
                                                                                    </div>
                                                                                    <p>Settings</p>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="filters.html">
                                                                                    <div class="icon yellow">
                                                                                        <i class="icon-schedule"></i>
                                                                                    </div>
                                                                                    <p>Activity</p>
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                -->
                                <div class="logout-btn">
                                    <a id='logout' class="btn btn-primary">Logout</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </header>
    <!-- END: .app-heading -->
    <!-- BEGIN .app-container -->
    <div class="app-container">
        <!-- BEGIN .app-side -->
        <aside class="app-side" id="app-side">
            <!-- BEGIN .side-content -->
            <div class="side-content ">
                <!-- BEGIN .user-profile -->
                <div class="user-profile">
                    <a href="index.php" class="logo">
                        <img src="../img/white-logo.png" class="img-responsive" style="max-height:100px;" width="64px"
                             alt="Divino Admin Dashboard"/>
                    </a>
                    <ul class="profile-actions">
                        <li>
                            <a href="#">
                                <i class="fa fa-instagram"></i>

                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="icon-social-facebook"></i>
                            </a>
                        </li>
                        <li>
                            <a id='logout'>
                                <i class="icon-export"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- END .user-profile -->
                <!-- BEGIN .side-nav -->
                <nav class="side-nav">
                    <!-- BEGIN: side-nav-content -->
                    <ul class="unifyMenu" id="unifyMenu">
                        <li class="active selected">
                            <a href="#" class="has-arrow" aria-expanded="false">
                                        <span class="has-icon">
                                            <i class="icon-laptop_windows"></i>
                                        </span>
                                <span class="nav-title">Dashboards</span>
                            </a>
                            <ul aria-expanded="false" >
                                <li>
                                    <a href='index.php' >Dashboard</a>
                                </li>

                            </ul>
                            </a>
                            <ul aria-expanded="false">

                                <li>
                                    <a href='calendar.php'>Calendar</a>
                                </li>

                            </ul>

                            <ul aria-expanded="false" class="collapse in" >

                                <li>
                                    <a href='balcao.php' class="current-page">Balcao</a>
                                </li>

                            </ul>
                        </li>


                        <li>
                            <br>
                            <a href="#" class="has-arrow" aria-expanded="false">
                                        <span class="has-icon">
                                            <i class="icon-tabs-outline"></i>
                                        </span>
                                <span class="nav-title">Relatorios</span>
                            </a>
                            <ul aria-expanded="false">

                                <li>
                                    <a href='relatorio_agendamentos.php'>Agendamentos</a>

                                </li>
                                <li>
                                    <a href='relatorio_profisisonais.php'>Profisisonais</a>

                                </li>
                                <li>
                                    <a href='relatorio_total.php'>Total</a>

                                </li>
                                <li>
                                    <a href='relatorio_clientes.php'>Clientes</a>

                                </li>
                                <li>
                                    <a href='relatorio_balcao.php'>Balcao</a>

                                </li>
                                <li>
                                    <a href='relatorio_ag_gratuitos.php'>Agendamentos Gratuitos</a>

                                </li>

                                <li>
                                    <a href='relatorio_prod_cortesia.php'>Produtos Cortesia</a>

                                </li>

                                <li>
                                    <a href='relatorio_estoque.php'>Estoque</a>

                                </li>

                                <li>
                                    <a href='relatorio_aniversarios.php' >Aniversáriantes</a>

                                </li>


                            </ul>
                        </li>

                        <li>
                            <br>
                            <a href="#" class="has-arrow" aria-expanded="false">
                                        <span class="has-icon">
                                             <i class="icon-center_focus_strong"></i>
                                        </span>
                                <span class="nav-title">Cadastros</span>
                            </a>
                            <ul aria-expanded="false">
                                <li>
                                    <a href='//localhost:8888/agendamentoAPI/web/index.php?r=cliente%2Findex'>Clientes</a>
                                    <a href='//localhost:8888/agendamentoAPI/web/index.php?r=filial%2Findex'>Filiais</a>
                                    <a href='//localhost:8888/agendamentoAPI/web/index.php?r=produto%2Findex'>Produtos</a>
                                    <a href='//localhost:8888/agendamentoAPI/web/index.php?r=profissao%2Findex'>Profissões</a>

                                    <a href='//localhost:8888/agendamentoAPI/web/index.php?r=profissional%2Findex'>Profissionais</a>
                                    <a href='//localhost:8888/agendamentoAPI/web/index.php?r=usuario%2Findex'>Usuários</a>


                                </li>


                            </ul>
                        </li>
                    </ul>
                    <!-- END: side-nav-content -->
                </nav>
                <!-- END: .side-nav -->
            </div>
            <!-- END: .side-content -->
        </aside>

        <!-- BEGIN .app-main -->
        <div class="app-main">
            <!-- BEGIN .main-heading -->
            <header class="main-heading">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
                            <div class="page-icon">
                                <i class="icon-laptop_windows"></i>
                            </div>
                            <div class="page-title">
                                <h5>Dashboard</h5>
                                <h6 class="sub-heading">Bem vindo ao Dashboard da Divino</h6>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                            <div class="right-actions">
                                <a href="#" class="btn btn-primary float-right" data-toggle="tooltip" data-placement="left" title="Imprimir Relatórios">
                                    <i class="icon-download4"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- END: .main-heading -->
            <!-- BEGIN .main-content -->
            <div class="main-content">
                <!-- Row start -->
                <div class="row gutters">



                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">


                        <div class="main-content">


                            <!-- Row start -->
                            <div class="row gutters">


                                <div class="card">
                                    <div class="card-body">
                                        <div class="stats-widget">
                                            <div class="stats-widget-header">
                                                <!--                                                <i class="icon-facebook"></i>-->
                                            </div>
                                            <div class="stats-widget-body">
                                                <!-- Row start -->
                                                <ul class="row no-gutters">


                                                    <div class="semprodutoestoque" style="display: none;">
                                                <span class="closebtn"
                                                      onclick="this.parentElement.style.display='none';">&times;</span>
                                                        Não possuí esse produto no estoque.
                                                    </div>

                                                    <div class="estoqueminimo" style="display: none;">
                                                <span class="closebtn"
                                                      onclick="this.parentElement.style.display='none';">&times;</span>
                                                        Seu Estoque está no mínimo!
                                                    </div>

                                                    <div id="conteudo2">
                                                        <div class="row">
                                                            <center>
                                                                <div class="col">
                                                                    <h2>Venda de Produtos</h2>
                                                                    <br>
                                                                </div>


                                                                <div class="col-md-5">
                                                                    <p>Produtos

                                                                    <select class='select-style' name="produtos" id="combobox-produtos">

                                                                    </select></p>

                                                                </div>


                                                                <div id="dialog-alert-pagamento" title="Finalizar Pagamento">


                                                                    <div class="row">
                                                                        <div class="col-md-12 text-center">
                                                                            Realmente finalizar a Compra?
                                                                        </div>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <center>
                                                                                <p>Total =
                                                                                    <a id="total-produtex">R$</a>
                                                                                </p>
                                                                            </center>
                                                                        </div>
                                                                    </div>
                                                                    <hr>
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <button class="col-md-12 btn btn-default"
                                                                                    id="finalizar-dinheiro">Dinheiro.
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                    <hr>

                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <button class="col-md-12 btn btn-default"
                                                                                    id="finalizar-debito">Cartão Débito.
                                                                            </button>
                                                                        </div>
                                                                    </div>

                                                                    <hr>

                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <button class="col-md-12 btn btn-default"
                                                                                    id="finalizar-credito">Cartão Crédito.
                                                                            </button>
                                                                        </div>
                                                                    </div>


                                                                </div>

                                                                <div class="col-md-2">

                                                                    <p> Adicionar <a style="text-align: center; "
                                                                       class="btn btn-default" id="addProduto2"
                                                                       role="button">+</a>
                                                                    </p>

                                                                </div>



                                                                <div class="col-md-2">
                                                                    <p>Total
                                                                    <input style="width:50%;" type="text" disabled name="total"
                                                                           id="total-produtos"/></p>

                                                                </div>

                                                                <div class="col-md-3">

                                                                    <a
                                                                       class="btn btn-default" id="finalizaCompra"
                                                                       role="button">Finalizar Compra</a>

                                                                </div>


                                                                <div class="row gutters">
                                                                    <div class="row">

                                                                        <div class="col-md-12" id="lista-produtos">
                                                                            <br>
                                                                            <hr>

                                                                            <p>Lista Produtos:</p>

                                                                            <table id="table-produtos" style="width:100%"
                                                                                   class="ui-widget ui-widget-content">
                                                                                <thead>
                                                                                <tr class="ui-widget-header ">
                                                                                    <th style=" display: none;">ID</th>
                                                                                    <th style='width: 50%'>Produto</th>
                                                                                    <th style='width: 35%' >Valor</th>

                                                                                    <th style=" display: none;">Servico</th>
                                                                                    <th style='width: 15%'>Alterar</th>
                                                                                </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                </tbody>
                                                                            </table>

                                                                        </div>

                                                                    </div>


                                                            </center>
                                                        </div>

                                                    </div>

                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- END: .main-content -->
        </div>
        <!-- END: .app-main -->
    </div>
    <!-- END: .app-container -->
    <!-- BEGIN .main-footer -->
    <footer class="main-footer fixed-btm">
        Copyright Divino Admin 2018.
    </footer>
    <!-- END: .main-footer -->
</div>
<!-- END: .app-wrap -->

<script src="../js/vendor/jquery-1.11.2.js"></script>
<script>window.jQuery || document.write('<script href="../js/vendor/jquery-1.11.2.js"><\/script>')</script>
<script src="../js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
<script src="../js/plugins.js"></script>





<script src="../js/main.js"></script>

<script src="../js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
<script src="../js/vendor/bootstrap.min.js"></script>


<!-- jQuery first, then Tether, then other JS. -->

<script src="../js/tether.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../vendor/unifyMenu/unifyMenu.js"></script>
<script src="../vendor/onoffcanvas/onoffcanvas.js"></script>
<script src="../js/moment.js"></script>

<!-- Slimscroll JS -->
<script src="../vendor/slimscroll/slimscroll.min.js"></script>
<script src="../vendor/slimscroll/custom-scrollbar.js"></script>



<script type="text/javascript" src="../js/wickedpicker/wickedpicker.min.js"></script>
<script type="text/javascript" src="../js/fullcalendar/moment.min.js"></script>

<script type="text/javascript" src="../js/fullcalendar/fullcalendar.js"></script>
<script type="text/javascript" src="../js/fullcalendar/pt-br.js"></script>
<script type="text/javascript" src="../js/fullcalendar/gcal.js"></script>

<script type="text/javascript" src="../js/jquery.timepicker.js"></script>




<script src="../js/balcao.js"></script>
<script src="../js/agendamento.js"></script>


<!-- Common JS -->
<script src="../js/common.js"></script>

<link href="../jquery-ui/jquery-ui.css" rel="stylesheet">


<link href="../css/custom-theme/jquery-ui-1.9.2.custom.css" rel="stylesheet">

<script src="../js/jquery-ui-1.9.2.custom.js"></script>



<style>

    .select-style {

        border: 1px solid #ccc;
        width: 175px;
        border-radius: 3px;
        overflow: hidden;
        background: #fafafa url("../img/oie_transparent.png") no-repeat 90% 50%;
    }

    .select-style select {
        list-style: georgian;
        padding: 5px 8px;
        width: 130%;
        border: none;
        box-shadow: none;
        background: transparent;
        background-image: none;
        -webkit-appearance: none;
    }

    .select-style select:focus {
        outline: none;
    }

    * {
        -webkit-box-sizing: border-box;
        box-sizing: border-box
    }

    /* Esconder no IE 10 */
    .select-style ::-ms-expand {
        display: none
    }

    /* Outros navegadores */
    .select-style {
        -webkit-appearance: none;
        appearance: none
    }

    .select-style :focus {
        outline: none;
    }

    .select-style :before {
        display: block;
        font-family: 'FontAwesome';
        height: 100%;
        position: absolute;
        top: 0;
        right: 0;
        width: 1em;
        z-index: -1
    }

    .select-style > select {
        display: block;
        margin: 0;
        padding: .5em;
        width: 100%;
    }

    /**
     * Pseudo-class 'any', referência:
     * https://developer.mozilla.org/en-US/docs/Web/CSS/:any */
    :-moz-any( .select-style ):before {
        background-color: #fff;
        pointer-events: none;
        z-index: 1;
    }

    .select-style {
        background-color: #fff;
        border: 1px solid #ccc;
        margin: 0 0 2em;
        padding: 0;
        overflow: hidden;
        -webkit-appearance: none; /* Remove estilo padrão do Chrome */
        -moz-appearance: none; /* Remove estilo padrão do FireFox */
        appearance: none; /* Remove estilo padrão do FireFox*/
    }

    .select-style:hover {
        border-color: #333;
    }

    .select-style:before {
        color: #333;
        font-size: 1em;
        line-height: 2.5em;
        padding: 0 0.625em;
        text-align: center;
    }

    .select-style {

    >
    select {
        background-color: transparent;
        border: 0 none;
        box-shadow: none;
        color: #333;
        font-size: 100%;
        line-height: normal;
        overflow: hidden;

    }

</style>

</body>
</html>




