
<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="Divini Admin Panel" />
        <meta name="keywords" content="Login, Divino Login, Admin, Dashboard, Bootstrap4, Sass, CSS3, HTML5, Responsive Dashboard, Responsive Admin Template, Admin Template, Best Admin Template, Bootstrap Template, Themeforest" />
        <meta name="author" content="Fernando Humel" />
        <link rel="shortcut icon" href="../img/white-logo.png" />
        <title>Divino Dashboard - Login</title>

       <!-- Common CSS -->
        <link rel="stylesheet" href="../css/bootstrap.min.css" />
        <link rel="stylesheet" href="../fonts/icomoon/icomoon.css" />

        <link rel="stylesheet" href="../css/bootstrap-theme.css">
        <link rel="stylesheet" href="../css/bootstrap.css">

        <link href="../jquery-ui/jquery-ui.css" rel="stylesheet">
        <link href="../js/jquery.timepicker.css" rel="stylesheet">
        <link href="../js/wickedpicker/wickedpicker.min.css" rel="stylesheet">
        <link href="../js/fullcalendar/fullcalendar.css" rel="stylesheet">
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
        <link rel="stylesheet" href="../css/main.css" />
<style>


</style>

    </head>

    <body class="login-bg" >

        <div class="container">
            <center>
            <div class="login-screen row align-items-center">


                    <form method="post" name ='act' action="" id="ajax_form">
                        <div class="login-container" style="justify-content:center">

                            <div class="col-xl-4 col-lg-4 col-md-4  col-sm-4">
                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-4  col-sm-4">

                                    <div class="login-box">
                                        <a href="#" class="login-logo">
                                            <center><img width="50%" src="../img/logogrande.png" alt="Divino Admin Dashboard" /></center>
                                        </a>
                                        <br>  <br> <br>
                                        <span id="login-text"> FAÇA O LOGIN PARA INICIAR SUA SESSÃO</span>
                                        <br>  <br> <br>
                                        <center>
                                        <div class="input-group" style="width:100%;">
                                            <span class="input-group-addon" ><i class="icon-account_circle"></i></span>
                                            <input type="text" id="usuario" class="form-control" placeholder="Usuário" aria-label="username" aria-describedby="username">
                                        </div>
                                        </center>
                                        <br>
                                        <center>
                                            <div class="input-group" style="width:100%; ">
                                            <span class="input-group-addon" ><i class="icon-verified_user"></i></span>
                                            <input type="password" id="password" name="password" class="form-control" placeholder="Senha" aria-label="Password" aria-describedby="password"  value="<?php if(isset($_COOKIE['password'])){ echo $_COOKIE['password']; }; ?>" />
                                        </div>
                                        </center>
                                        <div class="actions clearfix" >

                                         <span style="float:left;" >      <input  id='remember' name="lemb_senha" type="checkbox" value=""/>

                                            Lembrar senha  </span>

                                                <button style="float:right;"  id="login" class="btn btn-primary">Login</button>

                                        </div>





                            </div>
                        </div>

                    </form>
                <div class="col-xl-4 col-lg-4 col-md-4  col-sm-4">
                </div>
                </div>

            </div>
        </center>
        </div>
        <footer class="main-footer no-bdr fixed-btm">

                Copyright Divino Admin 2018.

        </footer>
    </body>


        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"> </script>
        <script src="../js/vendor/jquery-1.11.2.js"></script>
        <script>window.jQuery || document.write('<script src="../js/vendor/jquery-1.11.2.js"><\/script>')</script>
        <script src="../js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        <script src="../js/plugins.js"></script>



        <script src="../js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        <script src="../js/vendor/bootstrap.min.js"></script>


        <!-- jQuery first, then Tether, then other JS. -->
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
        <script src="../js/tether.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../vendor/unifyMenu/unifyMenu.js"></script>
        <script src="../vendor/onoffcanvas/onoffcanvas.js"></script>
        <script src="../js/moment.js"></script>

        <!-- Slimscroll JS -->
        <script src="../vendor/slimscroll/slimscroll.min.js"></script>
        <script src="../vendor/slimscroll/custom-scrollbar.js"></script>



    <script
            src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
            integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
            crossorigin="anonymous"></script>

        <script type="text/javascript" src="../js/wickedpicker/wickedpicker.min.js"></script>
        <script type="text/javascript" src="../js/fullcalendar/moment.min.js"></script>

        <script type="text/javascript" src="../js/fullcalendar/fullcalendar.js"></script>
        <script type="text/javascript" src="../js/fullcalendar/pt-br.js"></script>
        <script type="text/javascript" src="../js/fullcalendar/gcal.js"></script>

        <script type="text/javascript" src="../js/jquery.timepicker.js"></script>





        <!-- Common JS -->
        <script src="../js/common.js"></script>



        <script type = "text/javascript" >
            jQuery(document).ready(function() {
                jQuery('#ajax_form').submit(function() {
                    var dados = jQuery(this).serialize();
                    return false;

                });
            }); </script>


        <script>


            function isBlank(str) {
                return (!str || /^\s*$/.test(str));
            }


            function valida() {
                if (isBlank($('#usuario').val()) || isBlank($('#password').val())) {
                    return false;
                }

                return true;
            }


            $(document).ready(function () {
                //        console.log('Ready disparado');



                var senha = getCookie("senha");

                if (senha == null){

                }else{
                   $('#password').val(senha);
                }



                var usuario = getCookie("usuario");

                if (usuario == null){

                }else{
                    $('#usuario').val(usuario);
                }


                


              
                function enviaLogin() {


                    //            var url = "http://www.moradialzer.com.br/divinoAPI/web/index.php?r=divino/login&user=divino&pin=123";
                    //            var url = "http://www.moradialzer.com.br/divinoAPI/web/index.php?r=divino/login&user=" + $('#usuario').val() + "&pin=" + $('#password').val();
                    var url = "//localhost:8888/agendamentoAPI/web/index.php?r=divino/login&user=" + $('#usuario').val() + "&pin=" + $('#password').val();

                    console.log(url);
                    var jqxhr = $.get(url
                                      , function (data) {
                        //                    alert("success " + JSON.stringify(data));
                        //                    alert("success " + data[0].Nome);

                        if (data.error === false) {
                            if (parseInt(data.result[0].ID) > 0) {



                                setCookie("user", JSON.stringify(data));

                                var remember = document.getElementById('remember');
                                if (remember.checked) {

                                    setCookie("senha", $('#password').val());
                                    setCookie("usuario", $('#usuario').val());
                                }

                                setCookie("filial", JSON.stringify(data.result[0].Filial));
                                window.location.replace("index.php");
                            }
                            else {
                                alert("Humm..... " + JSON.stringify(data));
                            }
                        }
                        else {
                            alert(data.errorMessage);
                        }


                    })
                    .done(function () {
                        //                    alert("second success");
                    })
                    .fail(function () {
                        alert("error");
                    })
                    .always(function () {
                        //                    alert("finished");
                    });

                    // Perform other work here ...

                    // Set another completion function for the request above
                    jqxhr.always(function () {
                        //                alert("second finished");
                    });
                }



                $('#login').click(function () {

                    if (valida()){
                        // alert('passo para js');
                        enviaLogin();
                    }
                    else{
                        alert("Humm..... Prencha os campos para prosseguir com login.");
                    }
                });







                /*login com enter*/
                $('#navbar').keydown(function(event) {
                    // enter has keyCode = 13, change it if you want to use another button
                    if (event.keyCode == 13) {
                        if (valida()){
                            // alert('passo para js');
                            enviaLogin();
                        }
                        else{
                            alert("Humm..... Prencha os campos para prosseguir com login.");
                        }
                    }
                });

                console.log('Ready disparado');


            });

            function setCookie(key, value) {
                var expires = new Date();
                expires.setTime(expires.getTime() + (1 * 24 * 60 * 60 * 1000));
                document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
            }

            function getCookie(key) {
                var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
                return keyValue ? keyValue[2] : null;
            }

            function eraseCookie(name) {
                document.cookie = name + '=; Max-Age=0'
            }


            function logout(){

                eraseCookie("user");

            }

        </script>
<style>
    * {
        margin: 0;
        padding:0;
    }
    /* para garantir que estes elementos ocuparão toda a tela */
    body, html {
        width: 100%;
        height: 100%;
    }
    .container{
        margin:0px;

    }


    .row {
        display: block;
    }

    .form-control {
        font-size: 12px;

    }

    span i {
        font-size: 150%;
    }

    .main-footer {
        font-size: 12px;
        text-align: center;
    }

    #nome {
        font-size: 12px;
    }

    .main-footer {
        font-size: 12px;
        text-align: center;
    }

    .login-bg{
        font-size: 12px;
        text-align: center;
    }

    .form-control{
        padding-bottom: 4%;
        height:200%;
    }

</style>

</html>