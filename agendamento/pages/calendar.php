<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Dvino Admin Panel" />
    <meta name="keywords" content="Admin, Dashboard, Divino, Sass, CSS3, HTML5, Responsive Dashboard, Responsive Admin Template, Admin Template, Best Admin Template, Bootstrap Template, Themeforest" />
    <meta name="author" content="Fernando Humel" />
    <link rel="shortcut icon" href="../img/white-logo.png" />
    <title>Divino Dashboard</title>

    <!-- Common CSS -->
    <link rel="stylesheet" href="../css/bootstrap.min.css" />
    <link rel="stylesheet" href="../fonts/icomoon/icomoon.css" />
    <link rel="stylesheet" href="../css/main.css" />

    <!-- Other CSS includes plugins - Cleanedup unnecessary CSS -->
    <!-- Chartist css -->
    <link href="../vendor/chartist/css/chartist.min.css" rel="stylesheet" />
    <link href="../vendor/chartist/css/chartist-custom.css" rel="stylesheet" />


    <link rel="stylesheet" href="../css/bootstrap-theme.css">
    <link rel="stylesheet" href="../css/bootstrap.css">
    <link rel="stylesheet" href="../css/main.css">
    <link href="../jquery-ui/jquery-ui.css" rel="stylesheet">
    <link href="../js/jquery.timepicker.css" rel="stylesheet">
    <link href="../js/wickedpicker/wickedpicker.min.css" rel="stylesheet">
    <link href="../js/fullcalendar/fullcalendar.css" rel="stylesheet">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


</head>
<body>

<!-- Loading starts -->
<div class="loading-wrapper">
    <div class="loading">
        <div class="img"></div>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- Loading ends -->

<!-- BEGIN .app-wrap -->
<div class="app-wrap">
    <!-- BEGIN .app-heading -->
    <header class="app-header">
        <div class="container-fluid">
            <div class="row gutters">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-3 col-4">
                    <a class="mini-nav-btn" href="#" id="app-side-mini-toggler">
                        <i class="icon-chevron-thin-left"></i>
                    </a>
                    <a href="#app-side" data-toggle="onoffcanvas" class="onoffcanvas-toggler" aria-expanded="true">
                        <i class="icon-chevron-thin-left"></i>
                    </a>
                </div>
                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-4">
                    <a href="index.html" class="logo">

                    </a>
                </div>
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-3 col-4">
                    <ul class="header-actions">

                        <li>

                            <div class="dropdown-menu dropdown-menu-right lg" aria-labelledby="todos">
                                <ul class="stats-widget">
                                    <li>
                                        <h4>$37895</h4>
                                        <p>Revenue <span>+2%</span></p>
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="87"
                                                 aria-valuemin="0" aria-valuemax="100" style="width: 87%">
                                                <span class="sr-only">87% Complete (success)</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <h4>4,897</h4>
                                        <p>Downloads <span>+39%</span></p>
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="65"
                                                 aria-valuemin="0" aria-valuemax="100" style="width: 65%">
                                                <span class="sr-only">65% Complete (success)</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <h4>2,219</h4>
                                        <p>Uploads <span class="text-secondary">-7%</span></p>
                                        <div class="progress">
                                            <div class="progress-bar bg-danger" role="progressbar" aria-valuenow="42"
                                                 aria-valuemin="0" aria-valuemax="100" style="width: 42%">
                                                <span class="sr-only">42% Complete (success)</span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="dropdown">
                            <a href="#" id="userSettings" class="user-settings" data-toggle="dropdown"
                               aria-haspopup="true">
                                <!--                                        <img class="avatar" src="img/user.png" alt="User Thumb" />-->
                                <span class="user-name" id="nome"></span>
                                <i class="icon-chevron-small-down"></i>
                            </a>
                            <div class="dropdown-menu lg dropdown-menu-right" aria-labelledby="userSettings">
                                <!--
                                                                        <ul class="user-settings-list">
                                                                            <li>
                                                                                <a href="profile.html">
                                                                                    <div class="icon">
                                                                                        <i class="icon-account_circle"></i>
                                                                                    </div>
                                                                                    <p>Profile</p>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="profile.html">
                                                                                    <div class="icon red">
                                                                                        <i class="icon-cog3"></i>
                                                                                    </div>
                                                                                    <p>Settings</p>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="filters.html">
                                                                                    <div class="icon yellow">
                                                                                        <i class="icon-schedule"></i>
                                                                                    </div>
                                                                                    <p>Activity</p>
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                -->
                                <div class="logout-btn">
                                    <a id='logout' class="btn btn-primary">Logout</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </header>
    <!-- END: .app-heading -->
    <!-- BEGIN .app-container -->
    <div class="app-container">
        <!-- BEGIN .app-side -->
        <aside class="app-side" id="app-side">
            <!-- BEGIN .side-content -->
            <div class="side-content ">
                <!-- BEGIN .user-profile -->
                <div class="user-profile">
                    <a href="index.php" class="logo">
                        <img src="../img/white-logo.png" class="img-responsive"style="max-height:100px;" width="64px"
                             alt="Divino Admin Dashboard"/>
                    </a>
                    <ul class="profile-actions">
                        <li>
                            <a href="#">
                                <i class="fa fa-instagram"></i>

                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="icon-social-facebook"></i>
                            </a>
                        </li>
                        <li>
                            <a id='logout'>
                                <i class="icon-export"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- END .user-profile -->
                <!-- BEGIN .side-nav -->
                <nav class="side-nav">
                    <!-- BEGIN: side-nav-content -->
                    <ul class="unifyMenu" id="unifyMenu">
                        <li class="active selected">
                            <a href="#" class="has-arrow" aria-expanded="false">
                                        <span class="has-icon">
                                            <i class="icon-laptop_windows"></i>
                                        </span>
                                <span class="nav-title">Dashboards</span>
                            </a>
                            <ul aria-expanded="false" class="collapse in">
                                <li>
                                    <a href='index.php' >Dashboard</a>
                                </li>

                            </ul>
                            </a>
                            <ul aria-expanded="false">

                                <li>
                                    <a href='calendar.php' class="current-page">Calendar</a>
                                </li>

                            </ul>

                            <ul aria-expanded="false">

                                <li>
                                    <a href='balcao.php'>Balcao</a>
                                </li>

                            </ul>
                        </li>


                        <li>
                            <br>
                            <a href="#" class="has-arrow" aria-expanded="false">
                                        <span class="has-icon">
                                           <i class="icon-tabs-outline"></i>
                                        </span>
                                <span class="nav-title">Relatorios</span>
                            </a>
                            <ul aria-expanded="false">

                                <li>
                                    <a href='relatorio_agendamentos.php'>Agendamentos</a>

                                </li>
                                <li>
                                    <a href='relatorio_profisisonais.php'>Profisisonais</a>

                                </li>
                                <li>
                                    <a href='relatorio_total.php'>Total</a>

                                </li>
                                <li>
                                    <a href='relatorio_clientes.php'>Clientes</a>

                                </li>
                                <li>
                                    <a href='relatorio_balcao.php'>Balcao</a>

                                </li>
                                <li>
                                    <a href='relatorio_ag_gratuitos.php'>Agendamentos Gratuitos</a>

                                </li>

                                <li>
                                    <a href='relatorio_prod_cortesia.php'>Produtos Cortesia</a>

                                </li>

                                <li>
                                    <a href='relatorio_estoque.php'>Estoque</a>

                                </li>

                                <li>
                                    <a href='relatorio_aniversarios.php' class="current-page">Aniversáriantes</a>

                                </li>


                            </ul>
                        </li>

                        <li>
                            <br>
                            <a href="#" class="has-arrow" aria-expanded="false">
                                        <span class="has-icon">
                                            <i class="icon-center_focus_strong"></i>
                                        </span>
                                <span class="nav-title">Cadastros</span>
                            </a>
                            <ul aria-expanded="false">
                                <li>
                                    <a href='//localhost:8888/agendamentoAPI/web/index.php?r=cliente%2Findex'>Clientes</a>
                                    <a href='//localhost:8888/agendamentoAPI/web/index.php?r=filial%2Findex'>Filiais</a>
                                    <a href='//localhost:8888/agendamentoAPI/web/index.php?r=produto%2Findex'>Produtos</a>
                                    <a href='//localhost:8888/agendamentoAPI/web/index.php?r=profissao%2Findex'>Profissões</a>

                                    <a href='//localhost:8888/agendamentoAPI/web/index.php?r=profissional%2Findex'>Profissionais</a>
                                    <a href='//localhost:8888/agendamentoAPI/web/index.php?r=usuario%2Findex'>Usuários</a>


                                </li>


                            </ul>
                        </li>
                    </ul>
                    <!-- END: side-nav-content -->
                </nav>
                <!-- END: .side-nav -->
            </div>
            <!-- END: .side-content -->
        </aside>

        <!-- END: .app-side -->

        <!-- BEGIN .app-main -->
        <div class="app-main">
            <!-- BEGIN .main-heading -->
            <header class="main-heading">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
                            <div class="page-icon">
                                <i class="icon-laptop_windows"></i>
                            </div>
                            <div class="page-title">
                                <h5>Dashboard</h5>
                                <h6 class="sub-heading">Bem vindo ao Dashboard da Divino</h6>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                            <div class="right-actions">
                                <a href="#" class="btn btn-primary float-right" data-toggle="tooltip" data-placement="left" title="Imprimir Relatórios">
                                    <i class="icon-download4"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- END: .main-heading -->
            <!-- BEGIN .main-content -->
            <div class="main-content">
                <!-- Row start -->
                <div class="row gutters">
                    <div class="container">

                        <div class="semprodutoestoque" style="display: none">
                            <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
                            Não possuí esse produto no estoque.
                        </div>

                        <div class="chicao" style="display: none">
                            <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
                            Este Profisisonal Atende somente de Sexta e Sábado.
                        </div>

                        <div class="profisisonal" style="display: none">
                            <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
                            Selecione um Profisisonal.
                        </div>

                        <div class="servico" style="display: none">
                            <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
                            Voce precisa de um produto Servico.
                        </div>

                        <div class="campoagendamento" style="display: none">
                            <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
                            Informe todos os campos para incluir um agendamento!
                        </div>

                        <div class="informecliente" style="display: none">
                            <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
                            Informe todos campos para incluir o cliente.
                        </div>

                        <div class="selecionecliente" style="display: none">
                            <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
                            Selecione um cliente!
                        </div>

                        <div class="selecioneproduto" style="display: none">
                            <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
                            Selecione um produto!
                        </div>

                        <div class="estoqueminimo" style="display: none">
                            <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
                            Seu Estoque está no mínimo!
                        </div>


                        <div id="dialog-alert-cancelamento" title="Cancelar Agendamento">


                            <div class="row">
                                <div class="col-md-12 text-center">
                                    Realmente cancelar o agendamento?
                                </div>
                            </div>
                            <hr>

                            <div class="row">
                                <div class="col-md-12">
                                    <button class="col-md-12 btn btn-success" id="sim-cancelar">Sim. Cancelar.</button>
                                </div>
                            </div>

                            <hr>

                            <div class="row">
                                <div class="col-md-12">
                                    <button class="col-md-12 btn btn-danger" id="nao-cancelar">Não. Deixar como está.</button>
                                </div>
                            </div>


                        </div>



                        <div id="dialog-cancelamento-justificativa" title="Justificativa Agendamento">


                            <div class="row">
                                <div class="col-md-12 text-center">
                                    Escreva a justificatiava?
                                </div>
                            </div>
                            <hr>

                            <div class="row">
                                <div class="text-center">
                                    <p style="text-align:center";>Justificativa:</p>
                                    <input style="width:90%;" type="text" name="justificativa" id="justificativa"/>
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-md-12">
                                        <button class="col-md-12 btn btn-danger" id="sim-justificativa">Cancelar</button>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <button class="col-md-12 btn btn-default" id="nao-justificativa">Deixar como esta</button>
                                    </div>
                                </div>


                            </div>
                        </div>




                        <div id="dialog-alert-pagamento" title="Finalizar Pagamento">


                            <div class="row">
                                <div class="col-md-12 text-center">
                                    Realmente finalizar o agendamento?
                                </div>
                            </div>
                            <hr>

                            <div class="row">
                                <div class="col-md-12">
                                    <center>
                                        <p>Total =
                                            <a id="total-produtex">R$</a>
                                        </p>
                                    </center>
                                </div>
                            </div>
                            <hr>

                            <div class="row">
                                <div class="col-md-12">
                                    <button class="col-md-12 btn btn-default" id="pagamento-Dinheiro">Dinheiro.</button>
                                </div>
                            </div>

                            <hr>

                            <div class="row">
                                <div class="col-md-12">
                                    <button class="col-md-12 btn btn-default" id="pagamento-Debito">Cartão Débito.</button>
                                </div>
                            </div>


                            <hr>

                            <div class="row">
                                <div class="col-md-12">
                                    <button class="col-md-12 btn btn-default" id="pagamento-Credito">Cartão Crédito.</button>
                                </div>
                            </div>


                        </div>



                        <div id="dialog-alert-finalizar" title="Finalizar Agendamento">


                            <div class="row">
                                <div class="col-md-12 text-center">
                                    Realmente finalizar o agendamento?
                                </div>
                            </div>
                            <hr>

                            <div class="row">
                                <div class="col-md-12">
                                    <button class="col-md-12 btn btn-success" id="sim-finalizar">Sim. Finalizar.</button>
                                </div>
                            </div>

                            <hr>

                            <div class="row">
                                <div class="col-md-12">
                                    <button class="col-md-12 btn btn-danger" id="nao-finalizar">Não. Deixar como está.</button>
                                </div>
                            </div>


                        </div>

                        <div id="agendamento-gratuito" title="Sem  Pagamento">


                            <div class="row">
                                <div class="col-md-12 text-center">
                                    Parabéns esse agendamento é Gratuito!! Obrigado pela sua fidelidade
                                </div>
                            </div>
                            <hr>



                            <div class="row">
                                <div class="col-md-12">
                                    <button class="col-md-12 btn btn-default" id="pagamento-sem">Volte Sempre.</button>
                                </div>
                            </div>


                        </div>


                        <div id="dialog-alert-agendamento" title="Agendamento">


                            <div class="row">
                                <div class="col-md-12 text-center">
                                    O que deseja fazer com este agendamento?
                                </div>
                            </div>
                            <hr>
                            <div class="row">

                                <div class="col-md-4">
                                    <button class="col-md-12 btn btn-info" id="finalizar">Finalizar</button>
                                </div>

                                <div class="col-md-4">
                                    <button class="col-md-12 btn btn-success" id="editar">Editar</button>
                                </div>

                                <div class="col-md-4">
                                    <button class="col-md-12 btn btn-danger" id="cancelar">Cancelar</button>
                                </div>
                            </div>

                            <hr>

                            <div class="row">
                                <div class="col-md-12">
                                    <button class="col-md-12 btn btn-default" id="nada">Nada. Deixar como está.</button>
                                </div>
                            </div>


                        </div>
                        <center>

                            <div id="dialog-agendamento" title="Cadastros" >
                                <!--    <p class="validateTips">Todos os campos são necessários.</p>-->

                                <form>
                                    <fieldset>

                                        <div id="accordion-agendamento" class="accordion">

                                            <h3>Cliente</h3>
                                            <div>


                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <p>Nome:</p>
                                                        <input type="text" name="nome-cliente" id="nome-cliente"/>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <p>CPF:</p>
                                                        <input type="text" name="cpf-cliente" id="cpf-cliente"/>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <p>Aniversario:</p>
                                                        <input type="text" name="aniversario-cliente" id="aniversario-cliente"/>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <p>Telefone:</p>
                                                        <input type="text" name="telefone-cliente" id="telefone-cliente"/>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <p>Email:</p>
                                                        <input type="text" name="email-cliente" id="email-cliente"/>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <p>Cadastrar:</p>

                                                        <a class="btn btn-default" id="salvar-cliente" role="button">Cadastrar</a>
                                                    </div>


                                                </div>


                                            </div>


                                            <h3>Agendamento</h3>
                                            <div>
                                                <div class="row">
                                                    <div class="col-md-3">

                                                        <p>Cliente:</p>
                                                        <select id="combobox-cliente">
                                                            <!--                                        <option value="">Selecione um cliente...</option>-->
                                                        </select>
                                                    </div>

                                                    <div class="col-md-3">

                                                        <p>Data:</p>
                                                        <input type="text" id="datepicker">
                                                    </div>

                                                    <div class="col-md-3">

                                                        <p>Horário</p>

                                                        <input type="text" name="timepicker" id="timepicker" class="timepicker"/>


                                                    </div>


                                                    <div class="col-md-3">

                                                        <p>Profissional</p>
                                                        <div class="notIE">
                                                            <!-- <![endif]-->
                                                            <span class="fancyArrow"></span>
                                                            <select name="profissional" id="profissional" >
                                                                <!--                                            <option value="">Selecione um profissional...</option>-->
                                                            </select>
                                                        </div>

                                                    </div>


                                                </div>

                                                <div class="row">

                                                    <div class="col-md-3">
                                                        <p>Produtos</p>

                                                        <select name="produtos" id="produtos">
                                                            <!--                                            <option value="">Selecione um produto...</option>-->
                                                        </select>

                                                    </div>

                                                    <div class="col-md-3">
                                                        <p>Adicionar</p>
                                                        <a class="btn btn-default" id="addProduto" role="button">+</a>

                                                    </div>


                                                </div>

                                                <div class="row">

                                                    <div class="col-md-12" id="lista-produtos">
                                                        <p>Lista Produtos:</p>

                                                        <table id="table-produtos"  style="width:100%" class="ui-widget ui-widget-content">
                                                            <thead>
                                                            <tr class="ui-widget-header ">
                                                                <th style=" display: none;">ID</th>
                                                                <th>Produto</th>
                                                                <th>Valor</th>
                                                                <th>Tempo</th>
                                                                <th>Servico</th>
                                                                <th>Alterar</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                            </tr>
                                                            </tbody>
                                                        </table>

                                                    </div>

                                                </div>


                                            </div>

                                        </div>

                                        <!-- Allow form submission with keyboard without duplicating the dialog button -->
                                        <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
                                    </fieldset>
                                </form>
                            </div>
                        </center>



                        <!--Modal Atualizar Agendamento-->


                        <div id="dialog-agendamento-atualizar" title="Cadastros">
                            <!--    <p class="validateTips">Todos os campos são necessários.</p>-->

                            <form>
                                <fieldset>

                                    <div id="accordion-agendamento-atualizar" class="accordion">



                                        <h3>Agendamento</h3>
                                        <div>
                                            <div class="row">
                                                <div class="col-md-3">

                                                    <p>Cliente:</p>
                                                    <input type="text" id="combobox-cliente-atualizar" disabled="disabled">

                                                    <p>Telefone:</p>
                                                    <input type="text" id="combobox-cliente-telefone" disabled="disabled" >



                                                </div>

                                                <div class="col-md-3">

                                                    <p>Data:</p>
                                                    <input type="text" tabindex="-1" name="datepicker-atualiza" id="datepicker-atualiza">
                                                </div>

                                                <div class="col-md-3">

                                                    <p>Horário</p>

                                                    <input type="text" name="timepicker-atualiza"  tabindex="-1" id="timepicker-atualiza" class="timepicker"/>


                                                </div>


                                                <div class="col-md-3">

                                                    <p>Profissional</p>

                                                    <select name="profissional-atualizar" id="profissional-atualizar">
                                                        <!--                                            <option  value="">Selecione um profissional...</option>-->
                                                    </select>

                                                </div>


                                            </div>

                                            <div class="row">

                                                <div class="col-md-3">
                                                    <p>Produtos</p>

                                                    <select name="produtos-atualizar" id="produtos-atualizar">
                                                        <!--                                            <option value="">Selecione um produto...</option>-->
                                                    </select>

                                                </div>

                                                <div class="col-md-3">
                                                    <p>Adicionar</p>
                                                    <a class="btn btn-default" id="addProduto-atualizar" role="button">+</a>

                                                </div>


                                            </div>

                                            <div class="row">

                                                <div class="col-md-12" id="lista-produtos-atualizar">
                                                    <p>Lista Produtos:</p>

                                                    <table id="table-produtos-atualizar" class="ui-widget ui-widget-content">
                                                        <thead>
                                                        <tr class="ui-widget-header ">
                                                            <th style=" display: none;">ID</th>
                                                            <th>Produto</th>
                                                            <th>Valor</th>
                                                            <th>Tempo</th>
                                                            <th>Servico</th>
                                                            <th>Alterar</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        </tbody>
                                                    </table>

                                                </div>

                                            </div>


                                        </div>

                                    </div>

                                    <!-- Allow form submission with keyboard without duplicating the dialog button -->
                                    <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
                                </fieldset>
                            </form>
                        </div>


                        <br><br>
                        <div></div>

                        <div id='calendar'></div>


                        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->


                    </div>
                </div>
            </div>

            <!-- END: .main-content -->
        </div>
        <!-- END: .app-main -->
    </div>
    <!-- END: .app-container -->
    <!-- BEGIN .main-footer -->
    <footer class="main-footer fixed-btm">
        Copyright Divino Admin 2018.
    </footer>
    <!-- END: .main-footer -->
</div>
<!-- END: .app-wrap -->

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"> </script>
<script src="../js/vendor/jquery-1.11.2.js"></script>
<script>window.jQuery || document.write('<script href="../js/vendor/jquery-1.11.2.js"><\/script>')</script>
<script src="../js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
<script src="../js/plugins.js"></script>





<script src="../js/main.js"></script>

<script src="../js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
<script src="../js/vendor/bootstrap.min.js"></script>


<!-- jQuery first, then Tether, then other JS. -->

<script src="../js/tether.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../vendor/unifyMenu/unifyMenu.js"></script>
<script src="../vendor/onoffcanvas/onoffcanvas.js"></script>
<script src="../js/moment.js"></script>

<!-- Slimscroll JS -->
<script src="../vendor/slimscroll/slimscroll.min.js"></script>
<script src="../vendor/slimscroll/custom-scrollbar.js"></script>



<script type="text/javascript" src="../js/wickedpicker/wickedpicker.min.js"></script>
<script type="text/javascript" src="../js/fullcalendar/moment.min.js"></script>

<script type="text/javascript" src="../js/fullcalendar/fullcalendar.js"></script>
<script type="text/javascript" src="../js/fullcalendar/pt-br.js"></script>
<script type="text/javascript" src="../js/fullcalendar/gcal.js"></script>

<script type="text/javascript" src="../js/jquery.timepicker.js"></script>





<script src="../js/agendamento.js"></script>


<!-- Common JS -->
<script src="../js/common.js"></script>

<link href="../jquery-ui/jquery-ui.css" rel="stylesheet">


<link href="../css/custom-theme/jquery-ui-1.9.2.custom.css" rel="stylesheet">

<script src="../js/jquery-ui-1.9.2.custom.js"></script>


<style>
    .mydialog{
        height: 100%;

    }

    .ui-dialog{

        width:100%%;
        margin-left: 15%;
        height: auto;
    }
</style>

</body>
</html>