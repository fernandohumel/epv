<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Dvino Admin Panel"/>
    <meta name="keywords"
          content="Admin, Dashboard, Divino, Sass, CSS3, HTML5, Responsive Dashboard, Responsive Admin Template, Admin Template, Best Admin Template, Bootstrap Template, Themeforest"/>
    <meta na0me="author" content="Fernando Humel"/>
    <link rel="shortcut icon" href="../img/white-logo.png"/>
    <title>Divino Dashboard</title>

    <!-- Common CSS -->
    <link rel="stylesheet" href="../css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../fonts/icomoon/icomoon.css"/>
    <link rel="stylesheet" href="../css/main.css"/>

    <!-- Other CSS includes plugins - Cleanedup unnecessary CSS -->
    <!-- Chartist css -->
    <link href="../vendor/chartist/css/chartist.min.css" rel="stylesheet"/>
    <link href="../vendor/chartist/css/chartist-custom.css" rel="stylesheet"/>


    <link rel="stylesheet" href="../css/bootstrap-theme.css">
    <link rel="stylesheet" href="../css/bootstrap.css">

    <link href="../jquery-ui/jquery-ui.css" rel="stylesheet">
    <link href="../js/jquery.timepicker.css" rel="stylesheet">
    <link href="../js/wickedpicker/wickedpicker.min.css" rel="stylesheet">
    <link href="../js/fullcalendar/fullcalendar.css" rel="stylesheet">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/main.css">
    <link rel="stylesheet" href="cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
</head>
<body>

<!-- Loading starts -->
<div class="loading-wrapper">
    <div class="loading">
        <div class="img"></div>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- Loading ends -->

<!-- BEGIN .app-wrap -->
<div class="app-wrap">
    <!-- BEGIN .app-heading -->
    <header class="app-header">
        <div class="container-fluid">
            <div class="row gutters">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-3 col-4">
                    <a class="mini-nav-btn" href="#" id="app-side-mini-toggler">
                        <i class="icon-chevron-thin-left"></i>
                    </a>
                    <a href="#app-side" data-toggle="onoffcanvas" class="onoffcanvas-toggler" aria-expanded="true">
                        <i class="icon-chevron-thin-left"></i>
                    </a>
                </div>
                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 col-4">
                    <a href="index.php" class="logo">

                    </a>
                </div>
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-3 col-4">
                    <ul class="header-actions">

                        <li>

                            <div class="dropdown-menu dropdown-menu-right lg" aria-labelledby="todos">
                                <ul class="stats-widget">
                                    <li>
                                        <h4>$37895</h4>
                                        <p>Revenue <span>+2%</span></p>
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="87"
                                                 aria-valuemin="0" aria-valuemax="100" style="width: 87%">
                                                <span class="sr-only">87% Complete (success)</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <h4>4,897</h4>
                                        <p>Downloads <span>+39%</span></p>
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="65"
                                                 aria-valuemin="0" aria-valuemax="100" style="width: 65%">
                                                <span class="sr-only">65% Complete (success)</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <h4>2,219</h4>
                                        <p>Uploads <span class="text-secondary">-7%</span></p>
                                        <div class="progress">
                                            <div class="progress-bar bg-danger" role="progressbar" aria-valuenow="42"
                                                 aria-valuemin="0" aria-valuemax="100" style="width: 42%">
                                                <span class="sr-only">42% Complete (success)</span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="dropdown">
                            <a href="#" id="userSettings" class="user-settings" data-toggle="dropdown"
                               aria-haspopup="true">
                                <!--                                        <img class="avatar" src="img/user.png" alt="User Thumb" />-->
                                <span class="user-name" id="nome"></span>
                                <i class="icon-chevron-small-down"></i>
                            </a>
                            <div class="dropdown-menu lg dropdown-menu-right" aria-labelledby="userSettings">
                                <!--
<ul class="user-settings-list">
<li>
<a href="profile.html">
<div class="icon">
<i class="icon-account_circle"></i>
</div>
<p>Profile</p>
</a>
</li>
<li>
<a href="profile.html">
<div class="icon red">
<i class="icon-cog3"></i>
</div>
<p>Settings</p>
</a>
</li>
<li>
<a href="filters.html">
<div class="icon yellow">
<i class="icon-schedule"></i>
</div>
<p>Activity</p>
</a>
</li>
</ul>
-->
                                <div class="logout-btn">
                                    <a id='logout' class="btn btn-primary">Logout</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </header>
    <!-- END: .app-heading -->
    <!-- BEGIN .app-container -->
    <div class="app-container">
        <!-- BEGIN .app-side -->
        <aside class="app-side" id="app-side">
            <!-- BEGIN .side-content -->
            <div class="side-content ">
                <!-- BEGIN .user-profile -->
                <div class="user-profile">
                    <a href="index.php" class="logo">
                        <img src="../img/white-logo.png" style="max-height:100px;" width="64px" class="img-responsive"
                             alt="Divino Admin Dashboard"/>
                    </a>
                    <ul class="profile-actions">
                        <li>
                            <a href="#">
                                <i class="fa fa-instagram"></i>

                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="icon-social-facebook"></i>
                            </a>
                        </li>
                        <li>
                            <a id='logout'>
                                <i class="icon-export"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- END .user-profile -->
                <!-- BEGIN .side-nav -->
                <nav class="side-nav">
                    <!-- BEGIN: side-nav-content -->
                    <ul class="unifyMenu" id="unifyMenu">
                        <li>
                            <a href="#" class="has-arrow" aria-expanded="false">
                                        <span class="has-icon">
                                            <i class="icon-laptop_windows"></i>
                                        </span>
                                <span class="nav-title">Dashboards</span>
                            </a>
                            <ul aria-expanded="false">
                                <li>
                                    <a href='index.php'>Dashboard</a>
                                </li>

                            </ul>
                            <ul aria-expanded="false">

                                <li>
                                    <a href='calendar.php'>Calendar</a>
                                </li>

                            </ul>

                            <ul aria-expanded="false">

                                <li>
                                    <a href='balcao.php'>Balcao</a>
                                </li>

                            </ul>
                        </li>




                        <li class="active selected">
                            <br>
                            <a href="#" class="has-arrow" aria-expanded="false">
                                        <span class="has-icon">
                                           <i class="icon-tabs-outline"></i>
                                        </span>
                                <span class="nav-title">Relatorios</span>
                            </a>
                            <ul aria-expanded="false">

                                <li>
                                    <a href='relatorio_agendamentos.php'>Agendamentos</a>

                                </li>
                                <li>
                                    <a href='relatorio_profisisonais.php'>Profisisonais</a>

                                </li>
                                <li>
                                    <a href='relatorio_total.php'>Total</a>

                                </li>
                                <li>
                                    <a href='relatorio_clientes.php'>Clientes</a>

                                </li>
                                <li>
                                    <a href='relatorio_balcao.php'>Balcao</a>

                                </li>
                                <li>
                                    <a href='relatorio_ag_gratuitos.php'>Agendamentos Gratuitos</a>

                                </li>

                                <li>
                                    <a href='relatorio_prod_cortesia.php'>Produtos Cortesia</a>

                                </li>

                                <li>
                                    <a href='relatorio_estoque.php'>Estoque</a>

                                </li>

                                <li>
                                    <a href='relatorio_aniversarios.php' class="current-page">Aniversáriantes</a>

                                </li>


                            </ul>

                        </li>

                        <li>
                            <br>
                            <a href="#" class="has-arrow" aria-expanded="false">
                                        <span class="has-icon">
                                            <i class="icon-center_focus_strong"></i>
                                        </span>
                                <span class="nav-title">Cadastros</span>
                            </a>
                            <ul aria-expanded="false">
                                <li>
                                    <a href='//localhost:8888/agendamentoAPI/web/index.php?r=cliente%2Findex'>Clientes</a>
                                    <a href='//localhost:8888/agendamentoAPI/web/index.php?r=filial%2Findex'>Filiais</a>
                                    <a href='//localhost:8888/agendamentoAPI/web/index.php?r=produto%2Findex'>Produtos</a>
                                    <a href='//localhost:8888/agendamentoAPI/web/index.php?r=profissao%2Findex'>Profissões</a>

                                    <a href='//localhost:8888/agendamentoAPI/web/index.php?r=profissional%2Findex'>Profissionais</a>
                                    <a href='//localhost:8888/agendamentoAPI/web/index.php?r=usuario%2Findex'>Usuários</a>


                                </li>


                            </ul>
                        </li>
                    </ul>
                    <!-- END: side-nav-content -->
                </nav>
                <!-- END: .side-nav -->
            </div>
            <!-- END: .side-content -->
        </aside>
        <!-- END: .app-side -->

        <!-- BEGIN .app-main -->
        <div class="app-main">
            <!-- BEGIN .main-heading -->
            <header class="main-heading">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
                            <div class="page-icon">
                                <i class="icon-laptop_windows"></i>
                            </div>
                            <div class="page-title">
                                <h5>Dashboard</h5>
                                <h6 class="sub-heading">Bem vindo ao Dashboard da Divino</h6>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4">
                            <div class="right-actions">
                                <a href="#" class="btn btn-primary float-right" data-toggle="tooltip"
                                   data-placement="left" title="Imprimir Relatórios">
                                    <i class="icon-download4"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- END: .main-heading -->
            <!-- BEGIN .main-content -->
            <div class="main-content">
                <!-- Row start -->
                <div class="row gutters">

                    <div class="row">
                        <center>
                            <div class="col">
                                <h2>Selecione o Filtro e escolha o tipo do Relatório</h2>
                                <br>
                            </div>
                            <div id="data2" class="col-md-4">
                            </div>

                            <div id="data" class="col-md-1">
                                <h4>Data Inicial</h4>
                                <input type="text" id="inicio">
                            </div>

                            <div id="data" class="col-md-2">
                                <h4>Data Final</h4>
                                <div id="data" class="col-md-3">
                                </div>
                                <div id="data" class="col-md-6">
                                    <input type="text" id="fim">
                                </div>
                            </div>
                            <div id="data" class="col-md-1">
                                <h4>Status</h4>


                                <select class='select-style' id="combobox-status"></select>
                            </div>

                        </center>
                    </div>

                    <div class="row">
                    <div class="col-md-12">
                        <br>
                        <center>
                        <img src="//localhost:8888/agendamento/img/monthly-calendar01.png" alt="Divino Barbearia"
                             height="60" width="60">
                        <h3>Relatório Aniversário</h3>
                        <span>Veja os Aniversariantes </span>
                            <br>
                            <p></p>

                        <p><a class="btn btn-default" href="#resultado" id="aniversario" role="button">Ver
                                Agora&raquo;</a></p>
                        </center>
                    </div>
                    </center>

<!--                    <div id="relatorio-profissional">-->
<!--                        <div id="relatorio-box">-->
<!--                            <div class="col-md-3">-->
<!--                                <img src="//localhost:8888/agendamento/img/prof.png" alt="Divino Barbearia"-->
<!--                                     height="60" width="60">-->
<!--                                <h3>Relatório Profissionais</h3>-->
<!--                                <span>Veja os profissionais e de uma filial. </span>-->
<!--                                <p><a class="btn btn-default" href="#resultado" id="profissionais" role="button">Ver-->
<!--                                        Agora&raquo;</a></p>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->


<!--                    <div id="relatorio-total">-->
<!--                        <div id="relatorio-box">-->
<!--                            <div class="col-md-3">-->
<!--                                <img src="//localhost:8888/agendamento/img/graph.png" alt="Divino Barbearia"-->
<!--                                     height="60" width="60">-->
<!--                                <h3>Relatório Total</h3>-->
<!--                                <span>Veja os Gastis e lucros de uma filial. </span>-->
<!--                                <p><a class="btn btn-default" href="#resultado" id="total" role="button">Ver Agora&raquo;</a>-->
<!--                                </p>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->

<!--                    <div id="relatorio-box">-->
<!--                        <div class="col-md-3">-->
<!--                            <img src="//localhost:8888/agendamento/img/user.png" alt="Divino Barbearia"-->
<!--                                 height="60" width="60">-->
<!--                            <h3>Relatório Clientes</h3>-->
<!--                            <span>Veja os clientes cadastrados no sistema. </span>-->
<!--                            <p><a class="btn btn-default" href="#resultado" id="clientes" role="button">Ver-->
<!--                                    Agora&raquo;</a></p>-->
<!--                        </div>-->
<!--                    </div>-->

<!---->
<!--                    <div id="relatorio-box">-->
<!--                        <div class="col-md-3">-->
<!--                            <img src="//localhost:8888/agendamento/img/relatorio_balcao.png" alt="Divino Barbearia"-->
<!--                                 height="60" width="60">-->
<!--                            <h3>Relatório Balcao</h3>-->
<!---->
<!--                            <span>Veja o relatório do Balcao. </span>-->
<!---->
<!--                            <p><a class="btn btn-default" href="#resultado" id="balcao" role="button">Ver-->
<!--                                    Agora&raquo;</a></p>-->
<!--                        </div>-->
<!--                    </div>-->

<!--                    <div id="relatorio-box">-->
<!--                        <div class="col-md-3">-->
<!--                            <img src="//localhost:8888/agendamento/img/relatorio de agendamento.png"-->
<!--                                 alt="Divino Barbearia"-->
<!--                                 height="60" width="60">-->
<!--                            <h3>Relatório de Agendamentos Gratuitos</h3>-->
<!---->
<!--                            <span>Veja o relatório dos Agendamentos Gratuitos. </span>-->
<!---->
<!---->
<!--                            <p><a class="btn btn-default" href="#resultado" id="agendamentogratuito" role="button">Ver-->
<!--                                    Agora&raquo;</a></p>-->
<!--                        </div>-->
<!--                    </div>-->

<!--                    <div id="relatorio-box">-->
<!--                        <div class="col-md-3">-->
<!--                            <img src="//localhost:8888/agendamento/img/produtos cortesia1.png"-->
<!--                                 alt="Divino Barbearia"-->
<!--                                 height="60" width="60">-->
<!--                            <h3>Relatório de Produtos Cortesia</h3>-->
<!---->
<!--                            <span>Veja o relatório de Produtos Cortesia. </span>-->
<!---->
<!--                            <p><a class="btn btn-default" href="#resultado" id="produtocortesia" role="button">Ver Agora&raquo;</a>-->
<!--                            </p>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div id="relatorio-box">-->
<!--                        <div class="col-md-3">-->
<!--                            <img src="//localhost:8888/agendamento/img/stock.png" alt="Divino Barbearia"-->
<!--                                 height="60" width="60">-->
<!--                            <h3>Relatório do Estoque</h3>-->
<!--                            <span>Veja o relatório do Estoque. </span>-->
<!---->
<!--                            <p><a class="btn btn-default" href="#resultado" id="estoque" role="button">Ver-->
<!--                                    Agora&raquo;</a></p>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->


                <div class="row">

                    <div class="col-md-12">
                        <center>


                            <p></p>   <p></p><br>
                        <div rel="stylesheet" media="screen" id="resultado">


                        </div>

                            <div class="panel-body">

                                <table class="table table-hover table-striped" id="tb_detalhes2" class="display"
                                       width="100%">
                                    <thead>
                                    <tr>
                                        <th></th>

                                        <th style="color:#bc9355; text-align:left;">Nome</th>
                                        <th style="color:#bc9355; text-align:left;">Telefone</th>
                                        <th style="color:#bc9355; text-align:left;">Email</th>
                                        <th style="color:#bc9355; text-align:left;">Filial</th>
                                        <th style="color:#bc9355; text-align:left;">Data Aniversario</th>

                                    </tr>
                                    </thead>
                                    <tbody class="table-striped">
                                    </tbody>
                                </table>
                            </div>

                        <p><a class="btn btn-default" id="imprimir" role="button">Imprimir&raquo;</a></p>

                    </div>



                    </center>

                </div>


            </div>

            <!-- END: .main-content -->
        </div>
        <!-- END: .app-main -->
    </div>
    <!-- END: .app-container -->
    <!-- BEGIN .main-footer -->
    <footer class="main-footer fixed-btm">
        Copyright Divino Admin 2018.
    </footer>
    <!-- END: .main-footer -->
</div>
<!-- END: .app-wrap -->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script src="../js/vendor/jquery-1.11.2.js"></script>
<script>window.jQuery || document.write('<script href="../js/vendor/jquery-1.11.2.js"><\/script>')</script>
<script src="../js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
<script src="../js/plugins.js"></script>


<script src="../js/main.js"></script>
<script src="../js/vendor/bootstrap.min.js"></script>
<script src="../js/moment.js"></script>
<script src="../jquery-ui/jquery-ui.js"></script>
<script type="text/javascript" src="../js/wickedpicker/wickedpicker.min.js"></script>
<script type="text/javascript" src="../js/fullcalendar/moment.min.js"></script>
<script type="text/javascript" src="../js/fullcalendar/fullcalendar.js"></script>
<script type="text/javascript" src="../js/fullcalendar/pt-br.js"></script>
<script type="text/javascript" src="../js/fullcalendar/gcal.js"></script>
<script type="text/javascript" src="../js/jquery.timepicker.js"></script>

<script src="../js/relatorio.js"></script>

<script src="../js/tether.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../vendor/unifyMenu/unifyMenu.js"></script>
<script src="../vendor/onoffcanvas/onoffcanvas.js"></script>


        <link rel="stylesheet" href="../css/bootstrap.min.min.css">
        <link rel="stylesheet" href="../css/jquery.dataTables.min.css">

        <!-- Common JS -->
        <script src="../js/common.js"></script>

        <script src="../js/bootstrap.min.js"></script>


        <script src="../js/jquery.dataTables.min.js"></script>

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>



        <!-- Common JS -->
<script src="../js/common.js"></script>


<link href="../css/custom-theme/jquery-ui-1.9.2.custom.css" rel="stylesheet">
        <link href="cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" rel="stylesheet">
<script>


    $(document).ready(function () {

        var table = '';


        $("#aniversario").click(function () {

            if (validaRel())
                RelatorioAgendamentoDetalhes();
            else {
                alert('Informe as datas para  prosseguir...');
            }
        });


        function validaRel() {
            var inicio = $('#inicio').val();
            var fim = $('#fim').val();
            // var status = $('#status').val();
            var status = $('#combobox-status').val();

            if (notNull(inicio) && notNull(fim)) {
                return true;
            }
            else {
                return false;
            }
        }


        if (usuario !== null) {
            $('#nome').text(usuario.result[0].Nome);
            var perfil = usuario.result[0].Perfil;
            var id = usuario.result[0].ID;
            var idfilial = JSON.parse(getCookie("filial"));

//

        }
        else {

            window.location.replace("index.php");


            function logout() {

                eraseCookie("user");

            }


        }

        function notNull(a) {
            if (a != "" && !(a.match(/^\s+$/))) {
                return true;
            } else {
                return false;
            }
        }


        function format(d) {
            // console.log(d.Agendamento[0].NomeProduto);
            var retorno = '';





            for (var i = 0; i < d.Agendamento.length; i++) {


                var table =

                    '<td>' +
                    "Cliente : " + d.Agendamento[i].Cliente +
                    '</td>' +
                    '<td>' +
                    "Data : " + d.Agendamento[i].Data1 +
                    '</td>' +
                    '<td>' +
                    "Produto : " + d.Agendamento[i].NomeProduto +
                    '</td>' +
                    '<td>' +
                    "Valor : " + d.Agendamento[i].ValorProduto +
                    '</td>';

                var resultado = table;

            }




            return resultado;

        }


        function RelatorioAgendamentoDetalhes() {


            TabelaAgenda();

        }


        function TabelaAgenda() {


            if (table != '') {
                $('#tb_detalhes2').DataTable().destroy();
                $('#tb_detalhes2 tbody').empty();


                //revert table to it's original state


            }


            var status = $("#combobox-status").val();

            var filial = idfilial;

            var inicio = $("#inicio").val();

            var fim = $("#fim").val();


            var url = "//localhost:8888/agendamentoAPI/web/index.php?r=divino/relatorio-aniversario"
                + "&filial=" + filial
                + "&status=" + status
                + "&inicio=" + inicio
                + "&fim=" + fim;

            console.log(url);


            table = $("#tb_detalhes2").DataTable({

                "processing": false,

                "destroy": true,
                "oLanguage": {
                    "sProcessing": "Processando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "Não foram encontrados resultados",
                    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando de 0 até 0 de 0 registros",
                    "sInfoFiltered": "",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "Primeiro",
                        "sPrevious": "Anterior",
                        "sNext": "Seguinte",
                        "sLast": "Último"
                    }
                },
                "ajax": {
                    "url": url,
                    "type": "GET",
                    "dataSrc": function (d) {

                    return d;
        }
        },

            "columns"
        :
            [
                {

                    "orderable": false,

                    "defaultContent": ""
                },

                {"data": "Cliente"},
                {"data": "Telefone"},
                {"data": "Email"},
                {"data": "Filial"},
                {"data": "DataAniver"}
            ],
                "order"
        :
            [[1, 'asc']]
        } )
            ;


            var detailRows = [];

            $("#tb_detalhes2 tbody").on("click", "tr td.details-control", function () {
//            iniciatable();


                var
                    tr = $(this).closest("tr");
                var
                    row = table.row(tr);
                var
                    idx = $.inArray(tr.attr("id"), detailRows);
                if (row.child.isShown()) {
                    tr.removeClass("details");
                    row.child.hide();
                    // Remove from the "open" array
                    detailRows.splice(idx, 1);
                } else {
                    tr.addClass("details");
                    row.child(format(row.data())).show();
                    // Add to the "open" array
                    if (idx === -1) {
                        detailRows.push(tr.attr("id"));
                    }
                }
                e.preventDefault();
            });


            table.on("draw", function () {


                $.each(detailRows, function (i, id) {
                    $("#" + id + " td.details-control").trigger("click");
                });

            });


        }

    })
    ;


</script>

        <style>


            .navbar-inverse {
                opacity: 1;

            }

            select {
                color: #000;
                border: 1px solid #cccccc;
                border-radius: 0px;
                text-align: left;
                background: #fff !important;
                border-radius: 5px;
            }

            select {
                width: 100%;
            }

            .custom-combobox {
                position: relative;
                display: inline-block;
            }

            .custom-combobox-toggle {
                position: absolute;
                top: 0;
                bottom: 0;
                margin-left: -1px;
                padding: 0;
            }

            .custom-combobox-input {
                margin: 0;
                padding: 5px 10px;
            }

            #table-produtos {
                border-collapse: collapse;
                /*padding:20px;*/
                margin: 0;
                width: 100%;
            }

            #table-produtos-atualizar {
                border-collapse: collapse;
                /*padding:20px;*/
                margin: 0;
                width: 100%;
            }

            h4 {
                margin: 0px;
                padding: 0px;
            }

            input {
                margin: 0px;
                padding: 0px;
                margin-top: 5px;
            }
            #tb_detalhes2_info{
                color: #fff;
            }
            #tb_detalhes2_info{
                color: #fff;
            }
            .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
                color: black !important;
                border: none;
                background-color: #fff;
                background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #fff), color-stop(100%, #cbac7b));
                /* Chrome,Safari4+ */
                background: -webkit-linear-gradient(top, #fff 0%, #cbac7b 100%);
                /* Chrome10+,Safari5.1+ */
                background: -moz-linear-gradient(top, #fff 0%, #cbac7b 100%);
                /* FF3.6+ */
                background: -ms-linear-gradient(top, #fff 0%, #cbac7b 100%);
                /* IE10+ */
                background: -o-linear-gradient(top, #fff 0%, #cbac7b 100%);
                /* Opera 11.10+ */
                background: linear-gradient(to bottom, #fff 0%, #cbac7b 100%);
                /* W3C */
            }


        </style>


</body>
</html>



