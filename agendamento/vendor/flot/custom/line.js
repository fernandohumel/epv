$(function () {

	var d1, data, chartOptions;



    var myCookie = getCookie("filial");

    if (myCookie == null) {
        var filial = usuario.result[0].Filial;
    } else {
        var filial = JSON.parse(getCookie("filial"));
    }


    getAgendamentosMes();
    //inicia get agendamentos mes
    var data = JSON.parse(getCookie('agendamentos'));
    data = data.result;
	var d1 = [[1262304000000, data[0]], [1264982400000,  data[1]], [1267401600000,  data[2]], [1270080000000,  data[3]], [1272672000000,  data[4]], [1275350400000,  data[5]], [1277942400000,  data[6]], [1280620800000,  data[7]], [1283299200000,  data[8]], [1285891200000,  data[9]], [1288569600000,  data[10]], [1291161600000,  data[11]]];






	data = [{ 
		label: "Agendamentos", 
		data: d1
	}];

	chartOptions = {
		xaxis: {
			min: (new Date(2009, 11)).getTime(),
			max: (new Date(2010, 11)).getTime(),
			mode: "time",
			tickSize: [1, "month"],
			monthNames: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
			tickLength: 0
		},
		yaxis: {

		},
		series: {
			lines: {
				show: true, 
				fill: false,
				lineWidth: 2,
			},
			points: {
				show: true,
				radius: 4,
				fill: true,
				fillColor: "#ffffff",
				lineWidth: 2
			}
		},
		 grid:{
			hoverable: true,
			clickable: true,
			borderWidth: 1,
			tickColor: '#585e6f',
			borderColor: '#585e6f',
		},
		shadowSize: 0,
		legend: {
			show: true,
			position: 'nw'
		},
		tooltip: true,
		tooltipOpts: {
			content: '%s: %y'
		},
		colors: ['#cbac7b', '#e5e8f2', '#ff5661'],
	};

	var holder = $('#line-chart');

	if (holder.length) {
		$.plot(holder, data, chartOptions );
	}





    function getAgendamentosMes() {


        var url = "//localhost:8888/agendamentoAPI/web/index.php?r=divino/agendamentos-mes";
        $.ajax({
            url: url,
            data: {filial: filial},
            type: 'GET',
            async: false
        }).success(function (data) {


            setCookie("agendamentos", JSON.stringify(data));
            //se for false nao pode
        }).error(function () {
            alert('falha verificacao de dias disponiveis do profisisonal');
        });

    }


    function setCookie(key, value) {
        var expires = new Date();
        expires.setTime(expires.getTime() + (1 * 24 * 60 * 60 * 1000));
        document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
    }

    function getCookie(key) {
        var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
        return keyValue ? keyValue[2] : null;
    }

    function eraseCookie(name) {
        document.cookie = name + '=; Max-Age=0'
    }

});