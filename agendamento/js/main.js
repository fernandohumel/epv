function setCookie(key, value) {
    var expires = new Date();
    expires.setTime(expires.getTime() + (1 * 24 * 60 * 60 * 1000));
    document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
}

function getCookie(key) {
    var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
    return keyValue ? keyValue[2] : null;
}

function eraseCookie(name) {
    document.cookie = name + '=; Max-Age=0'
}


function logout(){

    eraseCookie("user");

}



    $('#logout').click(function () {
        logout();
        window.location.replace("../pages/login.php");
    });

    $('#cadastro').click(function () {
        window.location.replace("cadastro.html");
    });
    $('#ferramentas').click(function () {
  
        window.location.replace("ferramentas.html");
    });
    $('#home').click(function () {
        logout();
        window.location.replace("index.php");
    });




var usuario = JSON.parse(getCookie("user"));





if (usuario !== null) {
    $('#nome').text(usuario.result[0].Nome);
    var perfil = usuario.result[0].Perfil;
    var id = usuario.result[0].ID;
    var idfilial = JSON.parse(getCookie("filial"));

    // Store
    localStorage.setItem("user", usuario);

// Store
    localStorage.setItem("perfil", perfil);

// Store
    localStorage.setItem("id", id);

// Store
    localStorage.setItem("idfilial", idfilial);

// Store
    localStorage.setItem("nome", usuario.result[0].Nome);

// // Retrieve
//     document.getElementById("result").innerHTML = localStorage.getItem("lastname");

    //
    // if (perfil == 1 || perfil == 3) {
    //     document.getElementById("filial").style.display = "block";
    // } else {
    //     document.getElementById("cadastros_link_nao").style.display = "none";
    // }

    filial(idfilial);

    getFiliais();

}
else {

    window.location.replace("login.php");
}


//funcao onchange filial nome
function myFunction() {

    var e = document.getElementById("filialchange");
    var value = e.options[e.selectedIndex].value;
    var text = e.options[e.selectedIndex].text;
    //document.getElementById("fili").innerHTML = " Filial = "+ text;
}

//funcao retorna nome da filial
function filial(x) {


    var url = "//localhost:8888/agendamentoAPI/web/index.php?r=divino/filial-cliente&id=" + x;
    var jqxhr = $.get(url
        , function (data) {
            //escrevendo a filial
            // var div = document.getElementById("fili");
            // div.innerText = " Filial = "+ data.result[0].Nome;

            if (data.error === false) {


            }
            else {
                alert("Humm..... " + JSON.stringify(data));
            }

        })
        .done(function () {

            return;
            //                    alert("second success");
        })
        .fail(function () {
            alert("error getting clientes...");
        })
        .always(function () {
            //                    alert("finished");
        });

    // Perform other work here ...

    // Set another completion function for the request above
    jqxhr.always(function () {
        //                alert("second finished");

    });


}


//verifica se é mastert

//Funcao que retorna filial


function GetParam(name) {
    var start = location.search.indexOf("?" + name + "=");
    if (start < 0) start = location.search.indexOf("&" + name + "=");
    if (start < 0) return '';
    start += name.length + 2;
    var end = location.search.indexOf("&", start) - 1;
    if (end < 0) end = location.search.length;
    var result = '';
    for (var i = start; i <= end; i++) {
        var c = location.search.charAt(i);
        result = result + (c == '+' ? ' ' : c);
    }
    return unescape(result);
}


//        alert(GetParam('usuario'));


function enviaFilial() {
    var usuario = JSON.parse(getCookie("user"));

    var id = usuario.result[0].ID;

    //            var url = "//localhost:8888/divinoAPI/web/index.php?r=divino/login&user=divino&pin=123";
    var url = "//localhost:8888/agendamentoAPI/web/index.php?r=divino/muda-filial"
        + "&filial=" + $('#filialchange option:selected').val()
        + "&id_do_cara=" + id;


    var jqxhr = $.get(url
        , function (data) {
            // alert("success " + JSON.stringify(data));

            setCookie("filial", JSON.stringify($('#filialchange option:selected').val()));

            //setcookie("usuario.result[0].Perfil;", $('#filialchange option:selected').val());


            //window.location.replace("ferramentas.html");
        })
        .done(function () {
            return;
            //                    alert("second success");
        })
        .fail(function () {
            alert("error");
        })
        .always(function () {
            //                    alert("finished");
        });

    // Perform other work here ...

    // Set another completion function for the request above
    jqxhr.always(function () {
        //                alert("second finished");
    });
}


function getFiliais() {

    console.log('chego');

    var url = "//localhost:8888/agendamentoAPI/web/index.php?r=divino/filial";
    var jqxhr = $.get(url
        , function (data) {
            if (data.error === false) {

                //limpa o combo de clientes
                $('#filialchange').empty();


                //percorre array de usuarios
                for (var i = 0, len = data.result.length; i < len; i++) {


                    var filial = JSON.parse(getCookie("filial"));


                    if (filial == data.result[i].ID) {
                        $('#filialchange').append('<option value="' + data.result[i].ID + '" selected="selected">' + data.result[i].Nome + '</option>');
                    } else {
                        $('#filialchange').append('<option value="' + data.result[i].ID + '" ">' + data.result[i].Nome + '</option>');

                    }


                }


            }
            else {
                alert("Humm..... " + JSON.stringify(data));
            }

        })
        .done(function () {
            //                    alert("second success");
        })
        .fail(function () {
            alert("error getting clientes...");
        })
        .always(function () {
            //                    alert("finished");
        });

    // Perform other work here ...

    // Set another completion function for the request above
    jqxhr.always(function () {
        //                alert("second finished");
    });

}


$('#filialchange').change(function () {
    enviaFilial();

});